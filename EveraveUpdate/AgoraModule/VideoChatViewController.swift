//
//  VideoChatViewController.swift
//  Agora iOS Tutorial
//
//  Created by James Fang on 7/14/16.
//  Copyright © 2016 Agora.io. All rights reserved.
//

import UIKit
//import AgoraRtcKit
import AgoraRtcEngineKit
import Cosmos

class VideoChatViewController: BaseVC1 {
    @IBOutlet weak var localVideo: UIView!
    @IBOutlet weak var remoteVideo: UIView!
    @IBOutlet weak var remoteVideoMutedIndicator: UIImageView!
    @IBOutlet weak var localVideoMutedIndicator: UIView!
    @IBOutlet weak var micButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var uiv_ratingView: UIView!
    @IBOutlet weak var cus_rating: CosmosView!
    @IBOutlet weak var txv_feedBack: UITextView!
    
    weak var logVC: LogViewController?
    var agoraKit: AgoraRtcEngineKit!
//
    var isRemoteVideoRender: Bool = true {
        didSet {
            remoteVideoMutedIndicator.isHidden = isRemoteVideoRender
            remoteVideo.isHidden = !isRemoteVideoRender
        }
    }

    var isLocalVideoRender: Bool = false {
        didSet {
            localVideoMutedIndicator.isHidden = isLocalVideoRender
        }
    }

    var isStartCalling: Bool = true {
        didSet {
            if isStartCalling {
                micButton.isSelected = false
            }
            micButton.isHidden = !isStartCalling
            cameraButton.isHidden = !isStartCalling
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // This is our usual steps for joining
        // a channel and starting a call.
        initializeAgoraEngine()
        setupVideo()
        setupLocalVideo()
        joinChannel()
        self.uiv_ratingView.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let identifier = segue.identifier else {
            return
        }

        if identifier == "EmbedLogViewController",
            let vc = segue.destination as? LogViewController {
            self.logVC = vc
        }
    }

    func initializeAgoraEngine() {
        // init AgoraRtcEngineKit
        agoraKit = AgoraRtcEngineKit.sharedEngine(withAppId:agoraAppID , delegate: self)
    }

    func setupVideo() {
        // In simple use cases, we only need to enable video capturing
        // and rendering once at the initialization step.
        // Note: audio recording and playing is enabled by default.
        agoraKit.enableVideo()

        // Set video configuration
        // Please go to this page for detailed explanation
        // https://docs.agora.io/cn/Voice/API%20Reference/java/classio_1_1agora_1_1rtc_1_1_rtc_engine.html#af5f4de754e2c1f493096641c5c5c1d8f
        agoraKit.setVideoEncoderConfiguration(AgoraVideoEncoderConfiguration(size: AgoraVideoDimension640x360,
                                                                             frameRate: .fps15,
                                                                             bitrate: AgoraVideoBitrateStandard,
                                                                             orientationMode: .adaptative))
    }

    func setupLocalVideo() {
        // This is used to set a local preview.
        // The steps setting local and remote view are very similar.
        // But note that if the local user do not have a uid or do
        // not care what the uid is, he can set his uid as ZERO.
        // Our server will assign one and return the uid via the block
        // callback (joinSuccessBlock) after
        // joining the channel successfully.
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = 0
        videoCanvas.view = localVideo
        videoCanvas.renderMode = .hidden
        agoraKit.setupLocalVideo(videoCanvas)
    }

    func joinChannel() {
        // Set audio route to speaker
        agoraKit.setDefaultAudioRouteToSpeakerphone(true)

        // 1. Users can only see each other after they join the
        // same channel successfully using the same app id.
        // 2. One token is only valid for the channel name that
        // you use to generate this token.
        if let room_id = roomID{
            agoraKit.joinChannel(byToken: "", channelId: room_id, info: nil, uid: 0) { [unowned self] (channel, uid, elapsed) -> Void in
                // Did join channel "demoChannel1"
                self.isLocalVideoRender = true
                self.logVC?.log(type: .info, content: "did join channel")
            }
            isStartCalling = true
            UIApplication.shared.isIdleTimerDisabled = true
        }
    }

    func leaveChannel() {
        // leave channel and end chat
        agoraKit.leaveChannel(nil)

        isRemoteVideoRender = false
        isLocalVideoRender = false
        isStartCalling = false
        UIApplication.shared.isIdleTimerDisabled = false
        self.logVC?.log(type: .info, content: "did leave channel")
    }
    @IBAction func ratingOkBtnClicked(_ sender: Any) {
        
        if self.txv_feedBack.text == "" || cus_rating.rating == 0.0{
            showAlerMessage(message: "Please fill of your feedback")
        }else{
            
            if let selectedbooking = selectedBooking{
                self.showLoadingView(vc: self)
                ApiManager.completeBooking(id: selectedbooking.id!, review: self.txv_feedBack.text, rating: "\(cus_rating.rating)", seller_id: selectedbooking.seller_id!, buyer_id: selectedbooking.buyer_id!,buyer_name: selectedbooking.buyer_name!, buyer_picture: selectedbooking.buyer_picture ?? "", service_date: "\(selectedbooking.service_date)") { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        self.uiv_ratingView.isHidden = true
                        if user!.type == PARAMS.SELLER{
                            self.gotoStoryBoardVC("HistorySellerVC", fullscreen: true)
                        }else{
                            self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
                        }
                    }else{
                        self.uiv_ratingView.isHidden = true
                        self.showAlerMessage(message: "Network issue")
                    }
                }
            }
        }
    }
    @IBAction func ratingCancelBtnClicked(_ sender: Any) {
        
        if let selectedbooking = selectedBooking{
            self.showLoadingView(vc: self)
            ApiManager.completeBooking(id: selectedbooking.id!, review:"", rating: "0", seller_id: selectedbooking.seller_id!, buyer_id: selectedbooking.buyer_id!,buyer_name: selectedbooking.buyer_name!, buyer_picture: selectedbooking.buyer_picture ?? "", service_date: "\(selectedbooking.service_date)") { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.uiv_ratingView.isHidden = true
                    roomID = nil
                    self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
                    
                }else{
                    self.uiv_ratingView.isHidden = true
                    self.showAlerMessage(message: "Network issue")
                }
            }
        }
    }
    
    @IBAction func didClickHangUpButton(_ sender: UIButton) {
        /*sender.isSelected.toggle()
        if sender.isSelected {
            leaveChannel()
        } else {
            joinChannel()
        }*/
        // Create Alert
        let attributedString = NSAttributedString(string: "Do you want to leave?", attributes: [
            NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24),
            NSAttributedString.Key.foregroundColor : UIColor.black])
        let alert = UIAlertController(title: "", message: "",  preferredStyle: .alert)

        alert.setValue(attributedString, forKey: "attributedTitle")
        
        
        // Create OK button with action handler
        let ok = UIAlertAction(title: "OK", style: .default, handler: { (action) -> Void in
            if user!.type == PARAMS.BUYER{
                self.leaveChannel()
                self.showRatingView()
            }else if user!.type == PARAMS.SELLER{
                self.leaveChannel()
                self.gotoVC("SellerMainVC")
            }else{
                self.gotoStoryBoardVC("MainVC", fullscreen: true)
            }
        })

        // Create Cancel button with action handlder
        let cancel = UIAlertAction(title: "CANCEL", style: .cancel) { (action) -> Void in
            
        }
        //Add OK and Cancel button to an Alert object
        alert.addAction(ok)
        alert.addAction(cancel)

        // Present alert message to user
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRatingView() {
        self.uiv_ratingView.isHidden = false
    }

    @IBAction func didClickMuteButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        // mute local audio
        agoraKit.muteLocalAudioStream(sender.isSelected)
    }

    @IBAction func didClickSwitchCameraButton(_ sender: UIButton) {
        sender.isSelected.toggle()
        agoraKit.switchCamera()
    }
}

extension VideoChatViewController: AgoraRtcEngineDelegate {
    
    /// Callback to handle the event when the first frame of a remote video stream is decoded on the device.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - uid: user id
    ///   - size: the height and width of the video frame
    ///   - elapsed: Time elapsed (ms) from the local user calling JoinChannel method until the SDK triggers this callback.
    func rtcEngine(_ engine: AgoraRtcEngineKit, firstRemoteVideoDecodedOfUid uid:UInt, size:CGSize, elapsed:Int) {
        isRemoteVideoRender = true

        // Only one remote video view is available for this
        // tutorial. Here we check if there exists a surface
        // view tagged as this uid.
        let videoCanvas = AgoraRtcVideoCanvas()
        videoCanvas.uid = uid
        videoCanvas.view = remoteVideo
        videoCanvas.renderMode = .hidden
        agoraKit.setupRemoteVideo(videoCanvas)
    }

    /// Occurs when a remote user (Communication)/host (Live Broadcast) leaves a channel.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - uid: ID of the user or host who leaves a channel or goes offline.
    ///   - reason: Reason why the user goes offline
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOfflineOfUid uid:UInt, reason:AgoraUserOfflineReason) {
        isRemoteVideoRender = false
    }

    /// Occurs when a remote user’s video stream playback pauses/resumes.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - muted: YES for paused, NO for resumed.
    ///   - byUid: User ID of the remote user.
    func rtcEngine(_ engine: AgoraRtcEngineKit, didVideoMuted muted:Bool, byUid:UInt) {
        isRemoteVideoRender = !muted
    }

    /// Reports a warning during SDK runtime.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - warningCode: Warning code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurWarning warningCode: AgoraWarningCode) {
        logVC?.log(type: .warning, content: "did occur warning, code: \(warningCode.rawValue)")
    }

    /// Reports an error during SDK runtime.
    /// - Parameters:
    ///   - engine: RTC engine instance
    ///   - errorCode: Error code
    func rtcEngine(_ engine: AgoraRtcEngineKit, didOccurError errorCode: AgoraErrorCode) {
        logVC?.log(type: .error, content: "did occur error, code: \(errorCode.rawValue)")
    }
}
