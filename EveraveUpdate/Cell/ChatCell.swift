//
//  ChatCell.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import Kingfisher

class ChatCell: UITableViewCell {

    
    @IBOutlet weak var meView: UIView!
    @IBOutlet weak var melblContent: UILabel!
    @IBOutlet weak var melblTime: UILabel!
    //@IBOutlet weak var mecorner: UIImageView!
    @IBOutlet weak var youView: UIView!
    @IBOutlet weak var youlblContent: UILabel!
    @IBOutlet weak var youlblTime: UILabel!
    //@IBOutlet weak var youreadStatue: UIImageView!
    //@IBOutlet weak var yourcorner: UIImageView!
    @IBOutlet weak var cons_meWidth: NSLayoutConstraint!
    @IBOutlet weak var cons_youWidth: NSLayoutConstraint!
    
    var entity : ChatModel!{
        didSet {
            melblContent.text = entity.msgContent
            let time = getStrDateshort(entity.timestamp)
            melblTime.text = time
            youlblContent.text = entity.msgContent
            youlblTime.text = time
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
