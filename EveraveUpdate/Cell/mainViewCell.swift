//
//  mainViewCell.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import UIKit
import SwipeCellKit
import Kingfisher
import Cosmos

class mainViewCell: SwipeCollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet var imv_online: UIImageView!
    @IBOutlet var imv_offline: UIImageView!
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_category: UILabel!
    @IBOutlet weak var lbl_ratingVal: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    
    var entity: SellerModel!{
        
        didSet{
            let url = URL(string: entity.picture ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            //imv_profile.image = UIImage.init(named: "avatar")
            if entity.state == "online"{//on, off
                imv_online.isHidden = false
                imv_offline.isHidden = true
            }
            else{
                imv_online.isHidden = true
                imv_offline.isHidden = false
            }
            
            if let user_name = entity.user_name{
                lbl_name.text = user_name
            }
            
            let serviceModellist = entity.serviceModelList
            var num = 0
            var str_category = ""
            for one in serviceModellist{
                num += 1
                
                if num != serviceModellist.count{
                    str_category = str_category + one.name! + ","
                }else{
                    str_category = str_category + one.name!
                }
            }
            self.lbl_category.text = str_category
            self.lbl_ratingVal.text = "\(entity.rating ?? 0.0)"
            
            if entity.count?.toInt() == 0 || entity.count == ""{
                self.lbl_count.text = "(0 Reviews)"
            }else{
                self.lbl_count.text = "(" + entity.count! + " " + "Reviews)"
            }
        }
    }
}

class BookingCell: SwipeCollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet var lbl_name: UILabel!
    @IBOutlet var lbl_category: UILabel!
    @IBOutlet var lbl_time: UILabel!
    @IBOutlet var lbl_price: UILabel!
    @IBOutlet var lbl_state: UILabel!
    @IBOutlet weak var lbl_bookingstate: UILabel!
    
    var entity: BookingModel!{
        
        /* // 0 : buyer side - pending & seller side - request (buyer request)
        // 1 : buyer side - pay & seller side - pending (seller accept)
        // 2 : buyer side - Accepted & seller side - Accepted (buyer request)
        // 3 : Completed
        // 4 : Rejected (seller rejectl) , this is possible when it has been accepted.
        // 5 : Cancelled (buyer cancel)  , this is possible when it has been accepted.
        // 6 : refunded
         */
        
        didSet{
            
            if user?.type == PARAMS.BUYER{
                let url = URL(string: entity.seller_picture ?? "")
                imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
                lbl_name.text = entity.seller_name
                lbl_category.text = entity.service_name
                lbl_price.text = "$" + entity.service_cost!
                if entity.state == "0"{
                    self.lbl_bookingstate.isHidden = false
                    self.lbl_state.isHidden = true
                    if user?.type == PARAMS.BUYER{
                        self.lbl_bookingstate.text = "PENDING SELLER APPROVAL"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4seller")
                        lbl_state.text = ""
                    }else{
                        self.lbl_bookingstate.text = "SWIPE LEFT TO ACCEPT OR REJECT"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4buyer")
                        lbl_state.text = ""
                    }
                
                }else if entity.state == "1"{
                    if user?.type == PARAMS.BUYER{
                        self.lbl_bookingstate.isHidden = false
                        self.lbl_state.isHidden = false
                        self.lbl_bookingstate.text = "SWIPE LEFT TO PAY NOW"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4buyer")
                        lbl_state.text = "CONFIRMED"
                        lbl_state.textColor = UIColor.init(named: "ColorPending4buyer")
                    }else{
                        self.lbl_bookingstate.isHidden = false
                        self.lbl_state.isHidden = true
                        self.lbl_bookingstate.text = "PENDING CLIENT PAYMENT"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4seller")
                        lbl_state.text = ""
                    }
                }
                else if entity.state == "2"{
                    self.lbl_bookingstate.isHidden = false
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = "CLICK HERE TO START APPOINTMENT"
                    lbl_bookingstate.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                    lbl_state.text = "CONFIRMED"
                    lbl_state.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                }
                else if entity.state == "3"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "COMPLETED"
                    lbl_state.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                }else if entity.state == "4"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "REJECTED"
                    lbl_state.textColor = .red
                }else if entity.state == "5"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "CANCELED"
                    lbl_state.textColor = UIColor.init(named: "ColorCancel")
                }else{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = true
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = ""
                }
                lbl_time.text = getMessageTimeFromTimeStamp(Int64(entity.service_date))
            }else{
                let url = URL(string: entity.buyer_picture ?? "")
                imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
                lbl_name.text = entity.buyer_name
                lbl_category.text = entity.service_name
                lbl_price.text = "$" + entity.service_cost!
                if entity.state == "0"{
                    self.lbl_bookingstate.isHidden = false
                    self.lbl_state.isHidden = true
                    if user?.type == PARAMS.BUYER{
                        self.lbl_bookingstate.text = "PENDING SELLER APPROVAL"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4seller")
                        lbl_state.text = ""
                    }else{
                        self.lbl_bookingstate.text = "SWIPE LEFT TO ACCEPT OR REJECT"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4buyer")
                        lbl_state.text = ""
                    }
                
                }else if entity.state == "1"{
                    if user?.type == PARAMS.BUYER{
                        self.lbl_bookingstate.isHidden = false
                        self.lbl_state.isHidden = false
                        self.lbl_bookingstate.text = "SWIPE LEFT TO PAY NOW"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4buyer")
                        lbl_state.text = "CONFIRMED"
                        lbl_state.textColor = UIColor.init(named: "ColorPending4buyer")
                    }else{
                        self.lbl_bookingstate.isHidden = false
                        self.lbl_state.isHidden = true
                        self.lbl_bookingstate.text = "PENDING CLIENT PAYMENT"
                        self.lbl_bookingstate.textColor = UIColor.init(named: "ColorPending4seller")
                        lbl_state.text = ""
                    }
                }
                else if entity.state == "2"{
                    self.lbl_bookingstate.isHidden = false
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = "CLICK HERE TO START APPOINTMENT"
                    lbl_bookingstate.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                    lbl_state.text = "CONFIRMED"
                    lbl_state.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                }
                else if entity.state == "3"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "COMPLETED"
                    lbl_state.textColor = UIColor.init(named: "ColorCompletedDarkGreen")
                }else if entity.state == "4"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "REJECTED"
                    lbl_state.textColor = .red
                }else if entity.state == "5"{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = false
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = "CANCELED"
                    lbl_state.textColor = UIColor.init(named: "ColorCancel")
                }else{
                    self.lbl_bookingstate.isHidden = true
                    self.lbl_state.isHidden = true
                    self.lbl_bookingstate.text = ""
                    lbl_state.text = ""
                }
                lbl_time.text = getMessageTimeFromTimeStamp(Int64(entity.service_date))
            }
            
        }
    }
}

class reviewCell: UICollectionViewCell {
    
    @IBOutlet var imv_profile: UIImageView!
    @IBOutlet var lbl_userName: UILabel!
    @IBOutlet var lbl_time: UILabel!
    
    @IBOutlet weak var lbl_ratingVal: UILabel!
    @IBOutlet var lbl_reviewContent: UILabel!
    
    var entity: ReviewModel!{
        
        didSet{
            let url = URL(string: entity.buyer_picture ?? "")
            imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            //imv_profile.image = UIImage.init(named: "avatar")
            lbl_userName.text = entity.buyer_name
            lbl_time.text = getMessageTimeFromTimeStamp(Int64(entity.service_date))
            lbl_ratingVal.text = "\(entity.rating ?? 0.0)"
            lbl_reviewContent.text = entity.review
        }
    }
}

class portfolioCell: UICollectionViewCell {
    
    @IBOutlet var imv_portfolio: UIImageView!
    
    var entity: PortfolioModel!{
        
        didSet{
            let url = URL(string: entity.image ?? "")
            imv_portfolio.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
        }
    }
}

class uploadPictureCell: UICollectionViewCell {
    
    @IBOutlet var imv_portfolio: UIImageView!
    @IBOutlet var btn_close: UIButton!
    
    var entity: PortfolioModel!{
        
        didSet{
            if let url = URL(string: entity.image ?? ""){
                 imv_portfolio.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            }
            
            else if let image = entity.imageFile{
                imv_portfolio.image = image
            }
        }
    }
}

class timeCell: UICollectionViewCell {
    
    @IBOutlet weak var backView: UIView!
    @IBOutlet var lbl_time: UILabel!
    
    var entity: timeModel!{
        didSet{
            self.lbl_time.text = entity.str_time
        }
    }
    override var isSelected: Bool {
        didSet {
            self.backView.borderWidth = isSelected ? 2 : 0.0
            self.backView.borderColor = isSelected ? .black : .clear
        }
    }
}


