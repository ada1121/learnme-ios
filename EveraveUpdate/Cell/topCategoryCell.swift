//
//  topCategoryCell.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import UIKit
import SwipeCellKit
import Kingfisher

class topCategoryCell: UICollectionViewCell {
    
    @IBOutlet var photo: UIImageView!
    @IBOutlet var title: UILabel!
    
    var entity:topcategoryModel!{
        
        didSet{
            /*let url = URL(string: entity.image ?? "")
            photo.kf.setImage(with: url,placeholder: UIImage(named: "appLogo"))*/
            photo.image = UIImage.init(named: entity.image!)
            title.text = entity.title
        }
    }
}
