//
//  CustomizationExampleUsedClasses.swift
//  ExpyTableView
//
//  Created by Okhan on 22/06/2017.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import ExpyTableView

//MARK: Used Table View Classes

class SelCatCell: UITableViewCell, ExpyTableViewHeaderCell{
    
    @IBOutlet weak var labelCatName: UILabel!
    @IBOutlet weak var imageViewArrow: UIImageView!
    @IBOutlet weak var btn_catDelete: UIButton!
    
    func changeState(_ state: ExpyState, cellReuseStatus cellReuse: Bool) {
        
        switch state {
        case .willExpand:
            //print("")
            hideSeparator()
            arrowDown(animated: !cellReuse)
            
        case .willCollapse:
            //print("")
            arrowTop(animated: !cellReuse)
            
        case .didExpand:
            print("")
            
        case .didCollapse:
            showSeparator()
            //print("")
        }
    }
    
    private func arrowDown(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.imageViewArrow.transform = CGAffineTransform(rotationAngle: 0)
        }
    }
    
    private func arrowTop(animated: Bool) {
        UIView.animate(withDuration: (animated ? 0.3 : 0)) {
            self.imageViewArrow.transform = CGAffineTransform(rotationAngle: (CGFloat.pi / 1))
        }
    }
}



class DetailValTableViewCell: UITableViewCell { // for buyer side
    
    @IBOutlet weak var labelselectedName: UILabel!
    @IBOutlet weak var labelselectedvalue: UILabel!
    @IBOutlet weak var btn_appointment: UIButton!
}

class DetailValTableViewCell2: UITableViewCell {// for seller side
    
    @IBOutlet weak var btn_delete: UIButton!
    @IBOutlet weak var labelselectedName: UILabel!
    @IBOutlet weak var labelselectedvalue: UILabel!
}

