//
//  chatImageCell.swift
//  EveraveUpdate
//
//  Created by Mac on 7/2/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

class chatImageCell: UITableViewCell {

    
    @IBOutlet weak var uiv_me: UIView!
    @IBOutlet weak var imv_meImage: UIImageView!
    @IBOutlet weak var lbl_metime: UILabel!
    @IBOutlet weak var uiv_you: UIView!
    @IBOutlet weak var imv_youImage: UIImageView!
    @IBOutlet weak var lbl_youtime: UILabel!
    @IBOutlet weak var btn_me: UIButton!
    @IBOutlet weak var btn_you: UIButton!
    
    var entity : ChatModel!{
        didSet {
            let url = URL(string: entity.image)
            self.imv_meImage.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            self.imv_youImage.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            let time = getStrDateshort(entity.timestamp)
            lbl_metime.text = time
            lbl_youtime.text = time
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }
}
