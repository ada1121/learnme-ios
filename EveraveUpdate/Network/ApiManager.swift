//
//  ApiManager.swift
//  Everave Update
//
//  Created by Ubuntu on 16/01/2020
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Alamofire
import Foundation
import SwiftyJSON
// ************************************************************************//
                            // Learn me project //
// ************************************************************************//
let BASE_URL = "https://learnme.live/"
let SERVER_URL = "https://learnme.live/api/"

let SUCCESSTRUE = 200
let ALREADY_EXIST = 202
let INVALIDLOGIN = 201
let ALEADY_SENT = 204

let SIGNIN = SERVER_URL + "signin"
let SIGNUP = SERVER_URL + "signup"
//let SOCILASIGNUP = SERVER_URL + "socialsignup"
let FORGOT = SERVER_URL + "forgot"
let RESETPASSWORD = SERVER_URL + "reset_password"
let CHANGEPASSWORD = SERVER_URL + "change_password"
let EDITPROFILE = SERVER_URL + "editProfile"
let UPLOADPROFESSIONALPROFILE = SERVER_URL + "uploadProfessionalProfile"
let GETPROFESSIONALPROFILE = SERVER_URL + "getProfessionalProfile"
let UPLOADSERVICE = SERVER_URL + "uploadService"
let REMOVESERVICE = SERVER_URL + "removeService"
let UPLOADPAYMENT = SERVER_URL + "uploadPayment"
let GETPORTFOLIO = SERVER_URL + "getPortofolio"
let REMOVEPORTFOLIO = SERVER_URL + "removePortofolio"
let UPLOADPORTFOLIO = SERVER_URL + "uploadPortofolio"
let GETONLINESTATE = SERVER_URL + "getOnlineState"
let SETONLINESTATE = SERVER_URL + "setOnlineState"
let GETONOTIFICATIONS = SERVER_URL + "getNotificationState"
let SETNOTIFICATIONS = SERVER_URL + "setNotificationState"
let GETSELLERS = SERVER_URL + "getSellers"
let BOOKING = SERVER_URL + "booking"
let GETBOOKING = SERVER_URL + "getBooking"
let REMOVEBOOKING = SERVER_URL + "removeBooking"
let CANCELBOOKING = SERVER_URL + "cancelBooking"
let ACCEPTBOOKING = SERVER_URL + "acceptBooking"
let COMPLETEBOOKING = SERVER_URL + "completeBooking"
let GETREVIEW = SERVER_URL + "getReview"
let PAYMENTPROCESS = SERVER_URL + "stripe_payment"
let PAYMENTPROCESS_TEST = SERVER_URL + "stripe_payment_test"
let GETSELLERSCATEGORY = SERVER_URL + "getSellersCategory"
let CLOSEACCOUNT = SERVER_URL + "closeAccount"
let SUBMIT_FCM = SERVER_URL + "submitFCM"

struct PARAMS {
    // **********************************************************************//
                            // Everave Update //
    // **********************************************************************//
    static let MESSAGE = "message"
    static let RESULTCODE = "result_code"
    
    
    static let USERDATA = "user_data"
    static let ID = "id"
    static let USER_ID = "user_id"
    static let NAME = "name"
    static let SURNAME = "surname"
    static let USERNAME = "username"
    static let USER_NAME = "user_name"
    static let EMAIL = "email"
    static let PHONE = "phone"
    static let PHOTOURL = "photo_url"
    static let QRCODE = "qr_code"
    static let ISPROMOTED = "is_promoted"
    static let BIRTHDAY = "birthday"
    static let PASSWORD = "password"
    static let PARAM = "param"
    static let TYPE = "type"
    static let PHOTO = "photo"
    static let FILE = "file"
    static let FILE_ARR = "file[]"
    static let FILETYPE_ARR = "filetype[]"
    static let DESCRIPTION = "description"
    static let HADTAGS = "hashtags"
    static let RAVE_LIST = "rave_list"
    static let OWNER_ID = "owner_id"
    static let OWNER_NAME = "owner_name"
    static let MODE = "mode"
    static let PARTICIPANT_CAPACITY = "participant_capacity"
    static let PARTICIPANTS = "participants"
    static let LOCATION_NAME = "location_name"
    static let LATITUDE = "latitude"
    static let LONGITUDE = "longitude"
    static let FEATURE = "feature"
    static let DATE = "date"
    static let IS_CHARGEABLE = "is_chargeable"
    static let PRICE = "price"
    static let ICON_URL = "icon_url"
    static let IS_VISIBLE = "is_visible"
    static let ALLOW_HIDE = "allow_hide"
    static let ALLOW_EDIT = "allow_edit"
    static let ALLOW_INVITE = "allow_invite"
    static let ALLOW_PROMOTE = "allow_promote"
    static let ALLOW_SEE_CHAT = "allow_see_chat"
    static let CREATED_AT = "created_at"
    static let UPDATED_AT = "updated_at"
    static let JOINED = "joined"
    static let FILE_URL = "file_url"
    static let FROM = "from"
    static let TO = "to"
    static let IS_FAVORITE = "is_favorite"
    static let MEDIA_LIST = "media_list"
    static let RAVE_ID = "rave_id"
    static let ABOUT_ME = "about_me"
    static let STATUS = "status"
    static let JOINED_COUNT = "joined_count"
    static let ORGANIZED_COUNT = "organized_count"
    static let FRIEND_COUNT = "friend_count"
    static let FRIEND_LIST = "friend_list"
    static let USER_LIST = "user_list"
    static let FRIEND_ID = "friend_id"
    static let SENT = "sent"
    static let RECEIVED = "received"
    static let ACCEPTED = "accepted"
    static let ICON_PICTURE = "icon_picture"
    static let OWNER_PICTURE = "owner_picture"
    static let PINCODE = "pincode"
    
    // ========== learn me ========//
    static let SELLER = "seller"
    static let BUYER = "buyer"
    static let FB_ID = "fb_id"
    static let GOOGLE_ID = "google_id"
    static let FIRST_NAME = "first_name"
    static let LAST_NAME = "last_name"
    static let DOB = "dob"
    static let PICTURE = "picture"
    static let GENDER = "gender"
    static let SSN = "ssn"
    static let PROFILE_COMPLETED = "profile_completed"
    static let APPROVE = "approve"
    static let ONLINE = "online"
    static let TOKEN = "token"
    static let EMAILORPHONE = "emailorphone"
    static let DEVICE_TOKEN = "device_token"
    static let USER_INFO = "user_info"
    static let IMAGE = "image"
    static let RATING = "rating"
    static let STATE = "state"
    static let TIMEZONE = "timezone"
    static let LOGOUT = "logout"
    static let TOOLS = "tools"
    static let LANGUAGES = "languages"
    static let COUNT = "count"
}

class ApiManager {
    
    class func login(emailorphone : String, password : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let type = UserDefault.getString(key: PARAMS.TYPE,defaultValue: "buyer")
        
        let params = [PARAMS.EMAILORPHONE : emailorphone, PARAMS.PASSWORD : password, PARAMS.TYPE: type!, PARAMS.DEVICE_TOKEN: deviceTokenString ] as [String : Any]
        
        Alamofire.request(SIGNIN, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                
                //print("userinformation =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                let user_data = dict[PARAMS.USER_INFO].object
                let userdata = JSON(user_data)
                /*user?.clearUserInfo()
                user = UserModel(userdata)
                user?.saveUserInfo()
                user?.loadUserInfo()*/
                if status == SUCCESSTRUE {
                    completion(true, userdata)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getSellers( completion :  @escaping (_ success: Bool, _ response : Any?, _ badge: String?) -> ()) {
        let params = [PARAMS.USER_ID : "\(user?.id ?? 0)" ] as [String : Any]
        Alamofire.request(GETSELLERS, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let booking_badge =  dict["accept_badge"].stringValue
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                
                //let seller_infoJSON = JSON(seller_info as Any)
                if status == SUCCESSTRUE {
                    completion(true,dict, booking_badge)
                } else {
                    completion(false, status, nil)
                }
            }
        }
    }
    
    class func getBooking( completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USER_ID : "\(user?.id ?? 0)", PARAMS.TYPE: user?.type ?? PARAMS.BUYER] as [String : Any]
        Alamofire.request(GETBOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                //let booking_infoJSON = dict["booking_info"].arrayObject
                //let booking_info = JSON(booking_infoJSON as Any)
                if status == SUCCESSTRUE {
                    completion(true,dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getReview( seller_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["seller_id" : seller_id] as [String : Any]
        Alamofire.request(GETREVIEW, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                //let booking_infoJSON = dict["booking_info"].arrayObject
                //let booking_info = JSON(booking_infoJSON as Any)
                if status == SUCCESSTRUE {
                    completion(true,dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
   
    class func getPortofolio( user_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETPORTFOLIO, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                //let booking_infoJSON = dict["booking_info"].arrayObject
                //let booking_info = JSON(booking_infoJSON as Any)
                if status == SUCCESSTRUE {
                    completion(true,dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    
    class func getProfessionalProfile( user_id: String,completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id" : user_id] as [String : Any]
        Alamofire.request(GETPROFESSIONALPROFILE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,dict)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func removePortofolio(id: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["id" : id] as [String : Any]
        Alamofire.request(REMOVEPORTFOLIO, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func uploadPortofolio( image : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = UPLOADPORTFOLIO
        let user_id = "\(user?.id ?? 0)"
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                multipartFormData.append( user_id.data(using:String.Encoding.utf8)!, withName: "user_id")
                multipartFormData.append(URL(fileURLWithPath: image), withName: "image")
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                        upload.responseJSON { response in
                            switch response.result {
                                case .failure: completion(false, nil)
                                case .success(let data):
                                    let dict = JSON(data)
                                    let status = dict[PARAMS.RESULTCODE].intValue
                                    if status == SUCCESSTRUE {
                                        completion(true, status)
                                    } else {
                                        completion(false, status)
                                    }
                            }
                        }
                    case .failure( _):
                        completion(false, nil)
                }
            }
        )
    }
    
    class func uploadPayment( firstName : String,lastName: String, accountNumber: String, routingNumber: String, accountType: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["user_id" : "\(user?.id ?? 0)", "first_name" : firstName, "last_name": lastName, "account_number": accountNumber, "routing_number" : routingNumber, "account_type": accountType ] as [String : Any]
        
        Alamofire.request(UPLOADPAYMENT, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                if status == SUCCESSTRUE {
                    completion(true, status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func removeService(id: String,item_name: String,  completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["id": id, "item": item_name] as [String : Any]
        Alamofire.request(REMOVESERVICE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func uploadService(name: String,t_15: String,t_30: String,t_45: String,t_60: String,  completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id": "\(user!.id ?? 0)", "name": name, "t_15": t_15, "t_30": t_30, "t_45": t_45, "t_60": t_60 ] as [String : Any]
        Alamofire.request(UPLOADSERVICE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func uploadProfessionalProfile(des: String,languages: String,tools: String,mon: String,tue: String,wed: String,thr: String,fri: String,sat: String,sun: String,timezone: String,  completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id": "\(user!.id ?? 0)", "description": des,"languages": languages,"tools": tools, "mon": mon, "tue": tue, "wed": wed, "thr": thr, "fri": fri, "sat": sat, "sun": sun, "timezone": timezone  ] as [String : Any]
        Alamofire.request(UPLOADPROFESSIONALPROFILE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                let approve = dict["approve"].stringValue
                if status == SUCCESSTRUE {
                    completion(true,approve)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func editProfile( photo : String ,first_name : String, last_name : String, user_name : String, email : String, phone : String, dob : String, gender : String, ssn : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = EDITPROFILE
        let id = "\(user?.id ?? 0)"
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if !photo.isEmpty{
                    multipartFormData.append(URL(fileURLWithPath: photo), withName: "photo")
                }
                multipartFormData.append( id.data(using:.utf8)!, withName: "id")
                multipartFormData.append( first_name.data(using:.utf8)!, withName: "first_name")
                multipartFormData.append( last_name.data(using:.utf8)!, withName: "last_name")
                multipartFormData.append( user_name.data(using:.utf8)!, withName: "user_name")
                multipartFormData.append( email.data(using:.utf8)!, withName: "email")
                multipartFormData.append( phone.data(using:.utf8)!, withName: "phone")
                multipartFormData.append( dob.data(using:.utf8)!, withName: "dob")
                multipartFormData.append( gender.data(using:.utf8)!, withName: "gender")
                multipartFormData.append( ssn.data(using:.utf8)!, withName: "ssn")
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                            case .failure: completion(false, nil)
                            case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.RESULTCODE].intValue
                            let picture = dict["picture"].stringValue
                            if status == SUCCESSTRUE {
                                completion(true, picture)
                            } else {
                                completion(false, status)
                            }
                        }
                    }
                    case .failure( _):
                    completion(false, nil)
                }
            }
        )
    }
    
    class func getOnlineState(completion :  @escaping (_ success: Bool, _ state : String?, _ rating: Float?) -> ()) {
        let params = ["user_id": "\(user!.id ?? 0)"] as [String : Any]
        Alamofire.request(GETONLINESTATE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                    completion(false, nil, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                let state = dict["state"].stringValue
                let rating = dict["rating"].floatValue
                if status == SUCCESSTRUE {
                    completion(true,state,rating)
                } else {
                    completion(false, state, rating)
                }
            }
        }
    }
    
    class func setOnlineState(state: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id": "\(user!.id ?? 0)" , "state": state] as [String : Any]
        Alamofire.request(SETONLINESTATE, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func booking(seller_id: String,seller_name: String,seller_picture: String,service_name: String,service_time: String,service_cost: String,service_date: String,des: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["buyer_id": "\(user!.id ?? 0)" , "seller_id": seller_id, "buyer_name":user!.user_name ?? "","seller_name": seller_name,"buyer_picture":user!.picture ?? "" , "seller_picture":seller_picture ,"service_name": service_name,"service_time": service_time,"service_cost":service_cost , "service_date":service_date , "description":des ,"state": "0","rating": "0", "possible":"0"] as [String : Any]
        
        Alamofire.request(BOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else if status == 201{
                    completion(true, status)
                }else{
                    completion(false, status)
                }
            }
        }
    }
    
    class func paymentProcess(stripe_token: String,booking_id: String,amount: String,email: String,seller_id: String,seller_name: String,service_name: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["stripe_token": stripe_token,"booking_id": booking_id,"amount": amount,"email": email,"buyer_id": "\(user!.id ?? 0)" ,"buyer_name": "\(user!.user_name ?? "")" , "seller_id": seller_id,"seller_name": seller_name,"service_name":service_name] as [String : Any]
        
        var paymentURL = ""
        if paymentMode == mode.live.rawValue{
            paymentURL = PAYMENTPROCESS
            
        }else if paymentMode == mode.test.rawValue{
            paymentURL = PAYMENTPROCESS_TEST
        }
        
        Alamofire.request(paymentURL, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else{
                    completion(false, status)
                }
            }
        }
    }
    
    class func removeBooking(id: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["id": id] as [String : Any]
        Alamofire.request(REMOVEBOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func acceptBooking(id: String,state: String,buyer_id: String,buyer_name: String,seller_name: String,service_name: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["id": id,"state": state,"buyer_id": buyer_id,"buyer_name": buyer_name,"seller_name": seller_name,"service_name": service_name ] as [String : Any]
        Alamofire.request(ACCEPTBOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func completeBooking(id: String,review: String,rating: String,seller_id: String,buyer_id: String,buyer_name: String,buyer_picture: String,service_date: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["id": id,"state": "3","review": review,"rating": rating,"seller_id": seller_id,"buyer_id": buyer_id,"buyer_name": buyer_name,"buyer_picture": buyer_picture,"service_date": service_date ] as [String : Any]
        
        Alamofire.request(COMPLETEBOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func cancelBooking(id: String,reason: String,state: String,service_name: String,service_cost: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        
        let params = ["id": id,"reason": reason,"state": state,"service_name": service_name,"service_cost": service_cost] as [String : Any]
        
        Alamofire.request(CANCELBOOKING, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func signup( photo : String ,first_name : String, last_name : String, user_name : String, email : String, phone : String, dob : String, gender : String, ssn : String, password : String,type : String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let requestURL = SIGNUP
        Alamofire.upload(
            multipartFormData: { multipartFormData in
                if !photo.isEmpty{
                    multipartFormData.append(URL(fileURLWithPath: photo), withName: "photo")
                }
                
                multipartFormData.append( first_name.data(using:.utf8)!, withName: "first_name")
                multipartFormData.append( last_name.data(using:.utf8)!, withName: "last_name")
                multipartFormData.append( user_name.data(using:.utf8)!, withName: "user_name")
                multipartFormData.append( email.data(using:.utf8)!, withName: "email")
                multipartFormData.append( phone.data(using:.utf8)!, withName: "phone")
                multipartFormData.append( dob.data(using:.utf8)!, withName: "dob")
                multipartFormData.append( gender.data(using:.utf8)!, withName: "gender")
                multipartFormData.append( ssn.data(using:.utf8)!, withName: "ssn")
                multipartFormData.append( password.data(using:.utf8)!, withName: "password")
                multipartFormData.append( deviceTokenString.data(using:.utf8)!, withName: "device_token")
                multipartFormData.append( type.data(using:.utf8)!, withName: "type")
            },
            to: requestURL,
            encodingCompletion: { encodingResult in
                switch encodingResult {
                    case .success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                            case .failure: completion(false, nil)
                            case .success(let data):
                            let dict = JSON(data)
                            let status = dict[PARAMS.RESULTCODE].intValue
                            
                            if status == SUCCESSTRUE {
                                completion(true, dict)
                            } else if(status == 201)  {
                                completion(false, status)
                            } else if(status == 202)  {
                                completion(false, status)
                            } else{
                                completion(false, status)
                            }
                        }
                    }
                    case .failure( _):
                    completion(false, nil)
                }
            }
        )
    }
    
    class func changePwd(password: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["user_id": "\(user!.id ?? 0)", "password": password] as [String : Any]
        Alamofire.request(CHANGEPASSWORD, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func forgot(phone: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["phone": phone] as [String : Any]
        Alamofire.request(FORGOT, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func resetPassword(phone: String,password: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["phone": phone, "password": password] as [String : Any]
        Alamofire.request(RESETPASSWORD, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let status = dict[PARAMS.RESULTCODE].intValue// 0,1,2
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func getSellersCategory( _ category: String, completion :  @escaping (_ success: Bool, _ response : Any?, _ badge: String?) -> ()) {
        let params = [PARAMS.USER_ID : "\(user?.id ?? 0)", "category": category] as [String : Any]
        Alamofire.request(GETSELLERSCATEGORY, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil, nil)
                case .success(let data):
                let dict = JSON(data)
                //print("sellers =====> ",dict)
                let booking_badge =  dict["accept_badge"].stringValue
                let status = dict[PARAMS.RESULTCODE].intValue // 0,1,2
                
                //let seller_infoJSON = JSON(seller_info as Any)
                if status == SUCCESSTRUE {
                    completion(true,dict, booking_badge)
                } else {
                    completion(false, status, nil)
                }
            }
        }
    }
    
    class func closeAccount( completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = [PARAMS.USER_ID : "\(user?.id ?? 0)" ] as [String : Any]
        Alamofire.request(CLOSEACCOUNT, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.RESULTCODE].intValue
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
    
    class func submitFCM( friend_id: String, notitext: String, completion :  @escaping (_ success: Bool, _ response : Any?) -> ()) {
        let params = ["friend_id" : friend_id, "sender_id" : "\(user?.id ?? 0)", "notitext" : notitext ] as [String : Any]
        Alamofire.request(SUBMIT_FCM, method:.post, parameters:params)
        .responseJSON { response in
            switch response.result {
                case .failure:
                completion(false, nil)
                case .success(let data):
                let dict = JSON(data)
                let status = dict[PARAMS.RESULTCODE].intValue
                if status == SUCCESSTRUE {
                    completion(true,status)
                } else {
                    completion(false, status)
                }
            }
        }
    }
}
