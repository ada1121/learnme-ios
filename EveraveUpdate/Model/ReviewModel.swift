//
//  ReviewModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReviewModel: NSObject {
    
    var id : String?
    var seller_id : String?
    var buyer_id : String?
    var buyer_name : String?
    var buyer_picture : String?
    var service_date : Int64!
    var review : String?
    var rating : Float!
    
   override init() {
       super.init()
         id = ""
         seller_id = ""
         buyer_id = ""
         buyer_name = ""
         buyer_picture = ""
         service_date = 0
         review = ""
         rating = 0
   }
    
    init(id : String?, seller_id : String?,buyer_id : String?,buyer_name : String?,buyer_picture : String?,service_date : Int64!,review : String?,rating : Float!){
        
        self.id = id
        self.seller_id = seller_id
        self.buyer_id = buyer_id
        self.buyer_name = buyer_name
        self.buyer_picture = buyer_picture
        self.service_date = service_date
        self.review = review
        self.rating = rating
        
    }
    
    init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.seller_id  = json["seller_id"].stringValue
        self.buyer_id = json["buyer_id"].stringValue
        self.buyer_name = json["buyer_name"].stringValue
        self.buyer_picture = json["buyer_picture"].stringValue
        self.service_date = json["service_date"].int64Value
        self.review = json["review"].stringValue
        self.rating = json["rating"].floatValue
    }
}
