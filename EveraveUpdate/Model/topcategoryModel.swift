//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class RaveModel: NSObject {
    
        var id: String?
        var owner_id: String?
        var owner_name: String?
        var name: String?
        var mode: String?
        var participant_capacity: String?
        var participants: String?
        var location_name: String?
        var latitude: String?
        var longitude: String?
        var feature: String?
        var date: String?
        var from: String?
        var to: String?
        var is_chargeable: String?
        var price: String?
        var icon_url: String?
        var descriptionn: String?
        var is_visible: String?
        var allow_hide: String?
        var allow_edit: String?
        var allow_invite: String?
        var allow_promote: String?
        var allow_see_chat: String?
        var created_at: String?
        var updated_at: String?
        var joined: String?
        var is_favorite: String?
        var owner_picture: String?
        var media_list = [Media_list_Model]()
    
       override init() {
           super.init()
            id = "0"
            owner_id = ""
            owner_name = ""
            name = ""
            mode = ""
            participant_capacity = ""
            participants = ""
            location_name = ""
            latitude = ""
            longitude = ""
            feature = ""
            date = ""
            from = ""
            to = ""
            is_chargeable = ""
            price = ""
            icon_url = ""
            descriptionn = ""
            is_visible = ""
            allow_hide = ""
            allow_edit = ""
            allow_invite = ""
            allow_promote = ""
            allow_see_chat = ""
            created_at = ""
            updated_at = ""
            joined = ""
            is_favorite = ""
            owner_picture = ""
            media_list = [Media_list_Model()]
       }
       
    init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.owner_id = json[PARAMS.OWNER_ID].stringValue
        self.owner_name = json[PARAMS.OWNER_NAME].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.mode = json[PARAMS.MODE].stringValue
        self.participant_capacity = json[PARAMS.PARTICIPANT_CAPACITY].stringValue
        self.participants = json[PARAMS.PARTICIPANTS].stringValue
        self.location_name = json[PARAMS.LOCATION_NAME].stringValue
        self.latitude = json[PARAMS.LATITUDE].stringValue
        self.longitude = json[PARAMS.LONGITUDE].stringValue
        self.feature = json[PARAMS.FEATURE].stringValue
        
        let timestampdate = json[PARAMS.DATE].stringValue
        self.date = getStrDate(timestampdate)
        
        self.from = json[PARAMS.FROM].stringValue
        self.to = json[PARAMS.TO].stringValue
        self.is_chargeable = json[PARAMS.IS_CHARGEABLE].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.icon_url = json[PARAMS.ICON_URL].stringValue
        self.descriptionn = json[PARAMS.DESCRIPTION].stringValue
        self.is_visible = json[PARAMS.IS_VISIBLE].stringValue
        self.allow_hide = json[PARAMS.ALLOW_HIDE].stringValue
        self.allow_edit = json[PARAMS.ALLOW_EDIT].stringValue
        self.allow_invite = json[PARAMS.ALLOW_INVITE].stringValue
        self.allow_promote = json[PARAMS.ALLOW_PROMOTE].stringValue
        self.allow_see_chat = json[PARAMS.ALLOW_SEE_CHAT].stringValue
        self.created_at = json[PARAMS.CREATED_AT].stringValue
        self.updated_at = json[PARAMS.UPDATED_AT].stringValue
        self.joined = json[PARAMS.JOINED].stringValue
        self.is_favorite = json[PARAMS.IS_FAVORITE].stringValue
        self.owner_picture = json[PARAMS.OWNER_PICTURE].stringValue
        let media_List = json[PARAMS.MEDIA_LIST].arrayObject
        for one in media_List ?? [Media_list_Model()] {
            let jsonone = JSON(one as Any)
            let miniModel = Media_list_Model(jsonone)
            self.media_list.append(miniModel)
        }
    }
}

class Media_list_Model: NSObject {
    var id : String?
    var type:String?
    var file_url:String?
    override init(){
        super.init()
        id = ""
        type = ""
        file_url = ""
    }
    init(_ json :JSON){
        self.id = json[PARAMS.ID].stringValue
        self.type = json[PARAMS.TYPE].stringValue
        self.file_url = json[PARAMS.FILE_URL].stringValue
    }
}

class topcategoryModel: NSObject {
        var id : Int!
        var title: String?
        var image: String?
    
       override init() {
           super.init()
            id = -1
            title = ""
            image = ""
       }
       
    /*init(_ json : JSON) {
        self.id = json[PARAMS.ID].intValue
        self.title = json[PARAMS.TITLE].stringValue
        self.image = json[PARAMS.IMAGE].stringValue
    }*/
    init(id: Int, title: String, image:String){
        self.id = id
        self.title = title
        self.image = image
    }
}
