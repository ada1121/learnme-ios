//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class LoginInfo: NSObject {
    var email: String?
    var password: String?
    var type: String?
    override init() {
        super.init()
        email = ""
        password = ""
        type = ""
    }
    init(_ email: String, pwd: String, type: String){
        self.email = email
        self.password = pwd
        self.type = type
    }
    var isValid: Bool {
        return email != nil && email != "" && password != nil && password != ""
    }
    func loadInfo() {
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        password = UserDefault.getString(key: PARAMS.PASSWORD, defaultValue: "")
        type = UserDefault.getString(key: PARAMS.TYPE, defaultValue: "buyer")
   }
    func saveInfo() {
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.PASSWORD, value: password)
        UserDefault.setString(key: PARAMS.TYPE, value: type)
    }
    func clearInfo() {
        email = ""
        password = ""
        type = ""
        UserDefault.setString(key: PARAMS.EMAIL, value: nil)
        UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
        //UserDefault.setString(key: PARAMS.TYPE, value: nil)
    }
    func updateInfo(info: LoginInfo) {
        email = info.email
        password = info.password
        type = info.type
    }
}

class UserModel: NSObject {
    
        var id : Int!
        var fb_id : String?
        var google_id : String?
        var first_name : String?
        var last_name : String?
        var user_name : String?
        var des : String?
        var dob : String?
        var gender : String?
        var ssn : String?
        var email : String?
        var phone : String?
        var password : String?
        var picture : String?
        var profile_completed : String?// seller, buyer
        var approve : String?   // for seller
        // getting local database
        var type : String?   // seller, buyer
        var online : String?
        var token : String?
        var status : String?
    
       override init() {
           super.init()
            id = 0
            fb_id = ""
            google_id = ""
            first_name = ""
            last_name = ""
            user_name = ""
            email = ""
            phone = ""
            dob = ""
            picture = ""
            password = ""
            des = ""
            gender = ""
            ssn = ""
            type = ""   // seller, buyer
            profile_completed = "0"   // seller, buyer
            approve = "0"   // for seller
            online = "off"
            token = ""
            status = ""
       }
       
    init(_ json : JSON) {
        self.id = json[PARAMS.USER_ID].intValue
        self.fb_id = json[PARAMS.FB_ID].stringValue
        self.google_id = json[PARAMS.GOOGLE_ID].stringValue
        self.first_name = json[PARAMS.FIRST_NAME].stringValue
        self.last_name = json[PARAMS.LAST_NAME].stringValue
        self.user_name = json[PARAMS.USER_NAME].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.dob = json[PARAMS.DOB].stringValue
        self.picture = json[PARAMS.PICTURE].stringValue
        //self.password = json[PARAMS.PASSWORD].stringValue
        self.des = json[PARAMS.DESCRIPTION].stringValue
        self.gender = json[PARAMS.GENDER].stringValue
        self.ssn = json[PARAMS.SSN].stringValue
        self.type = UserDefault.getString(key: PARAMS.TYPE)  // seller, buyer
        self.profile_completed = json[PARAMS.PROFILE_COMPLETED].stringValue   // seller, buyer
        self.approve = json[PARAMS.APPROVE].stringValue   // for seller
        self.online = json[PARAMS.ONLINE].stringValue
        self.token = json[PARAMS.TOKEN].stringValue
        self.status = json[PARAMS.STATUS].stringValue
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
       return id != nil && id != 0
   }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
        
        /* description" : "Master Barber with experience with all types and lengths of hair, and can walk you through this virtual experience with ease! \n------------------------------------------\nTools Needed for Session\n------------------------------------------\n-Clippers, \n\n-Liners, \n\n-Guards 1-10 \n\n-Electric Razor\n\n-        \nRates\n-------------------------\n-30 Minutes\/$15 - Line up",
        "first_name" : "Steve",
        "dob" : "Jan\/06\/2000",
        "profile_completed" : "1",
        "email" : "bguess516@gmail.com",
        "user_name" : "Wavyq",
        "user_id" : "18",
        "fb_id" : "",
        "last_name" : "Steve",
        "gender" : "",
        "picture" : "https:\/\/learnme.live\/uploadfiles\/userphoto\/1593206518615.png",
        "google_id" : "",
        "ssn" : "2321",
        "phone" : "+17144648393",
        "approve"*/
        
        id = UserDefault.getInt(key: PARAMS.USER_ID, defaultValue: 0)
        fb_id = UserDefault.getString(key: PARAMS.FB_ID, defaultValue: "")
        google_id = UserDefault.getString(key: PARAMS.GOOGLE_ID, defaultValue: "")
        first_name = UserDefault.getString(key: PARAMS.FIRST_NAME, defaultValue: "")
        last_name = UserDefault.getString(key: PARAMS.LAST_NAME, defaultValue: "")
        user_name = UserDefault.getString(key: PARAMS.USER_NAME, defaultValue: "")
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        phone = UserDefault.getString(key: PARAMS.PHONE, defaultValue: "")
        dob = UserDefault.getString(key: PARAMS.DOB, defaultValue: "")
        picture = UserDefault.getString(key: PARAMS.PICTURE, defaultValue: "")
        password = UserDefault.getString(key: PARAMS.PASSWORD, defaultValue: "")
        des = UserDefault.getString(key: PARAMS.DESCRIPTION, defaultValue: "")
        gender = UserDefault.getString(key: PARAMS.GENDER, defaultValue: "")
        ssn = UserDefault.getString(key: PARAMS.SSN, defaultValue: "")
        type = UserDefault.getString(key: PARAMS.TYPE, defaultValue: "buyer")   // seller, buyer
        profile_completed = UserDefault.getString(key: PARAMS.PROFILE_COMPLETED, defaultValue: "0")   // seller, buyer
        approve = UserDefault.getString(key: PARAMS.APPROVE, defaultValue: "0")   // for seller
        online = UserDefault.getString(key: PARAMS.ONLINE, defaultValue: "off")
        token = UserDefault.getString(key: PARAMS.TOKEN, defaultValue: "")
        status = UserDefault.getString(key: PARAMS.STATUS, defaultValue: "")
        
    }
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setInt(key: PARAMS.USER_ID, value: id)
        UserDefault.setString(key: PARAMS.FB_ID, value: fb_id)
        UserDefault.setString(key: PARAMS.GOOGLE_ID, value: google_id)
        UserDefault.setString(key: PARAMS.FIRST_NAME, value: first_name)
        UserDefault.setString(key: PARAMS.LAST_NAME, value: last_name)
        UserDefault.setString(key: PARAMS.USER_NAME, value: user_name)
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.PHONE, value: phone)
        UserDefault.setString(key: PARAMS.DOB, value: dob)
        UserDefault.setString(key: PARAMS.PICTURE, value: picture)
        UserDefault.setString(key: PARAMS.PASSWORD, value: password)
        UserDefault.setString(key: PARAMS.DESCRIPTION, value: des)
        UserDefault.setString(key: PARAMS.GENDER, value: gender)
        UserDefault.setString(key: PARAMS.SSN, value: ssn)
        //UserDefault.setString(key: PARAMS.TYPE, value: type)   // seller, buyer
        UserDefault.setString(key: PARAMS.PROFILE_COMPLETED, value: profile_completed)   // seller, buyer
        UserDefault.setString(key: PARAMS.APPROVE, value: approve)   // for seller
        UserDefault.setString(key: PARAMS.ONLINE, value: online)
        UserDefault.setString(key: PARAMS.TOKEN, value: token)
        UserDefault.setString(key: PARAMS.STATUS, value: status)
    }
    // Clear save user credential
    func clearUserInfo() {
        
        id = 0
        fb_id = ""
        google_id = ""
        first_name = ""
        last_name = ""
        user_name = ""
        email = ""
        phone = ""
        dob = ""
        picture = ""
        password = ""
        des = ""
        gender = ""
        ssn = ""
        type = ""   // seller, buyer
        profile_completed = "0"   // seller, buyer
        approve = "0"   // for seller
        online = "off"
        token = ""
        status = ""
        
       UserDefault.setInt(key: PARAMS.USER_ID, value: 0)
       UserDefault.setString(key: PARAMS.FB_ID, value: nil)
       UserDefault.setString(key: PARAMS.GOOGLE_ID, value: nil)
       UserDefault.setString(key: PARAMS.FIRST_NAME, value: nil)
       UserDefault.setString(key: PARAMS.LAST_NAME, value: nil)
       UserDefault.setString(key: PARAMS.USER_NAME, value: nil)
       UserDefault.setString(key: PARAMS.EMAIL, value: nil)
       UserDefault.setString(key: PARAMS.PHONE, value: nil)
       UserDefault.setString(key: PARAMS.DOB, value: nil)
       UserDefault.setString(key: PARAMS.PICTURE, value: nil)
       //UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
       UserDefault.setString(key: PARAMS.DESCRIPTION, value: nil)
       UserDefault.setString(key: PARAMS.GENDER, value: nil)
       UserDefault.setString(key: PARAMS.SSN, value: nil)
       //UserDefault.setString(key: PARAMS.TYPE, value: nil)   // seller, buyer
       UserDefault.setString(key: PARAMS.PROFILE_COMPLETED, value: nil)   // seller, buyer
       UserDefault.setString(key: PARAMS.APPROVE, value: nil)   // for seller
       UserDefault.setString(key: PARAMS.ONLINE, value: nil)
       UserDefault.setString(key: PARAMS.TOKEN, value: nil)
       UserDefault.setString(key: PARAMS.STATUS, value: nil)
    }
    
    func updateUserInfo(user: UserModel) {
        id = user.id
        fb_id = user.fb_id
        google_id = user.google_id
        first_name = user.first_name
        last_name = user.last_name
        user_name = user.user_name
        email = user.email
        phone = user.phone
        dob = user.dob
        picture = user.picture
        password = user.password
        des = user.description
        gender = user.gender
        ssn = user.ssn
        type = user.type   // seller, buyer
        profile_completed = user.profile_completed // seller, buyer
        approve = user.approve   // for seller
        online = user.online
        token = user.token
        status = user.status
    }
}
