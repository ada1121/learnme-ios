//
//  PortfolioModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class PortfolioModel: NSObject {
    
        var id: String?
        var image: String?
        var imageFile: UIImage?
    
       override init() {
           super.init()
            id = "0"
            image = ""
       }
       
    init(id: String?, image: String?) {
        self.id = id
        self.image = image
    }
    init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.image = json[PARAMS.IMAGE].stringValue
    }
    init(_ imagefile: UIImage) {
        self.imageFile = imagefile
    }
}

