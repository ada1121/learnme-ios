//
//  SellerModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class SellerModel: NSObject {
    
    var id : String?
    var first_name : String?
    var last_name : String?
    var user_name : String?
    var picture : String?
    var des : String?
    var languages : String?
    var tools : String?
    var state : String?
    var rating : Float!
    var timezone : String?
    var count : String?
    var availableTimeModel : AvailableTimeModel?
    var portfolioList = [PortfolioModel]()
    var serviceModelList = [ServiceModel]()
     
       override init() {
           super.init()
            id = ""
            first_name = ""
            last_name = ""
            user_name = ""
            picture = ""
            des = ""
            languages = ""
            tools = ""
            state = "online"
            rating = 0
            timezone = ""
            count = "0"
            portfolioList = [PortfolioModel()]
            serviceModelList = [ServiceModel()]
       }
    
    init(id : String?, first_name : String?, last_name : String? ,user_name : String?, picture : String? ,des : String?,languages : String?,tools : String?, state : String?, rating : Float!,count: String?,timezone : String?,availableTimeModel : AvailableTimeModel?,portfolioList : [PortfolioModel], serviceModelList: [ServiceModel] ){
        
        self.id = id
        self.first_name = first_name
        self.last_name = last_name
        self.user_name = user_name
        self.picture = picture
        self.des = des
        self.languages = languages
        self.tools = tools
        self.state = state
        self.rating = rating
        self.count = count
        self.timezone = timezone
        self.portfolioList = portfolioList
        self.serviceModelList = serviceModelList
    }
       
    /*init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.owner_id = json[PARAMS.OWNER_ID].stringValue
        self.owner_name = json[PARAMS.OWNER_NAME].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.mode = json[PARAMS.MODE].stringValue
        self.participant_capacity = json[PARAMS.PARTICIPANT_CAPACITY].stringValue
        self.participants = json[PARAMS.PARTICIPANTS].stringValue
        self.location_name = json[PARAMS.LOCATION_NAME].stringValue
        self.latitude = json[PARAMS.LATITUDE].stringValue
        self.longitude = json[PARAMS.LONGITUDE].stringValue
        self.feature = json[PARAMS.FEATURE].stringValue
        
        let timestampdate = json[PARAMS.DATE].stringValue
        self.date = getStrDate(timestampdate)
        
        self.from = json[PARAMS.FROM].stringValue
        self.to = json[PARAMS.TO].stringValue
        self.is_chargeable = json[PARAMS.IS_CHARGEABLE].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.icon_url = json[PARAMS.ICON_URL].stringValue
        self.descriptionn = json[PARAMS.DESCRIPTION].stringValue
        self.is_visible = json[PARAMS.IS_VISIBLE].stringValue
        self.allow_hide = json[PARAMS.ALLOW_HIDE].stringValue
        self.allow_edit = json[PARAMS.ALLOW_EDIT].stringValue
        self.allow_invite = json[PARAMS.ALLOW_INVITE].stringValue
        self.allow_promote = json[PARAMS.ALLOW_PROMOTE].stringValue
        self.allow_see_chat = json[PARAMS.ALLOW_SEE_CHAT].stringValue
        self.created_at = json[PARAMS.CREATED_AT].stringValue
        self.updated_at = json[PARAMS.UPDATED_AT].stringValue
        self.joined = json[PARAMS.JOINED].stringValue
        self.is_favorite = json[PARAMS.IS_FAVORITE].stringValue
        self.owner_picture = json[PARAMS.OWNER_PICTURE].stringValue
        let media_List = json[PARAMS.MEDIA_LIST].arrayObject
        for one in media_List ?? [Media_list_Model()] {
            let jsonone = JSON(one as Any)
            let miniModel = Media_list_Model(jsonone)
            self.media_list.append(miniModel)
        }
    }*/
}
