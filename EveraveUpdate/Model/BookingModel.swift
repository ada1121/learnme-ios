//
//  BookingModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class BookingModel: NSObject {
    
    var id : String?
    var buyer_id : String?
    var seller_id : String?
    var buyer_name : String?
    var seller_name : String?
    var buyer_picture : String?
    var seller_picture : String?
    var service_name : String?
    var service_time : String?
    var service_cost : String?
    var service_date : Int64 = 0
    var des : String?
    var state : String?
    var reason : String?
    var review : String?
    var rating : Float = 0
    
    override init() {
        super.init()
        
        self.id = ""
        self.buyer_id = ""
        self.seller_id = ""
        self.buyer_name = ""
        self.seller_name = ""
        self.buyer_picture = ""
        self.seller_picture = ""
        self.service_name = ""
        self.service_time = ""
        self.service_cost = ""
        self.service_date = 0
        self.des = ""
        self.state = ""
        self.reason = ""
        self.review = ""
        self.rating = 0
        
    }
    init(id : String?, buyer_id : String?, seller_id : String?, buyer_name : String?, seller_name : String?, buyer_picture : String?, seller_picture : String?, service_name : String?, service_time : String?, service_cost : String?, service_date : Int64!, des : String?, state : String?, reason : String?, review : String?, rating : Float!) {
        
        self.id = id
        self.buyer_id = buyer_id
        self.seller_id = seller_id
        self.buyer_name = buyer_name
        self.seller_name = seller_name
        self.buyer_picture = buyer_picture
        self.seller_picture = seller_picture
        self.service_name = service_name
        self.service_time = service_time
        self.service_cost = service_cost
        self.service_date = service_date
        self.des = des
        self.state = state
        self.reason = reason
        self.review = review
        self.rating = rating
    }
       
    init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.buyer_id = json["buyer_id"].stringValue
        self.seller_id = json["seller_id"].stringValue
        self.buyer_name = json["buyer_name"].stringValue
        self.seller_name = json["seller_name"].stringValue
        self.buyer_picture = json["buyer_picture"].stringValue
        self.seller_picture = json["seller_picture"].stringValue
        self.service_name = json["service_name"].stringValue
        self.service_time = json["service_time"].stringValue
        self.service_cost = json["service_cost"].stringValue
        self.service_date = json["service_date"].int64Value
        self.des = json["description"].stringValue
        self.state = json["state"].stringValue
        self.reason = json["reason"].stringValue
        self.review = json["review"].stringValue
        self.rating = json["rating"].floatValue
    }
}
