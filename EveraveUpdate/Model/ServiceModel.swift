//
//  ServiceModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class ServiceModel: NSObject {
    
    var id : String?
    var name : String?
    var t_15 : String?
    var t_30 : String?
    var t_45 : String?
    var t_60 : String?
    var timePriceModelList = [TimePriceModel]()
    
       override init() {
           super.init()
            id = ""
            name = ""
            t_15 = ""
            t_30 = ""
            t_45 = ""
            t_60 = ""
       }
    
    init(id : String? ,name : String? ,t_15 : String? ,t_30 : String? ,t_45 : String? ,t_60 : String?, timepricemodelList : [TimePriceModel]){
        self.id = id
        self.name = name
        self.t_15 = t_15
        self.t_30 = t_30
        self.t_45 = t_45
        self.t_60 = t_60
        self.timePriceModelList = timepricemodelList
    }
       
    /*init(_ json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.owner_id = json[PARAMS.OWNER_ID].stringValue
        self.owner_name = json[PARAMS.OWNER_NAME].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.mode = json[PARAMS.MODE].stringValue
        self.participant_capacity = json[PARAMS.PARTICIPANT_CAPACITY].stringValue
        self.participants = json[PARAMS.PARTICIPANTS].stringValue
        self.location_name = json[PARAMS.LOCATION_NAME].stringValue
        self.latitude = json[PARAMS.LATITUDE].stringValue
        self.longitude = json[PARAMS.LONGITUDE].stringValue
        self.feature = json[PARAMS.FEATURE].stringValue
        
        let timestampdate = json[PARAMS.DATE].stringValue
        self.date = getStrDate(timestampdate)
        
        self.from = json[PARAMS.FROM].stringValue
        self.to = json[PARAMS.TO].stringValue
        self.is_chargeable = json[PARAMS.IS_CHARGEABLE].stringValue
        self.price = json[PARAMS.PRICE].stringValue
        self.icon_url = json[PARAMS.ICON_URL].stringValue
        self.descriptionn = json[PARAMS.DESCRIPTION].stringValue
        self.is_visible = json[PARAMS.IS_VISIBLE].stringValue
        self.allow_hide = json[PARAMS.ALLOW_HIDE].stringValue
        self.allow_edit = json[PARAMS.ALLOW_EDIT].stringValue
        self.allow_invite = json[PARAMS.ALLOW_INVITE].stringValue
        self.allow_promote = json[PARAMS.ALLOW_PROMOTE].stringValue
        self.allow_see_chat = json[PARAMS.ALLOW_SEE_CHAT].stringValue
        self.created_at = json[PARAMS.CREATED_AT].stringValue
        self.updated_at = json[PARAMS.UPDATED_AT].stringValue
        self.joined = json[PARAMS.JOINED].stringValue
        self.is_favorite = json[PARAMS.IS_FAVORITE].stringValue
        self.owner_picture = json[PARAMS.OWNER_PICTURE].stringValue
        let media_List = json[PARAMS.MEDIA_LIST].arrayObject
        for one in media_List ?? [Media_list_Model()] {
            let jsonone = JSON(one as Any)
            let miniModel = Media_list_Model(jsonone)
            self.media_list.append(miniModel)
        }
    }*/
}
