//
//  MessageSwipeModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class MessageSwipeModel{
    var id : String? = nil
    var sender_id = ""
    var msgAvatar = ""
    var msgUserName = ""
    var msgContent = ""
    var unreadMsgNum = ""
    var msgTime = ""
    var readStatus : Bool?
    var sender_photo = ""
    
    init() {
         id = ""
         sender_id = ""
         msgAvatar = ""
         msgUserName = ""
         msgContent = ""
         unreadMsgNum = ""
         msgTime = ""
         readStatus = true
         sender_photo = ""
    }
    init(_ id :String,sender_id : String, msgAvatar:String,msgUserName:String, msgContent:String,unreadMsgNum:String,msgTime:String,sender_photo : String){
        self.id = id
        self.sender_id = sender_id
        self.msgAvatar = msgAvatar
        self.msgUserName = msgUserName
        self.msgContent = msgContent
        self.msgTime = msgTime
        self.unreadMsgNum = unreadMsgNum
        if self.unreadMsgNum.toInt() ?? 0 > 0{
            self.readStatus = true
        }
        else{
            self.readStatus = false
        }
        self.sender_photo = sender_photo
    }
}
