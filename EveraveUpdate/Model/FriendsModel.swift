//
//  FriendsModel.swift
//  EveraveUpdate
//
//  Created by Mƒac on 2/22/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class FriendsModel{
    
    var id = ""
    var name = ""
    var surname = ""
    var email = ""
    var phone = ""
    var birthday = ""
    var photo_url = ""
    var qr_code = ""
    var about_me = ""
    var status = ""
    var joined_count = ""
    var organized_count = ""
    var friend_count = ""
    
    init(json : JSON) {
        self.id = json[PARAMS.ID].stringValue
        self.name = json[PARAMS.NAME].stringValue
        self.surname = json[PARAMS.SURNAME].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.phone = json[PARAMS.PHONE].stringValue
        self.birthday = json[PARAMS.BIRTHDAY].stringValue
        self.photo_url = json[PARAMS.PHOTOURL].stringValue
        self.qr_code = json[PARAMS.QRCODE].stringValue
        self.about_me = json[PARAMS.ABOUT_ME].stringValue
        self.status = json[PARAMS.STATUS].stringValue
        self.joined_count = json[PARAMS.JOINED_COUNT].stringValue
        self.organized_count = json[PARAMS.ORGANIZED_COUNT].stringValue
        self.friend_count = json[PARAMS.FRIEND_COUNT].stringValue
    }
}
