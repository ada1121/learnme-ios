//
//  SettingModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation

class SettingModel {
    
    var settingCaption:String = ""
    var image: String = ""
    var state : Bool = false
    
    init (_ settingCaption:String, image: String, state: Bool)
    {
        self.settingCaption = settingCaption
        self.image = image
        self.state = state
    }
}
