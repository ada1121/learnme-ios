//
//  ChatModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyUserDefaults

class ChatModel {
    
    var me : Bool
    var readState : Bool
    var timestamp : String
    var msgContent : String
    var image : String
    var photo : String
    var name : String
    var sender_id : String
    
    init(me : Bool,timestamp : String, msgContent : String, readState : Bool, image: String, photo : String, name: String , sender_id : String) {
        
        self.timestamp = timestamp
        self.msgContent = msgContent
        self.me = me
        self.readState = readState
        self.image = image
        self.photo = photo
        self.name = name
        self.sender_id = sender_id
    }
    
    init() {
        self.timestamp = ""
        self.msgContent = ""
        self.me = true
        self.readState = true
        self.image = ""
        self.photo = ""
        self.name = ""
        self.sender_id = ""
    }
}
