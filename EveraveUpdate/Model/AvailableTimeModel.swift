//
//  AvailableTimeModel.swift
//  EveraveUpdate
//
//  Created by Mac on 6/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class AvailableTimeModel: NSObject {
    
    var mon :String?
    var tue :String?
    var wed :String?
    var thr :String?
    var fri :String?
    var sat :String?
    var sun :String?
    
       override init() {
           super.init()
            mon = ""
            tue = ""
            wed = ""
            thr = ""
            fri = ""
            sat = ""
            sun = ""
       }
       
    init(_ json : JSON) {
        self.mon = json["mon"].stringValue
        self.tue = json["tue"].stringValue
        self.wed = json["wed"].stringValue
        self.thr = json["thr"].stringValue
        self.fri = json["fri"].stringValue
        self.sat = json["sat"].stringValue
        self.sun = json["sun"].stringValue
    }
    
    init( mon :String?,tue :String?,wed :String?,thr :String?,fri :String?,sat :String?,sun :String?) {
        self.mon = mon
        self.tue = tue
        self.wed = wed
        self.thr = thr
        self.fri = fri
        self.sat = sat
        self.sun = sun
    }
}
