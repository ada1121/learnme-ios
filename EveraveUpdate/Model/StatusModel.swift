//
//  StatusModel.swift
//  EveraveUpdate
//
//  Created by Mac on 7/1/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
class StatusModel {
    
    var online : String
    var sender_id : String
    var timesVal : String
    
    
    init(online : String,sender_id : String, timesVal : String) {
        
        self.online = online
        self.sender_id = sender_id
        self.timesVal = timesVal
        
    }
    
    init() {
        self.online = ""
        self.sender_id = ""
        self.timesVal = ""
    }
}
