//
//  BookingSellerVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/26/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit

class BookingSellerVC: BaseVC1  {
   
    @IBOutlet weak var col_mainView: UICollectionView!
    var  ds_booking = [BookingModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        self.getDataSource()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
   
    @IBAction func appointmentBtnClicked(_ sender: Any) {
         return
    }
     
     @IBAction func historyBtnClicked(_ sender: Any) {
        self.self.gotoStoryBoardVC("HistorySellerVC", fullscreen: true)
     }
    
    @IBAction func gotoBack(_ sender: Any) {
        //self.gotoStoryBoardVC("MainVC", fullscreen: true)
        self.gotoVC("SellerMainVC")
    }
    
    func getDataSource() {
        self.showLoadingView(vc: self)
        ApiManager.getBooking { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_booking.removeAll()
                let json = JSON(data as Any)
                let booking_info = json["booking_info"].arrayObject
                
                if let bookingInfo = booking_info{
                    var num = 0
                    for one in bookingInfo{
                        num += 1
                        let jsonONE = JSON(one as Any)
                        if jsonONE[PARAMS.STATE] == "0" || jsonONE[PARAMS.STATE] == "1" || jsonONE[PARAMS.STATE] == "2"{
                            self.ds_booking.append(BookingModel(jsonONE))
                        }
                        if num == bookingInfo.count{
                            self.col_mainView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

extension BookingSellerVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ds_booking.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell", for: indexPath) as! BookingCell
    
        cell.delegate = self
    
        cell.entity = self.ds_booking[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedBooking = nil
        selectedBooking = ds_booking[indexPath.row]
        self.gotoVC("BookingDetailVC")
    }
}

extension BookingSellerVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let w = collectionView.frame.size.width
            let h: CGFloat = 90
        
            return CGSize(width: w, height: h)
        }
}

extension BookingSellerVC : SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
               return nil
            }
            
            let acceptAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
                self.col_mainView.reloadData()
                if self.ds_booking[indexPath.row].state == "0"{
                    
                    let attributedString = NSAttributedString(string: "Confirm Appointment?", attributes: [
                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24),
                        NSAttributedString.Key.foregroundColor : UIColor.black])
                    let alert = UIAlertController(title: "", message: "",  preferredStyle: .alert)

                    alert.setValue(attributedString, forKey: "attributedTitle")
                    
                    
                    // Create OK button with action handler
                    let cancel = UIAlertAction(title: "CANCEL", style: .default, handler: { (action) -> Void in
                        
                    })
                    // Create Cancel button with action handlder
                    let ok = UIAlertAction(title: "OK", style: .cancel) { (action) -> Void in
                        let bookingmodel = self.ds_booking[indexPath.row]
                        self.showLoadingView(vc: self)
                        ApiManager.acceptBooking(id: bookingmodel.id!, state: "1", buyer_id: bookingmodel.buyer_id!, buyer_name: bookingmodel.buyer_name!, seller_name: bookingmodel.seller_name!, service_name: bookingmodel.service_name!) { (isSuccess, data) in
                            self.hideLoadingView()
                            if isSuccess{
                                
                                //self.progShowInfo(true, msg: "Accepted")
                                self.getDataSource()
                                self.showAlerMessage(message: "Accepted")
                            }else{
                                //self.progShowInfo(true, msg: "Network issue")
                                self.showAlerMessage(message: "Network issue")
                            }
                        }
                    }
                    //Add OK and Cancel button to an Alert object
                    alert.addAction(cancel)
                    alert.addAction(ok)
                    
                    // Present alert message to user
                    self.present(alert, animated: true, completion: nil)
                }
            }

            let rejectAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
                self.col_mainView.reloadData()
                if self.ds_booking[indexPath.row].state != "2"{//
                    
                    let attributedString = NSAttributedString(string: "Are you sure want to reject?", attributes: [
                        NSAttributedString.Key.font : UIFont.systemFont(ofSize: 24),
                        NSAttributedString.Key.foregroundColor : UIColor.black])
                    let alert = UIAlertController(title: "", message: "",  preferredStyle: .alert)

                    alert.setValue(attributedString, forKey: "attributedTitle")
                    
                    
                    // Create OK button with action handler
                    let cancel = UIAlertAction(title: "CANCEL", style: .default, handler: { (action) -> Void in
                        
                    })

                    // Create Cancel button with action handlder
                    let ok = UIAlertAction(title: "OK", style: .cancel) { (action) -> Void in
                        let bookingmodel = self.ds_booking[indexPath.row]
                        self.showLoadingView(vc: self)
                        ApiManager.removeBooking(id: bookingmodel.id!) { (isSuccess, data) in
                            self.hideLoadingView()
                            if isSuccess{
                                self.getDataSource()
                                //self.progShowInfo(true, msg: "Rejected")
                                self.showAlerMessage(message: "Rejected")
                                
                            }else{
                                //self.progShowInfo(true, msg: "Network issue")
                                self.showAlerMessage(message: "Network issue")
                            }
                        }
                    }
                    //Add OK and Cancel button to an Alert object
                    alert.addAction(cancel)
                    alert.addAction(ok)
                    // Present alert message to user
                    self.present(alert, animated: true, completion: nil)
                    
                }else{
                    refund_state = "4"
                    refundBooking = self.ds_booking[indexPath.row]
                    self.gotoVC("RefundVC")
                }
            }
               // customize swipe action
            //acceptAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
            //rejectAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

                   // customize the action appearance
            acceptAction.image = UIImage(named: "accept_set")
            rejectAction.image = UIImage(named: "reject_set")

            acceptAction.backgroundColor = UIColor.init(named: "colorSwipeCell")
            rejectAction.backgroundColor = UIColor.init(named: "colorSwipeCell")

            //acceptAction.title = NSLocalizedString("Conference", comment: "")
            //rejectAction.title = NSLocalizedString("Download", comment: "")
            //videoAction.textColor = UIColor.lightGray
            //downLoadAction.textColor = UIColor.lightGray

            /*if self.ds_booking[indexPath.row].state == "Pending"{
                return [rejectAction,acceptAction]
            }
            
            return [rejectAction]*/
            switch self.ds_booking[indexPath.row].state {
            case "0":
                return [rejectAction,acceptAction]
            case "1":
                return [rejectAction]
            case "2":
                return [rejectAction]
            
            default:
                return [rejectAction]
            }
        }
}


