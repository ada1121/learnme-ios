//
//  ServiceProfileVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/23/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import ExpyTableView
import ExpandableCell
import SwiftyJSON
import Cosmos
import LFTimePicker
import SwiftyMenu

class ServiceProfileVC: BaseVC1 ,UITextViewDelegate, UIGestureRecognizerDelegate, UIScrollViewDelegate {
    
    @IBOutlet weak var ServicesView: ExpyTableView!
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var lbl_aboutme: UITextView!
    @IBOutlet weak var txv_toolsneed: UITextView!
    @IBOutlet weak var txv_languages: UITextView!
    
    @IBOutlet weak var lbl_montime: UILabel!
    @IBOutlet weak var lbl_tuetime: UILabel!
    @IBOutlet weak var lbl_wedtime: UILabel!
    @IBOutlet weak var lbl_thrtime: UILabel!
    @IBOutlet weak var lbl_fritime: UILabel!
    @IBOutlet weak var lbl_sattime: UILabel!
    @IBOutlet weak var lbl_suntime: UILabel!
    
    @IBOutlet weak var imv_monDel: UIImageView!
    @IBOutlet weak var imv_tueDel: UIImageView!
    @IBOutlet weak var imv_wedDel: UIImageView!
    @IBOutlet weak var imv_thuDel: UIImageView!
    @IBOutlet weak var imv_friDel: UIImageView!
    @IBOutlet weak var imv_satDel: UIImageView!
    @IBOutlet weak var imv_sunDel: UIImageView!
    @IBOutlet weak var imv_monCheck: UIImageView!
    @IBOutlet weak var imv_tueCheck: UIImageView!
    @IBOutlet weak var imv_wedCheck: UIImageView!
    @IBOutlet weak var imv_thuCheck: UIImageView!
    @IBOutlet weak var imv_friCheck: UIImageView!
    @IBOutlet weak var imv_satCheck: UIImageView!
    @IBOutlet weak var imv_sunCheck: UIImageView!
    
    @IBOutlet weak var btn_monDel: UIButton!
    @IBOutlet weak var btn_tueDel: UIButton!
    @IBOutlet weak var btn_wedDel: UIButton!
    @IBOutlet weak var btn_thrDel: UIButton!
    @IBOutlet weak var btn_friDel: UIButton!
    @IBOutlet weak var btn_satDel: UIButton!
    @IBOutlet weak var btn_sunDel: UIButton!
    @IBOutlet weak var btn_monCheck: UIButton!
    @IBOutlet weak var btn_tueCheck: UIButton!
    @IBOutlet weak var btn_wedCheck: UIButton!
    @IBOutlet weak var btn_thrCheck: UIButton!
    @IBOutlet weak var btn_friCheck: UIButton!
    @IBOutlet weak var btn_satCheck: UIButton!
    @IBOutlet weak var btn_sunCheck: UIButton!
    @IBOutlet weak var dropDown: SwiftyMenu! // dropdownView
    
    @IBOutlet weak var uiv_bankinfoDlg: UIView!
    @IBOutlet weak var uiv_firstDlg: UIView!
    @IBOutlet weak var bankDetailDlg: UIView!
    @IBOutlet weak var btn_totalBtn: UIButton!
    @IBOutlet weak var btn_firstOK: UIButton!
    @IBOutlet weak var btn_save: UIButton!
    @IBOutlet weak var btn_close: UIButton!
    
    @IBOutlet weak var edt_bankfirstName: UITextField!
    @IBOutlet weak var edt_bankLastName: UITextField!
    @IBOutlet weak var edt_accountNumber: UITextField!
    @IBOutlet weak var edt_bankRoutingNumber: UITextField!
    
    
    @IBOutlet weak var edt_t15: UITextField!
    @IBOutlet weak var edt_t30: UITextField!
    @IBOutlet weak var edt_t45: UITextField!
    @IBOutlet weak var edt_t60: UITextField!
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    var dropDonwHeaderIndex = 0
    var CategoriesList = [ServiceModel]()
    var cellHeight = 40 // expandable table view cell height
    var payment_id = ""
    var paymentInputState = false
    //var payment_lastName = ""
    //var payment_account_number = ""
    //var payment_routing_number = ""
    //var payment_account_type = ""
    
    let timePicker = LFTimePickerController()
    var loadNum = -1
    var selectedCategory = ""
    
    @IBOutlet weak var cons_aboutmeHeight: NSLayoutConstraint!
    @IBOutlet weak var cons_languagesHeight: NSLayoutConstraint!
    @IBOutlet weak var cons_toolsneededHeight: NSLayoutConstraint!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            //self.ServicesView.expand(2)
            }
        )*/
        self.lbl_aboutme.delegate = self
        self.txv_toolsneed.delegate = self
        self.txv_languages.delegate = self
        
        timePicker.delegate = self
        // MARK: bank dialog setting
        /*if self.lbl_aboutme.text == ""{
            self.cons_aboutmeHeight.constant = 60
            self.lbl_aboutme.isEditable = true
            self.lbl_aboutme.becomeFirstResponder()
            self.lbl_aboutme.text = ""
        }*/
        
        
        self.uiv_bankinfoDlg.isHidden = true
        self.edtBankGropupinit()
        // MARK: Timer picker setting
        timePicker.timeType = .hour12
        self.navBarHidden()
        // dropdown datasource init
        dropDown.delegate = self
        dropDown.options = dropDownOptionsDataSource
        dropDown.didExpand = {
            //print("SwiftyMenu Expanded!")
        }
        dropDown.didCollapse = {
           // print("SwiftyMeny Collapsed")
        }
        dropDown.didSelectOption = { (selection: Selection) in
            print("DropDownParam ==\(selection.value) at index: \(selection.index)")
            
            self.dropDonwHeaderIndex = selection.index
            self.selectedCategory = selection.value as! String
        }
        //* Custom Behavior
        dropDown.scrollingEnabled = false
        dropDown.isMultiSelect = false
        //* Custom UI
        dropDown.rowHeight = 50
        dropDown.listHeight = 650
        //dropDown.borderWidth = 1.0
        //* Custom Colors
        dropDown.borderColor = .black
        //dropDown.optionColor = UIColor.init(named: "ColorDropDownOption")!
        dropDown.optionColor = .black
        dropDown.placeHolderColor = .black
        //dropDown.menuHeaderBackgroundColor = UIColor.init(named: "ColorDropHeader")!
        dropDown.menuHeaderBackgroundColor = .lightGray
        //dropDown.rowBackgroundColor = UIColor.init(named: "ColordropDownListBack")!
        dropDown.rowBackgroundColor = .white
        dropDown.placeHolderText = "Select Category"
        
        // Custom Animation
        dropDown.expandingAnimationStyle = .spring(level: .normal)
        dropDown.expandingDuration = 0.5
        dropDown.collapsingAnimationStyle = .linear
        dropDown.collapsingDuration = 0.5
        dropDown.hideOptionsWhenSelect = true
        
        let longPressRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(longPressed(sender:)))
        self.ServicesView.addGestureRecognizer(longPressRecognizer)
        if user?.profile_completed == "0"{
            self.setAvailableTimes()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if loadNum == -1{
            self.getProfessionalProfile()
            loadNum += 1
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // selected catview reload data
        ServicesView.rowHeight = 40.0 // table view row height defined
        ServicesView.expandingAnimation = .bottom
        ServicesView.collapsingAnimation = .top
        ServicesView.tableFooterView = UIView()
        // selectedCattable View set
        ServicesView.dataSource = self
        ServicesView.delegate = self
        ServicesView.reloadData()
        tableViewheight.constant = ServicesView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func setAvailableTimes()  {
        self.lbl_montime.text = "9 AM ~ 7 PM"
        self.lbl_tuetime.text = "9 AM ~ 7 PM"
        self.lbl_wedtime.text = "9 AM ~ 7 PM"
        self.lbl_thrtime.text = "9 AM ~ 7 PM"
        self.lbl_fritime.text = "9 AM ~ 7 PM"
        self.lbl_sattime.text = "9 AM ~ 7 PM"
        self.lbl_suntime.text = "9 AM ~ 7 PM"
    }
    
     func textViewDidBeginEditing(_ textView: UITextView) {
        if textView == lbl_aboutme{
            
            self.lbl_aboutme.isScrollEnabled = true
        }else if textView == txv_toolsneed{
            
            self.txv_toolsneed.isScrollEnabled = true
        }else{
            self.txv_languages.isScrollEnabled = true
//            if !txv_languages.text.isEmpty{
//                if txv_languages.text.count > 50 {
//                     self.txv_languages.isScrollEnabled = true
//                }
//
//            }else{
//                 self.txv_languages.isScrollEnabled = true
//            }
        }
    }
    
     func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView == lbl_aboutme{
            if !lbl_aboutme.text.isEmpty{
                if lbl_aboutme.text.count > 1 {
                    self.adjustUITextViewHeight(arg: lbl_aboutme)
                }
                 
            }else{
                self.cons_aboutmeHeight.constant = 40
            }
        }else if textView == txv_toolsneed{
            if !txv_toolsneed.text.isEmpty{
                if txv_toolsneed.text.count > 1 {
                    self.adjustUITextViewHeight(arg: txv_toolsneed)
                }
                 
            }else{
                self.cons_toolsneededHeight.constant = 40
            }
        }else{
            if !txv_languages.text.isEmpty{
                if txv_languages.text.count > 1 {
                    self.adjustUITextViewHeight(arg: txv_languages)
                }
                 
            }else{
                self.cons_languagesHeight.constant = 40
            }
        }
    }
    
    func edtBankGropupinit(){
        setEdtPlaceholder(edt_bankfirstName , placeholderText : "First Name", placeColor : .darkGray, padding: .left(10))
        setEdtPlaceholder(edt_bankLastName , placeholderText : "Last Name", placeColor : .darkGray, padding: .left(10))
        setEdtPlaceholder(edt_accountNumber , placeholderText : "Account Number", placeColor : .darkGray, padding: .left(10))
        setEdtPlaceholder(edt_bankRoutingNumber , placeholderText : "Routing Number", placeColor : .darkGray, padding: .left(10))
        
    }
    
    func adjustUITextViewHeight(arg : UITextView)
    {
        arg.translatesAutoresizingMaskIntoConstraints = true
        arg.sizeToFit()
        arg.isScrollEnabled = false
    }
    
    func getProfessionalProfile() {
        self.showLoadingView(vc: self)
        ApiManager.getProfessionalProfile(user_id: "\(user?.id! ?? 0)") { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                thisSeller = SellerModel()
                let dict = JSON(data as Any)
                thisSeller!.des = dict["description"].stringValue
                thisSeller!.languages = dict["languages"].stringValue
                thisSeller!.tools = dict["tools"].stringValue
                thisSeller!.rating = dict["rating"].floatValue
                thisSeller!.count = dict["count"].stringValue
                
                let payment_info = dict["payment_info"].arrayObject
                if let paymentInfo = payment_info{
                    for one in paymentInfo{
                        let json = JSON(one as Any)
                        self.payment_id = json["id"].stringValue
                        self.edt_bankfirstName.text = json["first_name"].stringValue
                        self.edt_bankLastName.text = json["last_name"].stringValue
                        self.edt_accountNumber.text = json["account_number"].stringValue
                        self.edt_bankRoutingNumber.text = json["routing_number"].stringValue
                        //self.edt_checkingState.text = json["account_type"].stringValue
                        self.paymentInputState = true
                    }
                }
                let availableItems = dict["availabletime_info"].arrayObject
                if let availableitems = availableItems{
                    for one in availableitems{
                        let json = JSON(one)
                        thisSeller!.availableTimeModel = AvailableTimeModel(json)
                    }
                }
                let serviceItems = dict["service_info"].arrayObject
                thisSeller!.serviceModelList.removeAll()
                var servicenum = 0
                if let serviceitems =  serviceItems{
                    for one in serviceitems{
                        servicenum += 1
                        let json = JSON(one)
                        let servicemodel = ServiceModel()
                        
                        if json["name"].stringValue != ""{
                            servicemodel.name = json[PARAMS.NAME].stringValue
                        }
                        if json["id"].stringValue != ""{
                            servicemodel.id = json[PARAMS.ID].stringValue
                        }
                        if json["t_15"].stringValue != ""{
                            let timepricemodel = TimePriceModel()
                            timepricemodel.time = "15"
                            timepricemodel.price = json["t_15"].stringValue
                            servicemodel.timePriceModelList.append(timepricemodel)
                            servicemodel.t_15 = json["t_15"].stringValue
                        }
                        
                        if json["t_30"].stringValue != ""{
                            let timepricemodel = TimePriceModel()
                            timepricemodel.time = "30"
                            timepricemodel.price = json["t_30"].stringValue
                            servicemodel.timePriceModelList.append(timepricemodel)
                            servicemodel.t_30 = json["t_30"].stringValue
                        }
                        
                        if json["t_45"].stringValue != ""{
                            let timepricemodel = TimePriceModel()
                            timepricemodel.time = "45"
                            timepricemodel.price = json["t_45"].stringValue
                            servicemodel.timePriceModelList.append(timepricemodel)
                            servicemodel.t_45 = json["t_45"].stringValue
                        }
                        
                        if json["t_60"].stringValue != ""{
                            let timepricemodel = TimePriceModel()
                            timepricemodel.time = "60"
                            timepricemodel.price = json["t_60"].stringValue
                            servicemodel.timePriceModelList.append(timepricemodel)
                            servicemodel.t_60 = json["t_60"].stringValue
                        }
                        thisSeller!.serviceModelList.append(servicemodel)
                        
                        if servicenum == serviceitems.count{
                            
                        }
                    }
                }
                self.setSellerProfile()
            }
        }
    }
    
    func localTime(in timeZone: String) -> String {
        let f = ISO8601DateFormatter()
        f.formatOptions = [.withInternetDateTime]
        f.timeZone = TimeZone(identifier: timeZone)
        return f.string(from: Date())
    }
    
    @objc func orientationDidChange() {
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight: ServicesView.reloadSections(IndexSet(Array(ServicesView.expandedSections.keys)), with: .none)
        default:break
        }
    }
    
    func setSellerProfile() {
        if let thisseller = thisSeller{
                self.lbl_aboutme.text = thisseller.des
                self.txv_languages.text = thisseller.languages
                self.txv_toolsneed.text = thisseller.tools
            ratingVal = "\(thisseller.rating ?? 0.0)"
            countVal = thisseller.count ?? "0"
                if self.lbl_aboutme.text != ""{
                    self.adjustUITextViewHeight(arg: lbl_aboutme)
                }
            
                if self.txv_languages.text != ""{
                    self.adjustUITextViewHeight(arg: txv_languages)
                }
            
                if self.txv_toolsneed.text != ""{
                    self.adjustUITextViewHeight(arg: txv_toolsneed)
                }
                
                self.CategoriesList = thisseller.serviceModelList
                for i in 0 ..< CategoriesList.count {
                    ServicesView.expandedSections[i] = true
                }
            
            if let availabletimeModel = thisseller.availableTimeModel{
                var availtime = ""
                if availabletimeModel.mon! != "" && availabletimeModel.mon! != "Closed"{
                    let timeduration = availabletimeModel.mon!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.setDaysImagesBtn(1, delBtn_show: true)
                    self.lbl_montime.text = availtime
                    
                }else{
                    availtime = "Closed"
                    self.lbl_montime.text = availtime
                    self.setDaysImagesBtn(1, delBtn_show: false)
                }
                if availabletimeModel.tue! != ""  && availabletimeModel.tue! != "Closed"{
                    let timeduration = availabletimeModel.tue!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.setDaysImagesBtn(2, delBtn_show: true)
                    self.lbl_tuetime.text = availtime
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(2, delBtn_show: false)
                    self.lbl_tuetime.text = availtime
                }
                if availabletimeModel.wed! != "" && availabletimeModel.wed! != "Closed"{
                    let timeduration = availabletimeModel.wed!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.lbl_wedtime.text = availtime
                    self.setDaysImagesBtn(3, delBtn_show: true)
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(3, delBtn_show: false)
                    self.lbl_wedtime.text = availtime
                }
                if availabletimeModel.thr! != "" && availabletimeModel.thr! != "Closed"{
                    let timeduration = availabletimeModel.thr!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.lbl_thrtime.text = availtime
                    self.setDaysImagesBtn(4, delBtn_show: true)
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(4, delBtn_show: false)
                    self.lbl_thrtime.text = availtime
                }
                if availabletimeModel.fri! != "" && availabletimeModel.fri! != "Closed"{
                    let timeduration = availabletimeModel.fri!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.lbl_fritime.text = availtime
                    self.setDaysImagesBtn(5, delBtn_show: true)
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(5, delBtn_show: false)
                    self.lbl_fritime.text = availtime
                }
                if availabletimeModel.sat! != "" && availabletimeModel.sat! != "Closed"{
                    let timeduration = availabletimeModel.sat!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.lbl_sattime.text = availtime
                    self.setDaysImagesBtn(6, delBtn_show: true)
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(6, delBtn_show: false)
                    self.lbl_sattime.text = availtime
                }
                if availabletimeModel.sun! != "" && availabletimeModel.sun! != "Closed" {
                    let timeduration = availabletimeModel.sun!
                    let startTime: String = String(timeduration.split(separator: "~").first!).trim()
                    let lastTime: String = String(timeduration.split(separator: "~").last!).trim()
                    availtime =  gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + " " + "~" + " " + gethourTimeFromTimeStamp(Int64(lastTime.toInt()!))
                    self.lbl_suntime.text = availtime
                    self.setDaysImagesBtn(7, delBtn_show: true)
                }else{
                    availtime = "Closed"
                    self.setDaysImagesBtn(7, delBtn_show: false)
                    self.lbl_suntime.text = availtime
                }
            }
        }
    }
    
    @IBAction func catRemoveBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let section = button.tag
        
        var alert = UIAlertController()
        alert = UIAlertController(title: "", message: "Are you sure want to remove this service?", preferredStyle: .alert)
        let remove = UIAlertAction(title: "Remove", style: .default, handler: { action in
            
            self.showLoadingView(vc: self)
            let id = self.CategoriesList[section].id
            ApiManager.removeService(id: id!, item_name: "") { (isSuccess, data) in
                
                self.hideLoadingView()
                if isSuccess{
                    self.CategoriesList.remove(at: section)
                    self.ServicesView.reloadData()
                    self.tableViewheight.constant = self.ServicesView.contentSize.height
                    thisSeller?.serviceModelList.removeAll()
                    thisSeller?.serviceModelList = self.CategoriesList
                    self.view.layoutIfNeeded()
                }
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
             alert.dismiss(animated: false)
         })
        alert.addAction(remove)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    @IBAction func listRemoveBtnClicked(_ sender: Any) {
        let button = sender as! UIButton
        let row = button.tag
        let section = button.superview?.tag ?? 0
        
        var alert = UIAlertController()
        
        alert = UIAlertController(title: "", message: "Are you sure want to remove this item?", preferredStyle: .alert)
        let remove = UIAlertAction(title: "Remove", style: .default, handler: { action in
            self.showLoadingView(vc: self)
            let id = self.CategoriesList[section].id
            if self.CategoriesList[section].timePriceModelList.count == 1{
                ApiManager.removeService(id: id!, item_name: "") { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        self.CategoriesList.remove(at: section)
                        self.ServicesView.reloadData()
                        self.tableViewheight.constant = self.ServicesView.contentSize.height
                        thisSeller?.serviceModelList.removeAll()
                        thisSeller?.serviceModelList = self.CategoriesList
                        self.view.layoutIfNeeded()
                    }
                }
            }else{
                let item_name = "t_" + self.CategoriesList[section].timePriceModelList[row - 1].time!
                ApiManager.removeService(id: id!, item_name:item_name) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        self.CategoriesList[section].timePriceModelList.remove(at: row - 1)
                        self.ServicesView.reloadData()
                        self.tableViewheight.constant = self.ServicesView.contentSize.height
                        thisSeller?.serviceModelList.removeAll()
                        thisSeller?.serviceModelList = self.CategoriesList
                        self.view.layoutIfNeeded()
                    }
                }
            }
        })
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
             alert.dismiss(animated: false)
         })
        alert.addAction(remove)
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
    func setDaysImagesBtn(_ index: Int, delBtn_show: Bool ) {
        if index == 1{
            if delBtn_show{
                self.btn_monDel.isHidden = false
                self.btn_monCheck.isHidden = true
                self.imv_monDel.isHidden = false
                self.imv_monCheck.isHidden = true
                
            }else{
                self.btn_monDel.isHidden = true
                self.btn_monCheck.isHidden = false
                self.imv_monDel.isHidden = true
                self.imv_monCheck.isHidden = false
            }
        }else if index == 2{
            if delBtn_show{
                self.btn_tueDel.isHidden = false
                self.btn_tueCheck.isHidden = true
                self.imv_tueDel.isHidden = false
                self.imv_tueCheck.isHidden = true
            }else{
                self.btn_tueDel.isHidden = true
                self.btn_tueCheck.isHidden = false
                self.imv_tueDel.isHidden = true
                self.imv_tueCheck.isHidden = false
            }
        }else if index == 3{
            if delBtn_show{
                self.btn_wedDel.isHidden = false
                self.btn_wedCheck.isHidden = true
                self.imv_wedDel.isHidden = false
                self.imv_wedCheck.isHidden = true
            }else{
                self.btn_wedDel.isHidden = true
                self.btn_wedCheck.isHidden = false
                self.imv_wedDel.isHidden = true
                self.imv_wedCheck.isHidden = false
            }
        }else if index == 4{
            if delBtn_show{
                self.btn_thrDel.isHidden = false
                self.btn_thrCheck.isHidden = true
                self.imv_thuDel.isHidden = false
                self.imv_thuCheck.isHidden = true
            }else{
                self.btn_thrDel.isHidden = true
                self.btn_thrCheck.isHidden = false
                self.imv_thuDel.isHidden = true
                self.imv_thuCheck.isHidden = false
            }
        }else if index == 5{
            if delBtn_show{
                self.btn_friDel.isHidden = false
                self.btn_friCheck.isHidden = true
                self.imv_friDel.isHidden = false
                self.imv_friCheck.isHidden = true
            }else{
                self.btn_friDel.isHidden = true
                self.btn_friCheck.isHidden = false
                self.imv_friDel.isHidden = true
                self.imv_friCheck.isHidden = false
            }
        }else if index == 6{
            if delBtn_show{
                self.btn_satDel.isHidden = false
                self.btn_satCheck.isHidden = true
                self.imv_satDel.isHidden = false
                self.imv_satCheck.isHidden = true
            }else{
                self.btn_satDel.isHidden = true
                self.btn_satCheck.isHidden = false
                self.imv_satDel.isHidden = true
                self.imv_satCheck.isHidden = false
            }
        }else if index == 7{
            if delBtn_show{
                self.btn_sunDel.isHidden = false
                self.btn_sunCheck.isHidden = true
                self.imv_sunDel.isHidden = false
                self.imv_sunCheck.isHidden = true
            }else{
                self.btn_sunDel.isHidden = true
                self.btn_sunCheck.isHidden = false
                self.imv_sunDel.isHidden = true
                self.imv_sunCheck.isHidden = false
            }
        }
    }
    
    func showAlert_Remove(_ section:Int,row: Int) {
        if row == 0{ // category totally remove
            self.CategoriesList.remove(at: section)
            self.ServicesView.reloadData()
            self.tableViewheight.constant = self.ServicesView.contentSize.height
            thisSeller?.serviceModelList.removeAll()
            thisSeller?.serviceModelList = self.CategoriesList
            self.view.layoutIfNeeded()
        }else{
            self.CategoriesList[section].timePriceModelList.remove(at: row - 1) // subcategory remove
            if self.CategoriesList[section].timePriceModelList.count == 0{
                self.CategoriesList.remove(at: section)
            }
            self.ServicesView.reloadData()
            self.tableViewheight.constant = self.ServicesView.contentSize.height
            thisSeller?.serviceModelList.removeAll()
            thisSeller?.serviceModelList = self.CategoriesList
            self.view.layoutIfNeeded()
        }
    }
    
    @objc func longPressed(sender: UILongPressGestureRecognizer) {
        if sender.state == UIGestureRecognizer.State.began {
            let touchPoint = sender.location(in: self.ServicesView)
            if let indexPath = ServicesView.indexPathForRow(at: touchPoint) {
                
                let row  = indexPath.row
                let section = indexPath.section
                
                var alert = UIAlertController()
                if row  == 0{
                    alert = UIAlertController(title: "", message: "Are you sure want to remove this service?", preferredStyle: .alert)
                }else{
                    alert = UIAlertController(title: "", message: "Are you sure want to remove this item?", preferredStyle: .alert)
                }
                
                let remove = UIAlertAction(title: "Remove", style: .default, handler: { action in
                    
                    self.showLoadingView(vc: self)
                    if row == 0{
                        let id = self.CategoriesList[section].id
                        ApiManager.removeService(id: id!, item_name: "") { (isSuccess, data) in
                            
                            self.hideLoadingView()
                            if isSuccess{
                                self.showAlert_Remove(indexPath.section, row: indexPath.row)
                            }
                        }
                    }else{
                        let id = self.CategoriesList[section].id
                        if self.CategoriesList[section].timePriceModelList.count == 1{
                            ApiManager.removeService(id: id!, item_name: "") { (isSuccess, data) in
                                self.hideLoadingView()
                                if isSuccess{
                                    self.showAlert_Remove(indexPath.section, row: indexPath.row)
                                }
                            }
                        }else{
                            let item_name = "t_" + self.CategoriesList[section].timePriceModelList[row - 1].time!
                            ApiManager.removeService(id: id!, item_name:item_name) { (isSuccess, data) in
                                self.hideLoadingView()
                                if isSuccess{
                                    self.showAlert_Remove(indexPath.section, row: indexPath.row)
                                }
                            }
                        }
                    }
                })
                let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
                     alert.dismiss(animated: false)
                 })
                alert.addAction(remove)
                alert.addAction(cancel)
                DispatchQueue.main.async(execute: {
                    self.present(alert, animated: true)
                })
            }
        }
    }
    
    func saveBtnAction() {
        var miniTimepriceList = [TimePriceModel]()
        miniTimepriceList.removeAll()
        
        let str_edt15 = self.edt_t15.text
        let str_edt30 = self.edt_t30.text
        let str_edt45 = self.edt_t45.text
        let str_edt60 = self.edt_t60.text
        var num = -1
        for one in CategoriesList{
            num += 1
            if one.name == selectedCategory{
                CategoriesList.remove(at: num)
                break
            }
        }
        
        if !str_edt15!.isEmpty{
            miniTimepriceList.append(TimePriceModel(id: "", time: "15", price: str_edt15))
        }
        if !str_edt30!.isEmpty{
            miniTimepriceList.append(TimePriceModel(id: "", time: "30", price: str_edt30))
        }
        if !str_edt45!.isEmpty{
            miniTimepriceList.append(TimePriceModel(id: "", time: "45", price: str_edt45))
        }
        if !str_edt60!.isEmpty{
            miniTimepriceList.append(TimePriceModel(id: "", time: "60", price: str_edt60))
        }
        self.showLoadingView(vc: self)
        ApiManager.uploadService(name: selectedCategory, t_15: str_edt15!, t_30: str_edt30!, t_45: str_edt45!, t_60: str_edt60!) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.CategoriesList.append(ServiceModel(id: "", name: self.selectedCategory, t_15: str_edt15, t_30: str_edt30, t_45: str_edt45, t_60: str_edt60, timepricemodelList: miniTimepriceList))
                self.ServicesView.reloadData()
                self.tableViewheight.constant = self.ServicesView.contentSize.height
                self.view.layoutIfNeeded()
                thisSeller?.serviceModelList.removeAll()
                thisSeller?.serviceModelList = self.CategoriesList
                self.scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
            }
        }
    }
    
    func showApproveDialog(){
        let alertController = UIAlertController(title: nil, message:"Profile submitted successfully. We will contact you via email (ASAP) when approved.", preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
            self.gotoVC("LoginNav")
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        //self.dismiss(animated: false, completion: nil)
        //self.navigationController?.popViewController(animated: true)
        self.gotoStoryBoardVC("GeneralProfileVC", fullscreen: true)
    }
    @IBAction func btn_monDelClicked(_ sender: Any) {
        self.imv_monDel.isHidden = true
        self.imv_monCheck.isHidden = false
        self.btn_monDel.isHidden = true
        self.btn_monCheck.isHidden = false
        self.lbl_montime.text = "Closed"
    }
    @IBAction func btn_tueDelClicked(_ sender: Any) {
        self.lbl_tuetime.text = "Closed"
        self.imv_tueDel.isHidden = true
        self.imv_tueCheck.isHidden = false
        self.btn_tueDel.isHidden = true
        self.btn_tueCheck.isHidden = false
    }
    @IBAction func btn_wedDelClicked(_ sender: Any) {
        self.lbl_wedtime.text = "Closed"
        self.imv_wedDel.isHidden = true
        self.imv_wedCheck.isHidden = false
        self.btn_wedDel.isHidden = true
        self.btn_wedCheck.isHidden = false
    }
    @IBAction func btn_thuDelClicked(_ sender: Any) {
        self.lbl_thrtime.text = "Closed"
        self.imv_thuDel.isHidden = true
        self.imv_thuCheck.isHidden = false
        self.btn_thrDel.isHidden = true
        self.btn_thrCheck.isHidden = false
    }
    @IBAction func btn_friDelClicked(_ sender: Any) {
        self.lbl_fritime.text = "Closed"
        self.imv_friDel.isHidden = true
        self.imv_friCheck.isHidden = false
        self.btn_friDel.isHidden = true
        self.btn_friCheck.isHidden = false
    }
    @IBAction func btn_satDelClicked(_ sender: Any) {
        self.lbl_sattime.text = "Closed"
        self.imv_satDel.isHidden = true
        self.imv_satCheck.isHidden = false
        self.btn_satDel.isHidden = true
        self.btn_satCheck.isHidden = false
    }
    @IBAction func btn_sunDelClicked(_ sender: Any) {
        self.lbl_suntime.text = "Closed"
        self.imv_sunDel.isHidden = true
        self.imv_sunCheck.isHidden = false
        self.btn_sunDel.isHidden = true
        self.btn_sunCheck.isHidden = false
    }
    @IBAction func btn_monCheckClicked(_ sender: Any){
        self.imv_monDel.isHidden = false
        self.imv_monCheck.isHidden = true
        self.btn_monDel.isHidden = false
        self.btn_monCheck.isHidden = true
        self.lbl_montime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_tueCheckClicked(_ sender: Any){
        self.imv_tueDel.isHidden = false
        self.imv_tueCheck.isHidden = true
        self.btn_tueDel.isHidden = false
        self.btn_tueCheck.isHidden = true
        self.lbl_tuetime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_wedCheckClicked(_ sender: Any){
        self.imv_wedDel.isHidden = false
        self.imv_wedCheck.isHidden = true
        self.btn_wedDel.isHidden = false
        self.btn_wedCheck.isHidden = true
        self.lbl_wedtime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_thuCheckClicked(_ sender: Any){
        self.imv_thuDel.isHidden = false
        self.imv_thuCheck.isHidden = true
        self.btn_thrDel.isHidden = false
        self.btn_thrCheck.isHidden = true
        self.lbl_thrtime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_friCheckClicked(_ sender: Any){
        self.imv_friDel.isHidden = false
        self.imv_friCheck.isHidden = true
        self.btn_friDel.isHidden = false
        self.btn_friCheck.isHidden = true
        self.lbl_fritime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_satCheckClicked(_ sender: Any){
        self.imv_satDel.isHidden = false
        self.imv_satCheck.isHidden = true
        self.btn_satDel.isHidden = false
        self.btn_satCheck.isHidden = true
        self.lbl_sattime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_sunCheckClicked(_ sender: Any){
        self.imv_sunDel.isHidden = false
        self.imv_sunCheck.isHidden = true
        self.btn_sunDel.isHidden = false
        self.btn_sunCheck.isHidden = true
        self.lbl_suntime.text = "9 AM ~ 7 PM"
    }
    @IBAction func btn_monSettingClicked(_ sender: Any) {
        if self.lbl_montime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.mon.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_tueSettingClicked(_ sender: Any) {
        if self.lbl_tuetime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.tue.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_wedSettingClicked(_ sender: Any) {
        if self.lbl_wedtime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.wed.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_thuSettingClicked(_ sender: Any) {
        if self.lbl_thrtime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.thu.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_friSettingClicked(_ sender: Any) {
        if self.lbl_fritime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.fri.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_satSettingClicked(_ sender: Any) {
        if self.lbl_sattime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.sat.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func btn_sunSettingClicked(_ sender: Any) {
        if self.lbl_suntime.text != "Closed"{
            self.navigationController?.isNavigationBarHidden = false
            weekday = Weeks.sun.rawValue
            self.navigationController?.pushViewController(timePicker, animated: true)
        }
    }
    @IBAction func addserviceBtnClicked(_ sender: Any) {
        let str_edt15 = self.edt_t15.text
        let str_edt30 = self.edt_t30.text
        let str_edt45 = self.edt_t45.text
        let str_edt60 = self.edt_t60.text
        
        if (selectedCategory.isEmpty){
            progShowInfo(true, msg: "Please select your category")
            return
        }
        if !str_edt15!.isEmpty{
            if (str_edt15?.toFloat()! ?? 0 < 10.0){
                progShowInfo(true, msg: "Minimum amount must be at least $10")
                return
            }
        }
        if !str_edt30!.isEmpty{
            if (str_edt30?.toFloat()! ?? 0 < 10.0){
                progShowInfo(true, msg: "Minimum amount must be at least $10")
                return
            }
        }
        if !str_edt45!.isEmpty{
            if (str_edt45?.toFloat()! ?? 0 < 10.0){
                progShowInfo(true, msg: "Minimum amount must be at least $10")
                return
            }
        }
        if !str_edt60!.isEmpty{
            if (str_edt60?.toFloat()! ?? 0 < 10.0){
                progShowInfo(true, msg: "Minimum amount must be at least $10")
                return
            }
        }
        
        if (str_edt15!.isEmpty && str_edt30!.isEmpty && str_edt45!.isEmpty && str_edt60!.isEmpty){
            progShowInfo(true, msg: "Please input your category prices")
            return
        }else{
            self.saveBtnAction()
        }

    }
    
    @IBAction func BankInfoBtnClicked(_ sender: Any) {
        self.uiv_bankinfoDlg.isHidden = false
        self.btn_totalBtn.isUserInteractionEnabled = true
        self.uiv_firstDlg.isHidden = false
        self.bankDetailDlg.isHidden = true
    }
    
    @IBAction func portfolioBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("UploadPicturesVC", fullscreen: true)
    }
    
    @IBAction func viewClientBtnclicked(_ sender: Any) {
        aboutMe = self.lbl_aboutme.text
        tools = self.txv_toolsneed.text
        languages = self.txv_languages.text
        
        availableTimes.removeAll()
        availableTimes.append(lbl_montime.text!)
        availableTimes.append(lbl_tuetime.text!)
        availableTimes.append(lbl_wedtime.text!)
        availableTimes.append(lbl_thrtime.text!)
        availableTimes.append(lbl_fritime.text!)
        availableTimes.append(lbl_sattime.text!)
        availableTimes.append(lbl_suntime.text!)
        self.gotoStoryBoardVC("PreviewVC", fullscreen: true)
    }
    
    @IBAction func bankTotalBackBtnClicked(_ sender: Any) {
        self.uiv_bankinfoDlg.isHidden = true
        self.uiv_firstDlg.isHidden = true
        self.bankDetailDlg.isHidden = true
    }
    
    @IBAction func firstDlgOKBtnClicked(_ sender: Any) {
        self.uiv_firstDlg.isHidden = true
        self.bankDetailDlg.isHidden = false
        self.btn_totalBtn.isUserInteractionEnabled = false
    }
    
    @IBAction func detailSaveBtnClicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        ApiManager.uploadPayment(firstName: self.edt_bankfirstName.text!, lastName: self.edt_bankLastName.text!, accountNumber: self.edt_accountNumber.text!, routingNumber: self.edt_bankRoutingNumber.text!, accountType: "Checking") { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.paymentInputState = true
                self.uiv_bankinfoDlg.isHidden = true
                self.uiv_bankinfoDlg.isHidden = true
                self.btn_totalBtn.isUserInteractionEnabled = true
            }
        }
    }
    
    @IBAction func detailCloseBtnClicked(_ sender: Any) {
        self.uiv_bankinfoDlg.isHidden = true
        self.uiv_bankinfoDlg.isHidden = true
        self.btn_totalBtn.isUserInteractionEnabled = true
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if self.lbl_aboutme.text.isEmpty{
            self.progShowInfo(true, msg: "Please input your service description")
            return
        }
        
        if self.txv_languages.text.isEmpty{
            self.progShowInfo(true, msg: "Please input your languages")
            return
        }
        if thisSeller?.serviceModelList.count == 0 {
            self.progShowInfo(true, msg: "Add a Service to Profile Before Submitting")
            return
        }
        if !self.paymentInputState{
            self.progShowInfo(true, msg: "Please Confirm Your Banking Information")
            return
        }else{
            let str_mon = lbl_montime.text
            let str_tue = lbl_tuetime.text
            let str_wed = lbl_wedtime.text
            let str_thu = lbl_thrtime.text
            let str_fri = lbl_fritime.text
            let str_sat = lbl_sattime.text
            let str_sun = lbl_suntime.text
            
            let currentTime = NSDate().timeIntervalSince1970 * 1000
            let todayweeknum = Date().dayNumberOfWeek() // sun=1 ~ sat=7
            
            var monTimestamp = "Closed"
            if str_mon != "Closed" && str_mon != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 2))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_mon!.split(separator: "~").last!).trim() == "12 AM"{
                   monTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_mon!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    monTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_mon!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_mon!.split(separator: "~").last!).trim())
                }
            }
            print("monTimestamp==>",monTimestamp)
            
            var tueTimestamp = "Closed"
            if str_tue != "Closed"  && str_tue != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 3))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_tue!.split(separator: "~").last!).trim() == "12 AM"{
                   tueTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_tue!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    tueTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_tue!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_tue!.split(separator: "~").last!).trim())
                }
            }
            print("tueTimestamp==>",tueTimestamp)
            
            var wedTimestamp = "Closed"
            if str_wed != "Closed" && str_wed != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 4))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_wed!.split(separator: "~").last!).trim() == "12 AM"{
                   wedTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_wed!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    wedTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_wed!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_wed!.split(separator: "~").last!).trim())
                }
            }
            print("wedTimestamp==>",wedTimestamp)
            
            var thuTimestamp = "Closed"
            if str_thu != "Closed" && str_thu != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 5))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_thu!.split(separator: "~").last!).trim() == "12 AM"{
                   thuTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_thu!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    thuTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_thu!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_thu!.split(separator: "~").last!).trim())
                }
            }
            print("thuTimestamp==>",thuTimestamp)
            
            var friTimestamp = "Closed"
            if str_fri != "Closed" && str_fri != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 6))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_fri!.split(separator: "~").last!).trim() == "12 AM"{
                   friTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_fri!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    friTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_fri!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_fri!.split(separator: "~").last!).trim())
                }
            }
            print("friTimestamp==>",friTimestamp)
            
            var satTimestamp = "Closed"
            if str_sat != "Closed" && str_sat != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 7))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_sat!.split(separator: "~").last!).trim() == "12 AM"{
                   satTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_sat!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    satTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_sat!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_sat!.split(separator: "~").last!).trim())
                }
            }
            print("satTimestamp==>",satTimestamp)
            
            var sunTimestamp = "Closed"
            if str_sun != "Closed" && str_sun != ""{
                let weektimestamp = currentTime - Double(86400000 * (todayweeknum! - 1))
                let weekDateString = getonlyDateFromTimeStamp(Int64(weektimestamp))
                
                if String(str_sun!.split(separator: "~").last!).trim() == "12 AM"{
                   sunTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_sun!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateandMinString(weekDateString + " " + "11:59 PM")
                }else{
                    sunTimestamp = getTimeStampFromDateString(weekDateString + " " + String(str_sun!.split(separator: "~").first!).trim()) + " " + "~" + " " + getTimeStampFromDateString(weekDateString + " " + String(str_sun!.split(separator: "~").last!).trim())
                }
            }
            print("sunTimestamp==>",sunTimestamp)
            self.showLoadingView(vc: self)
            ApiManager.uploadProfessionalProfile(des: self.lbl_aboutme.text!,languages: self.txv_languages.text!, tools: self.txv_toolsneed.text!, mon: monTimestamp, tue: tueTimestamp, wed: wedTimestamp, thr: thuTimestamp, fri: friTimestamp, sat: satTimestamp, sun: sunTimestamp, timezone: localTimeZoneIdentifier) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let approve = data as! String
                    if approve == "0"{
                        user?.profile_completed = "1"
                        user?.saveUserInfo()
                        user?.loadUserInfo()
                        self.showApproveDialog()
                    }else{
                        //self.progShowInfo(true, msg: "Your Profile has been successfully updated")
                        self.showAlerMessage(message: "Your Profile has been successfully updated")
                        if controllerName == controllerNames.MainVC.rawValue{
                            self.gotoVC("SellerMainVC")
                        }else{
                            self.gotoVC("SettingsVC")
                        }
                    }
                }
            }
        }
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension ServiceProfileVC : ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelCatCell.self)) as! SelCatCell
        cell.labelCatName.text = CategoriesList[section].name
        cell.btn_catDelete.tag = section
        cell.layoutMargins = UIEdgeInsets.zero
        //cell.showSeparator()
        return cell
    }
}

// Selected category autodimention
extension ServiceProfileVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        //print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }*/
}

//MARK: UITableView Data Source Methods
extension ServiceProfileVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        //print("\(CategoriesList.count)")
        return CategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Row count for section \(section) is \(CategoriesList[section].timePriceModelList.count)")
        return CategoriesList[section].timePriceModelList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailValTableViewCell2.self)) as! DetailValTableViewCell2
        //  expandable TableView changePart
        cell.labelselectedName.text = CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].time! + "min"
        cell.labelselectedvalue.text = "$" + CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].price!
        cell.btn_delete.tag = indexPath.row
        cell.btn_delete.superview?.tag = indexPath.section
        cell.layoutMargins = UIEdgeInsets.zero
         //cell.showSeparator()
        //cell.hideSeparator()
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension ServiceProfileVC : ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            return
            
        case .willCollapse:
            return
            
        case .didExpand:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant += CGFloat(detailnum * cellHeight )
            tableViewheight.constant = ServicesView.contentSize.height
            
        case .didCollapse:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant -= CGFloat(detailnum * cellHeight)
            tableViewheight.constant = ServicesView.contentSize.height
        }
    }
}
extension ServiceProfileVC: LFTimePickerDelegate {

    func didPickTime(_ start: String, end: String) {
        self.navBarHidden()
        if  let weekDay = weekday{
            if weekDay == Weeks.mon.rawValue{
                
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_montime.text = starttime + " " + "~" + " " + lasttime
            }else if weekDay == Weeks.tue.rawValue{
                //self.lbl_tuetime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_tuetime.text = starttime + " " + "~" + " " + lasttime
            }else if weekDay == Weeks.wed.rawValue{
                //self.lbl_wedtime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_wedtime.text = starttime + " " + "~" + " " + lasttime
            }else if weekDay == Weeks.thu.rawValue{
                //self.lbl_thrtime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_thrtime.text = starttime + " " + "~" + " " + lasttime
            }else if weekDay == Weeks.fri.rawValue{
                //self.lbl_fritime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_fritime.text = starttime + " " + "~" + " " + lasttime
            }else if weekDay == Weeks.sat.rawValue{
                //self.lbl_sattime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_sattime.text = starttime + " " + "~" + " " + lasttime
            }else {
                //self.lbl_suntime.text = start + " " + "~" + " " + end
                let starttime = String(start.split(separator: " ").first!.split(separator: ":").first!) + " " + start.split(separator: " ").last!
                let lasttime = String(end.split(separator: " ").first!.split(separator: ":").first!) + " " + end.split(separator: " ").last!
                self.lbl_suntime.text = starttime + " " + "~" + " " + lasttime
            }
        }
    }
    
    func didPickTime1() {
        self.navBarHidden()
    }
}

extension ServiceProfileVC : SwiftyMenuDelegate {
    
    func didSelectOption(_ swiftyMenu: SwiftyMenu, _ selectedOption: SwiftyMenuDisplayable, _ index: Int) {
        print("Selected option: \(selectedOption), at index: \(index)")
    }
    func swiftyMenuWillAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will appear.")
    }
    func swiftyMenuDidAppear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did appear.")
    }
    func swiftyMenuWillDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu will disappear.")
    }
    func swiftyMenuDidDisappear(_ swiftyMenu: SwiftyMenu) {
        //print("SwiftyMenu did disappear.")
    }
}

// String extension to conform SwiftyMenuDisplayable for defult behavior
extension String: SwiftyMenuDisplayable {
    public var displayableValue: String {
        return self
    }
    public var retrivableValue: Any {
        return self
    }
}

enum Weeks: String{
    case mon = "mon"
    case tue = "tue"
    case wed = "wed"
    case thu = "thu"
    case fri = "fri"
    case sat = "sat"
    case sun = "sun"
}


