//
//  BusinessProfileVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/1/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class BusinessProfileVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    @IBAction func goBtnClicked(_ sender: Any) {
        self.gotoStoryBoardVC("GeneralProfileVC", fullscreen: true)
    }
    @IBAction func goHelp(_ sender: Any) {
        self.gotoVC("HelpVC")
    }
}
