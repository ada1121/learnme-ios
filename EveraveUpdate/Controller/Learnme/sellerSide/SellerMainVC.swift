//
//  SellerMainVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/11/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit
import Cosmos
import Firebase
import FirebaseDatabase

class SellerMainVC: BaseVC1  {
    
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var swbtn_online: UISwitch!
    @IBOutlet weak var lbl_fullName: UILabel!
    @IBOutlet weak var cus_rating: CosmosView!
    @IBOutlet weak var lbl_pendingRequest: UITextView!
    @IBOutlet weak var lbl_upcomingAppointsment: UITextView!
    @IBOutlet weak var lbl_bookingBadge: UILabel!
    @IBOutlet weak var imv_bookingBadge: UIImageView!
    var  ds_booking = [BookingModel]()
    
    @IBOutlet weak var lbl_notyicon: UIImageView!
    @IBOutlet weak var lbl_msgNum: UILabel!
    var Nnumber = 0
    
    var msgDatasource = [MessageSwipeModel]()
    var msgDatasource1 = [MessageSwipeModel]()
    var newChatHandle: UInt?
    
    override func viewWillAppear(_ animated: Bool) {
        self.getOnlineState()
        self.userlistListner()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if user!.isValid{
            loadLayout()
            swbtn_online.addTarget(self, action: #selector(switchChanged), for: UIControl.Event.valueChanged)
        }
//        if Nnumber > 0{
//            self.lbl_msgNum.text = "\(Nnumber)"
//            self.lbl_notyicon.isHidden = false
//        }else{
//            self.lbl_msgNum.text = ""
//            self.lbl_notyicon.isHidden = true
//        }
        
    }
    
    func userlistListner()  {
        self.msgDatasource.removeAll()
        let userRoomid = "u" + "\(user?.id ?? 0)"
        newChatHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            guard userlist.id != nil else{
                //self.hideLoadingView()
                return
            }
            
            self.msgDatasource.append(userlist)
            var ids = [String]()
            for one in self.msgDatasource{
                ids.append(one.id!)
            }
            //print(ids.removeDuplicates())
            var num = 0
            self.msgDatasource1.removeAll()
            for  one in ids.removeDuplicates(){
                num += 1
                self.msgDatasource1.append(self.getDataFromID(one))
                if num == ids.removeDuplicates().count{
                    //dump(self.msgDatasource1, name: "msgDatasource1====>")
                    //dump(self.msgDatasource, name: "msgDatasource====.")
                    
                    var unRead = 0
                    var num = 0
                    for one in self.msgDatasource1{
                        num += 1
                        if one.unreadMsgNum != "0" && one.unreadMsgNum.toInt() ?? 0 > 0{
                            unRead += 1
                        }
                        if num == self.msgDatasource1.count{
                            if unRead > 0{
                                self.lbl_msgNum.isHidden = false
                                self.lbl_msgNum.text = "\(unRead)"
                                self.lbl_notyicon.isHidden = false
                                self.view.layoutIfNeeded()
                            }else{
                                 self.lbl_msgNum.isHidden = true
                                 self.lbl_msgNum.text = ""
                                 self.lbl_notyicon.isHidden = true
                            }
                        }
                    }
                }
             }
          }
       )
    }
    
    func getDataFromID(_ id: String) -> MessageSwipeModel {
          var returnModel : MessageSwipeModel?
          for one in self.msgDatasource{
              if id == one.id{
                  returnModel = one
              }
          }
          return returnModel ?? MessageSwipeModel()
      }
    
      
      /*func getUnreadNum(msgdataSource: [MessageSwipeModel], completion :  @escaping (_ number: String) -> ()) {
          var unRead = 0
          var num = 0
          for one in msgdataSource{
              num += 1
              if one.unreadMsgNum != "0" && one.unreadMsgNum.toInt() ?? 0 > 0{
                  unRead += 1
              }
              if num == msgDatasource.count{
                  completion("\(unRead)")
              }
          }
      }*/
    
    @objc func switchChanged(mySwitch: UISwitch) {
        
        if user!.isValid{
            let value = mySwitch.isOn
            var state = ""
            if value{
                state = "online"
            }else{
                state = "offline"
            }
            ApiManager.setOnlineState(state: state) { (isSuccess, data) in
                if isSuccess{
                    print(state)
                }
            }
        }
    }
    
    func getOnlineState() {
        self.showLoadingView(vc: self)
        ApiManager.getOnlineState { (isSuccess, state, rating) in
            self.hideLoadingView()
            if isSuccess{
                let state = state!
                if state == "online"{
                    self.swbtn_online.isOn = true
                }else{
                    self.swbtn_online.isOn = false
                }
                let rating = rating!
                self.cus_rating.rating = Double(rating)
                user?.online = state
                user?.saveUserInfo()
                user?.loadUserInfo()
                self.getDataSource()
            }
        }
    }
    
    func getDataSource() {
        ApiManager.getBooking { (isSuccess, data) in
            if isSuccess{
                self.ds_booking.removeAll()
                let json = JSON(data as Any)
                let booking_info = json["booking_info"].arrayObject
                
                if let bookingInfo = booking_info{
                    var num = 0
                    for one in bookingInfo{
                        num += 1
                        let jsonONE = JSON(one as Any)
                        if jsonONE[PARAMS.STATE] == "0" || jsonONE[PARAMS.STATE] == "1" || jsonONE[PARAMS.STATE] == "2"{
                            self.ds_booking.append(BookingModel(jsonONE))
                        }
                        if num == bookingInfo.count{
                            self.setBookingState()
                        }
                    }
                }
            }else{
                self.lbl_bookingBadge.isHidden = true
                self.imv_bookingBadge.isHidden = true
                self.lbl_pendingRequest.text = ""
                self.lbl_upcomingAppointsment.text = ""
            }
        }
    }
    
    func setBookingState() {
        var bookingBadge = 0
        var pendingRequest = ""
        var upcommingRequest = ""
        for one in ds_booking{
            if one.state == "0"{
                bookingBadge += 1
                pendingRequest += getTimeFromTimeStamp(Int64(one.service_date)) + "\n" + one.service_time! + "min" + "\n"
            }else if one.state == "2"{
                upcommingRequest += getTimeFromTimeStamp(Int64(one.service_date)) + "\n" + one.service_time! + "min" + "\n"
            }
        }
        self.lbl_pendingRequest.text = pendingRequest
        
        /*self.lbl_pendingRequest.text = "Jun/27/2020 10:00 AM \n 15min "
        self.lbl_pendingRequest.isScrollEnabled = false
        print("\(self.lbl_pendingRequest.text.count)")
        if self.lbl_pendingRequest.text.count > 48{
            self.lbl_pendingRequest.isScrollEnabled = true
        }*/
        
        self.lbl_upcomingAppointsment.text = upcommingRequest
        if bookingBadge != 0{
            self.lbl_bookingBadge.isHidden = false
            self.imv_bookingBadge.isHidden = false
            self.lbl_bookingBadge.text = "\(bookingBadge)"
            
        }else{
            self.lbl_bookingBadge.isHidden = true
            self.imv_bookingBadge.isHidden = true
        }
    }
    
    func loadLayout()  {
        let url = URL(string: user!.picture ?? "")
        self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
        self.lbl_fullName.text = (user?.first_name)! + " " + (user?.last_name)!
        swbtn_online.set(width: 35, height: 20)
        
    }
    
    @IBAction func gotoProfilePage(_ sender: Any) {
        rootcontrollerName1 = rootcontrollerNames1.SellerMainVC.rawValue
        self.gotoStoryBoardVC("GeneralProfileVC", fullscreen: true)
    }
    @IBAction func gotoMessagePage(_ sender: Any) {
        
        self.gotoVC("MessageViewVC")
        
    }
    @IBAction func gotoBookingPage(_ sender: Any) {
        
        
         self.gotoStoryBoardVC("BookingSellerVC", fullscreen: true)
      
        //self.gotoVC("BookingVC")
       
    }
    @IBAction func gotoHomePage(_ sender: Any) {
        
             return
        
        
    }
    @IBAction func gotoSettingpage(_ sender: Any) {
        
             self.gotoVC("SettingsVC")
        
        
    }
    @IBAction func gotoHelpPage(_ sender: Any) {
        self.gotoVC("HelpVC")
    }
}





