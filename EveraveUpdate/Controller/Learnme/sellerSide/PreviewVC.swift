//
//  PreviewVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/25/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//



import UIKit
import ExpyTableView
import ExpandableCell
import SwiftyJSON
import Cosmos

class PreviewVC: BaseVC1 {
    
    @IBOutlet weak var ServicesView: ExpyTableView!
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_sellerName: UILabel!
    @IBOutlet weak var lbl_serviceCategory: UILabel!
    @IBOutlet weak var lbl_currentTime: UILabel!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var lbl_aboutme: UILabel!
    @IBOutlet weak var lbl_toolsneeded: UILabel!
    @IBOutlet weak var lbl_languages: UILabel!
    
    @IBOutlet weak var lbl_montime: UILabel!
    @IBOutlet weak var lbl_tuetime: UILabel!
    @IBOutlet weak var lbl_wedtime: UILabel!
    @IBOutlet weak var lbl_thrtime: UILabel!
    @IBOutlet weak var lbl_fritime: UILabel!
    @IBOutlet weak var lbl_sattime: UILabel!
    @IBOutlet weak var lbl_suntime: UILabel!
    @IBOutlet weak var col_reviews: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbl_reviews: UILabel!
    @IBOutlet weak var lbl_portfolios: UILabel!
    @IBOutlet weak var lbl_ratingVal: UILabel!
    @IBOutlet weak var lbl_reviewNum: UILabel!
    
    var CategoriesList = [ServiceModel]()
    var ds_reviews = [ReviewModel]()
    var ds_portfolios = [PortfolioModel]()
    var cellHeight = 40 // expandable table view cell height
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        toptabType = topTab.review.rawValue
        setSellerProfile()
        self.lbl_currentTime.text = getCurrentDateStr()
        
        /*DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            //self.ServicesView.expand(2)
            }
        )*/
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getDataSource4Service()
        getDataSource4Reviews()
        //getDataSource4Portfolios()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // selected catview reload data
        ServicesView.rowHeight = 40.0 // table view row height defined
        ServicesView.expandingAnimation = .bottom
        ServicesView.collapsingAnimation = .top
        ServicesView.tableFooterView = UIView()
        // selectedCattable View set
        ServicesView.dataSource = self
        ServicesView.delegate = self
        ServicesView.reloadData()
        tableViewheight.constant = ServicesView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func localTime(in timeZone: String) -> String {
        let f = ISO8601DateFormatter()
        f.formatOptions = [.withInternetDateTime]
        f.timeZone = TimeZone(identifier: timeZone)
        return f.string(from: Date())
    }
    
    @objc func orientationDidChange() {
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight: ServicesView.reloadSections(IndexSet(Array(ServicesView.expandedSections.keys)), with: .none)
        default:break
        }
    }
    
    func setSellerProfile() {
        if let this_seller = thisSeller{
            self.lbl_sellerName.text = user?.user_name
            var num = 0
            var serviceCategories = ""
            for one in this_seller.serviceModelList{
                num += 1
                if num != this_seller.serviceModelList.count{
                    serviceCategories = serviceCategories + one.name! + ","
                }else{
                    serviceCategories = serviceCategories + one.name!
                }
            }
            
            self.lbl_serviceCategory.text = serviceCategories
            let url = URL(string: user!.picture ?? "")
            self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            self.lbl_aboutme.text = aboutMe
            self.lbl_toolsneeded.text = tools
            self.lbl_languages.text = languages
            self.lbl_ratingVal.text = ratingVal
            if countVal.toInt() == 0 || countVal == ""{
                self.lbl_reviewNum.text = "(0 Reviews)"
            }else{
                self.lbl_reviewNum.text = "(" + countVal +  "Reviews)"
            }
            
            
            self.lbl_montime.text = availableTimes[0]
            self.lbl_tuetime.text = availableTimes[1]
            self.lbl_wedtime.text = availableTimes[2]
            self.lbl_thrtime.text = availableTimes[3]
            self.lbl_fritime.text = availableTimes[4]
            self.lbl_sattime.text = availableTimes[5]
            self.lbl_suntime.text = availableTimes[6]
        }
    }
    
    func getDataSource4Service() {
       if let this_seller = thisSeller{
           self.CategoriesList = this_seller.serviceModelList
            for i in 0 ..< CategoriesList.count {
                ServicesView.expandedSections[i] = true
            }
       }
    }
    
    func getDataSource4Reviews() {
        self.showLoadingView(vc: self)
        ApiManager.getReview(seller_id: "\(user!.id ?? 0)") { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let dict = JSON(data as Any)
                //dump(dict, name: "dict=====>")
                self.ds_reviews.removeAll()
                let review_info = dict["review_info"].arrayObject
                if let reviewinfo = review_info{
                    var num = 0
                    for one in reviewinfo{
                        num += 1
                        let json = JSON(one)
                        self.ds_reviews.append(ReviewModel(json))
                        if num == reviewinfo.count{
                            self.col_reviews.reloadData()
                            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                            layout.minimumInteritemSpacing = 0
                            layout.minimumLineSpacing = 0
                            let height =  self.col_reviews.collectionViewLayout.collectionViewContentSize.height
                            self.collectionViewHeight.constant = height
                            self.view.layoutIfNeeded()
                        }
                    }
                }
            }
        }
    }
    
    func getDataSource4Portfolios() {
        self.showLoadingView(vc: self)
        ApiManager.getPortofolio(user_id: "\(user!.id ?? 0)") { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let dict = JSON(data as Any)
                //dump(dict, name: "dict=====>")
                self.ds_portfolios.removeAll()
                let portfolio_info = dict["portofolios"].arrayObject
                if let portfolioinfo = portfolio_info{
                    var num = 0
                    for one in portfolioinfo{
                        num += 1
                        let json = JSON(one)
                        self.ds_portfolios.append(PortfolioModel(json))
                        if num == portfolioinfo.count{
                            self.col_reviews.reloadData()
                            let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                            layout.minimumInteritemSpacing = 0
                            layout.minimumLineSpacing = 0
                            let height =  self.col_reviews.collectionViewLayout.collectionViewContentSize.height
                            self.collectionViewHeight.constant = height
                            self.view.layoutIfNeeded()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func gotoAppointment(_ sender: Any) {
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
    
    @IBAction func gotoReviews(_ sender: Any) {
        self.lbl_reviews.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.lbl_reviews.textColor = .black
        self.lbl_portfolios.font = UIFont.systemFont(ofSize: 15.0)
        self.lbl_portfolios.textColor = .lightGray
        self.collectionViewHeight.constant = 0
        toptabType = topTab.review.rawValue
        self.getDataSource4Reviews()
    }
    
    @IBAction func gotoPortfolios(_ sender: Any) {
        self.lbl_portfolios.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.lbl_portfolios.textColor = .black
        self.lbl_reviews.font = UIFont.systemFont(ofSize: 15.0)
        self.lbl_reviews.textColor = .lightGray
        self.collectionViewHeight.constant = 0
        toptabType = topTab.portfolio.rawValue
        self.getDataSource4Portfolios()
    }
    
    @IBAction func gotoMessage(_ sender: Any) {
        
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension PreviewVC : ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelCatCell.self)) as! SelCatCell
        cell.labelCatName.text = CategoriesList[section].name
        cell.layoutMargins = UIEdgeInsets.zero
        cell.showSeparator()
        return cell
    }
}

// Selected category autodimention
extension PreviewVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        //print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }*/
}

//MARK: UITableView Data Source Methods
extension PreviewVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        //print("\(CategoriesList.count)")
        return CategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Row count for section \(section) is \(CategoriesList[section].miniFood.count)")
        return CategoriesList[section].timePriceModelList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailValTableViewCell.self)) as! DetailValTableViewCell
        //  expandable TableView changePart
        cell.labelselectedName.text = CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].time! + "min"
        cell.labelselectedvalue.text = "$" + CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].price!
        cell.btn_appointment.tag = indexPath.section
        cell.btn_appointment.superview?.tag = indexPath.row
        cell.layoutMargins = UIEdgeInsets.zero
         //cell.showSeparator()
        cell.hideSeparator()
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension PreviewVC : ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            return
            
        case .willCollapse:
            return
            
        case .didExpand:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant += CGFloat(detailnum * cellHeight )
            tableViewheight.constant = ServicesView.contentSize.height
            
        case .didCollapse:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant -= CGFloat(detailnum * cellHeight)
            tableViewheight.constant = ServicesView.contentSize.height
        }
    }
}

extension PreviewVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                return self.ds_reviews.count
            }else{
                return self.ds_portfolios.count
            }
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reviewCell", for: indexPath) as! reviewCell
                cell.entity = self.ds_reviews[indexPath.row]
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "portfolioCell", for: indexPath) as! portfolioCell
                cell.entity = self.ds_portfolios[indexPath.row]
                return cell
            }
        }else{
            return UICollectionViewCell.init()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension PreviewVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                let w = collectionView.frame.size.width
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                formatter.roundingMode = NumberFormatter.RoundingMode.ceiling
                formatter.maximumFractionDigits = 0
                
                let content = ds_reviews[indexPath.row].review
                let font = UIFont.systemFont(ofSize: 12)
                let title_width = content!.size(OfFont: font).width // size: {w: 98.912 h: 14.32}
                let title_height = content!.size(OfFont: font).height
                let rate : CGFloat = title_width / (UIScreen.main.bounds.width - 20)
                
                let roundedValue = formatter.string(for: rate)
                let prefix_number = roundedValue?.toInt()
                let toptitle_height = title_height * CGFloat(prefix_number!)
                
                let h: CGFloat = 90 + toptitle_height
                return CGSize(width: w, height: h)
            }else{
                let w = collectionView.frame.size.width
                let h = w / 4 * 3
                return CGSize(width: w, height: h)
            }
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
}
