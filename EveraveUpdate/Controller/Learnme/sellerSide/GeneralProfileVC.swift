//
//  GeneralProfileVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/11/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import SKCountryPicker
import Photos
import DKImagePickerController
import DKCamera

class GeneralProfileVC : BaseVC1, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cons_contryName: NSLayoutConstraint!
    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var signCaption: UILabel!
    @IBOutlet weak var edtfirstName: UITextField!
    @IBOutlet weak var edtlastName: UITextField!
    @IBOutlet weak var edtuserName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPhone: UITextField!
    @IBOutlet weak var edtDob: UITextField!
    @IBOutlet weak var edtssn: UITextField!
    @IBOutlet weak var imv_profile: UIImageView!
    
    var firstName = ""
    var lastName = ""
    var usertName = ""
    var email = ""
    var phone = ""
    var dob = ""
    var ssn = ""
    
    let validator = Validator()
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        loadLayout()
        imv_profile.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 1
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
         self.assets.removeAll()
        self.assets = assets
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.gotoUploadProfile(image)
                })
            }
        }
        else {}
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_profile.image = image
        }
    }
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode.text = country.dialingCode!
            self.countryName.text = country.countryName
            self.cons_contryName.constant = CGFloat(country.countryName.count * 8)
            
            /*print("country.countryName: ", country.countryName)
            print("country.countryCode: ", country.countryCode)
            print("country.digitCountrycode: ", country.digitCountrycode)
            print("country.dialingCode: ", country.dialingCode)*/
            
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
    }
    
    func loadLayout() {
        countryCode.text = "+1"
        countryName.text = "United States"
        signCaption.text = "Next"
        edtfirstName.text = user?.first_name
        edtlastName.text = user?.last_name
        edtuserName.text = user?.user_name
        edtEmail.text = user?.email
        edtPhone.text = user?.phone
        edtssn.text = user?.ssn
        edtDob.text = user?.dob
        let url = URL(string: user!.picture ?? "")
        self.imv_profile.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
    }
    
    @IBAction func goBack(_ sender: Any) {
        
        if user!.approve == "0"{
            self.gotoStoryBoardVC("BusinessProfileVC", fullscreen: true)
        }else{
            if let rootcontroller = rootcontrollerName1{
                if rootcontroller == rootcontrollerNames1.SettingsVC.rawValue{
                    self.gotoVC("SettingsVC")
                }else {
                    self.gotoVC("SellerMainVC")
                }
            }
        }
    }
    
    func editInit() {
        setEdtPlaceholder(edtfirstName, placeholderText: "Firstname", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtlastName, placeholderText: "Lastname", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtuserName, placeholderText: "Username", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtEmail, placeholderText: "Email ID", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtPhone, placeholderText: "Phone number", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtDob, placeholderText: "DOB", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtssn, placeholderText: "SSN last four", placeColor: UIColor.lightGray, padding: .left(40))
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        firstName = self.edtfirstName.text ?? ""
        lastName = self.edtlastName.text ?? ""
        usertName = self.edtuserName.text ?? ""
        email = self.edtEmail.text ?? ""
        phone = self.edtPhone.text ?? ""
        dob = self.edtDob.text ?? ""
        ssn = self.edtssn.text ?? ""
        
        if firstName.isEmpty{
            self.progShowInfo(true, msg: "Enter your First name.")
            return
        }
        if lastName.isEmpty{
            self.progShowInfo(true, msg: "Enter your Family name.")
            return
        }
        if usertName.isEmpty{
            self.progShowInfo(true, msg: "Enter your User name.")
            return
        }
        if email.isEmpty{
            self.progShowInfo(true, msg: "Enter your email.")
            return
        }
        if phone.isEmpty{
            self.progShowInfo(true, msg: "Enter your phone number")
            return
        }
        if dob.isEmpty{
            self.progShowInfo(true, msg: "Enter your DOB")
            return
        }
        if ssn.isEmpty{
            self.progShowInfo(true, msg: "Enter your SSN")
            return
        }
        
        if !isValidEmail(testStr: email){
            self.progShowInfo(true, msg: "Invalid email")
            return
        }else{
            self.showLoadingView(vc: self)
            
            ApiManager.editProfile(photo: imageFils.first ?? "", first_name: firstName, last_name: lastName, user_name: usertName, email: email, phone: phone, dob: dob, gender: "man", ssn: ssn) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let user_image = data as! String
                    user?.first_name = self.firstName
                    user?.last_name = self.lastName
                    user?.user_name = self.usertName
                    user?.email = self.email
                    user?.phone = self.phone
                    user?.dob = self.dob
                    user?.ssn = self.ssn
                    if !user_image.isEmpty{
                        user?.picture = user_image
                    }
                    user?.saveUserInfo()
                    user?.loadUserInfo()
                    self.gotoProfessionalProfile()
                }
            }
        }
    }
    
    func gotoProfessionalProfile()  {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ServiceProfileVC", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: "ServiceProfileNav")
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
}



