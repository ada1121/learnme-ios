
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import Photos
import DKImagePickerController
import DKCamera
import SwiftyJSON
import SwiftyUserDefaults

class UploadPicturesVC: BaseVC1{

    var pictureData  = [PortfolioModel]()
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    var num = -1
    
    @IBOutlet weak var ui_picture_coll: UICollectionView!
    @IBOutlet weak var uiv_upload: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiv_upload.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if num == -1{
            self.getDataSource4Portfolios()
            num += 1
        }
    }
    
    func getDataSource4Portfolios() {
        self.showLoadingView(vc: self)
        ApiManager.getPortofolio(user_id: "\(user?.id ?? 0)") { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let dict = JSON(data as Any)
                self.pictureData.removeAll()
                let portfolio_info = dict["portofolios"].arrayObject
                if let portfolioinfo = portfolio_info{
                    var num = 0
                    for one in portfolioinfo{
                        num += 1
                        let json = JSON(one)
                        self.pictureData.append(PortfolioModel(json))
                        if num == portfolioinfo.count{
                            self.ui_picture_coll.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func onClickItemClose(_ sender: UIButton) {
        self.showLoadingView(vc: self)
        let button = sender as UIButton
        let pos = button.tag
        ApiManager.removePortofolio(id: pictureData[pos].id!) { (isSuccess, true) in
            self.hideLoadingView()
            if isSuccess{
                self.pictureData.remove(at: pos)
                self.ui_picture_coll.reloadData()
            }else{
                self.progShowInfo(true, msg: "Network issue!")
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "ServiceProfileVC", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: "ServiceProfileNav")
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 1
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
        self.assets.removeAll()
       self.assets = assets
       if assets.count > 0 {
           for asset in assets {
               asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                   self.gotoUploadProfile(image)
               })
           }
       }
       else {}
   }
    
    func gotoUploadProfile(_ image: UIImage?) {
        
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
        }
        self.showLoadingView(vc: self)
        ApiManager.uploadPortofolio(image: imageFils[0]) { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.getDataSource4Portfolios()
            }
        }
    }
}

//MARK: UICollectionViewDataSource
extension UploadPicturesVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return pictureData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "uploadPictureCell", for: indexPath) as! uploadPictureCell
            
        cell.entity = pictureData[indexPath.row]
        cell.btn_close.tag = indexPath.row
        return cell
    }
}

extension UploadPicturesVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let pos = indexPath.row
        // TODO: goto detail show vc
    }
}

extension UploadPicturesVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h = collectionView.frame.size.height * 0.9
        let w = h / 4 * 3
        return CGSize(width: w, height: h)
    }
}

public protocol ImagePickerDelegate: class {
    func didSelect(image: UIImage?)
}

