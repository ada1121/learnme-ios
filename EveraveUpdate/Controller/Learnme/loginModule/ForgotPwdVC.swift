//
//  ForgotPwdVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//
import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import SKCountryPicker
import Photos
import DKImagePickerController
import DKCamera
import FirebaseAuth
import KAPinField

class ForgotPwdVC: BaseVC1 {

    @IBOutlet weak var otpCodeView: KAPinField!
    @IBOutlet weak var edt_phoneNumber: UITextField!
    @IBOutlet weak var btn_submit: UIButton!
    var verificationID = ""
    @IBOutlet weak var uiv_dlg: UIView!
    @IBOutlet weak var uiv_dlgBack: UIView!
    
    @IBOutlet weak var edt_newPwd: UITextField!
    @IBOutlet weak var confirmPwd: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setStyle()
        self.setDlg()
        self.edt_phoneNumber.becomeFirstResponder()
    }
    
    func setDlg()  {
        self.uiv_dlgBack.isHidden = true
        self.uiv_dlg.isHidden = true
        self.edt_newPwd.text = ""
        self.confirmPwd.text = ""
    }
    
    @IBAction func gotoSend(_ sender: Any) {
        
        let phone = self.edt_phoneNumber.text
        if phone!.count < 9 || phone!.length > 15{
            self.progShowInfo(true, msg: "Invalid phone number")
            return
        }
        
        else if phone != ""{
            var phoneNumber = phone
            if (!(phone?.contains("+"))!){
                phoneNumber = "+1" + phone!
            }
            self.showLoadingView(vc: self)
            ApiManager.forgot(phone: (phoneNumber?.trim())!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                     self.sendOTP()
                }else{
                    self.showAlerMessage(message: "Phone number not exist")
                }
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func sendNewPwd(_ sender: Any) {
        let newPwd = self.edt_newPwd.text
        let confirmPwd = self.confirmPwd.text
        if newPwd == "" || confirmPwd == ""{
            self.showAlerMessage(message: "Enter your password")
            return
        }
        if confirmPwd != newPwd{
            self.showAlerMessage(message: "Confirm your password")
            return
        }else{
            self.showLoadingView(vc: self)
            ApiManager.resetPassword(phone: self.edt_phoneNumber.text!, password: newPwd!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.uiv_dlgBack.isHidden = true
                    self.uiv_dlg.isHidden = true
                    self.edt_newPwd.text = ""
                    self.confirmPwd.text = ""
                    self.showAlerMessage(message: "Updated your password!")
                }else{
                    self.showAlerMessage(message: "Network issue.")
                    self.uiv_dlgBack.isHidden = true
                    self.uiv_dlg.isHidden = true
                    self.edt_newPwd.text = ""
                    self.confirmPwd.text = ""
                }
            }
        }
    }
    
    @IBAction func cancelPwd(_ sender: Any) {
        self.uiv_dlgBack.isHidden = true
        self.uiv_dlg.isHidden = true
        self.edt_newPwd.text = ""
        self.confirmPwd.text = ""
    }
    
    func sendOTP()  {
        if self.btn_submit.currentTitle == "SUBMIT"{
            var phonenumber = self.edt_phoneNumber.text!
            if !phonenumber.contains("+"){
                phonenumber = "+1" + phonenumber
            }
            self.showLoadingView(vc: self)
            Auth.auth().settings!.isAppVerificationDisabledForTesting = false
            PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) { (verificationID, error) in
                self.hideLoadingView()
                    if let error = error {
                        let errStr = error.localizedDescription
                        let invalidPhoneTypes = ["TOO_LONG", "TOO_SHORT", "Invalid format"]
                        
                        if errStr == "MISSING_CLIENT_IDENTIFIER" {
                            self.showAlerMessage(message: "You have tried too many times using this device! You cannot log in now. Please wait.")
                        } else if invalidPhoneTypes.contains(errStr) {
                            self.showAlerMessage(message: "Invalid Phone number.")
                        } else {
                            self.showAlerMessage(message: "Somethings wrong!")
                        }
                        
                        return
                    }
                self.showAlerMessage(message: "Please input your verification code.")
                self.btn_submit.setTitle("RESENT", for: .normal)
                self.verificationID = verificationID ?? ""
                self.dismissKeyboard()
            }
        }else{
            refreshPinField()
            var phonenumber = self.edt_phoneNumber.text!
            if !phonenumber.contains("+"){
                phonenumber += "+1" + phonenumber
            }
            self.showLoadingView(vc: self)
            Auth.auth().settings!.isAppVerificationDisabledForTesting = false
            PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) { (verificationID, error) in
                self.hideLoadingView()
                    if let error = error {
                        let errStr = error.localizedDescription
                        let invalidPhoneTypes = ["TOO_LONG", "TOO_SHORT", "Invalid format"]
                        
                        if errStr == "MISSING_CLIENT_IDENTIFIER" {
                            self.showAlerMessage(message: "You have tried too many times using this device! You cannot log in now. Please wait.")
                        } else if invalidPhoneTypes.contains(errStr) {
                            self.showAlerMessage(message: "Invalid Phone number.")
                        } else {
                            print("error sring ===>",errStr)
                            self.showAlerMessage(message: "Somethings wrong!")
                        }
                        
                        return
                    }
                self.showAlerMessage(message: "Please input your verification code.")
                self.verificationID = verificationID ?? ""
                self.dismissKeyboard()
            }
        }
    }
    
    func setStyle() {
        otpCodeView.properties.delegate = self
//        otpCodeView.properties.token = "-"
        otpCodeView.properties.animateFocus = true
        otpCodeView.text = ""
        otpCodeView.keyboardType = .numberPad
        otpCodeView.properties.numberOfCharacters = 6
        otpCodeView.appearance.tokenColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.tokenFocusColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.textColor = UIColor.black
        otpCodeView.appearance.font = .menlo(40)
        otpCodeView.appearance.kerning = 24
        otpCodeView.appearance.backOffset = 5
        otpCodeView.appearance.backColor = UIColor.clear
        otpCodeView.appearance.backBorderWidth = 1
        otpCodeView.appearance.backBorderColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.backCornerRadius = 4
        otpCodeView.appearance.backFocusColor = UIColor.clear
        otpCodeView.appearance.backBorderFocusColor = UIColor.black.withAlphaComponent(0.8)
        otpCodeView.appearance.backActiveColor = UIColor.clear
        otpCodeView.appearance.backBorderActiveColor = UIColor.black
        otpCodeView.appearance.backRounded = false
        
    }
    
     func refreshPinField() {
        otpCodeView.text = ""
//        UIPasteboard.general.string = targetCode
        setStyle()
    }
}

extension ForgotPwdVC : KAPinFieldDelegate {
    
    func pinField(_ field: KAPinField, didChangeTo string: String, isValid: Bool) {
        if isValid {
            print("Valid input: \(string) ")
        } else {
            print("Invalid input: \(string) ")
            self.otpCodeView.animateFailure()
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        
        print("didFinishWith : \(code)")
        
        //self.showLoadingView(vc: self, label: "Requesting...")
        self.showLoadingView(vc: self)
        self.dismissKeyboard()
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID , verificationCode: code)
        
        //print("verifycode ==> ",verificationCode)
        Auth.auth().signIn(with: credential) { authData, error in

            if let error = error {
                //self.hideLoadingView()
                self.hideLoadingView()
                //self.showError(error.localizedDescription)
                self.showAlerMessage(message: error.localizedDescription)
                field.animateFailure()
                self.otpCodeView.becomeFirstResponder()
                return
            }
            
            self.hideLoadingView()
            field.animateSuccess(with: "👍") {
                print("OK")
                self.uiv_dlg.isHidden = false
                self.uiv_dlgBack.isHidden = false
            }
        }
    }
}
