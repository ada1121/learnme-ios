//
//  SplashVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON

class SplashVC: BaseVC1 {
    
    var networkStatus = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0, execute: {
                self.networkStatus = 1
                if self.networkStatus == 1{
                    self.gotoVC("StartSC")
                    self.showAlerMessage(message: "No  Internet connection")
                }
                else{
                    return
                }
            }
        )
        self.checkBackgrouond()
    }
    
    func checkBackgrouond(){
        if UserDefault.getBool(key: PARAMS.LOGOUT,defaultValue: false){
            self.gotoVC("StartSC")
        }
        
        ApiManager.login(emailorphone: user!.email!, password: user!.password!) { (isSuccess, data) in
            if isSuccess{
                let data = JSON(data as Any)
                print("data =========>",data)
                let password = user!.password!
                user?.clearUserInfo()
                user = UserModel(data)
                user?.password = password
                user?.saveUserInfo()
                user?.loadUserInfo()
                dump(user, name: "userinfo =========>")
                
                if user?.type == PARAMS.SELLER{
                    if user?.approve == "1"{
                            self.gotoVC("SellerMainVC")
                    }else{
                        if user?.profile_completed == "0"{
                            self.gotoStoryBoardVC("BusinessProfileVC", fullscreen: true)
                        }else{
                            self.gotoVC("LoginNav")
                        }
                    }
                }else{
                    self.gotoStoryBoardVC("MainVC", fullscreen: true)
                }
            }else{
                self.gotoVC("StartSC")
            }
        }
    }
}

