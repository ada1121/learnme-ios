//
//  SignUpVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/9/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import SKCountryPicker
import Photos
import DKImagePickerController
import DKCamera
import FirebaseAuth

class SignUpVC: BaseVC1, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cons_contryName: NSLayoutConstraint!

    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var edtfirstName: UITextField!
    @IBOutlet weak var edtlastName: UITextField!
    @IBOutlet weak var edtuserName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPhone: UITextField!
    @IBOutlet weak var edtDob: UITextField!
    @IBOutlet weak var edtSsn: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    @IBOutlet weak var confirmPwd: UITextField!
    @IBOutlet weak var checkBox: GDCheckbox!
    @IBOutlet weak var uiv_ssn: UIView!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var cons_edtPassword: NSLayoutConstraint!
    
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    var firstName = ""
    var lastName = ""
    var usertName = ""
    var email = ""
    var phone = ""
    var dob = ""
    var ssn = ""
    var password = ""
    var confirmpassword = ""
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.edtDob.text = birthdate
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editInit()
        loadLayout()
        imv_avatar.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        //test()
    }
    
    func test()  {
        self.edtfirstName.text = "qqq"
        self.edtlastName.text = "www"
        self.edtuserName.text = "qqq"
        self.edtEmail.text = "qqq@gmail.com"
        self.edtDob.text = "2000-08-09"
        self.edtSsn.text = "12345"
        self.edtPwd.text = "123456"
        self.confirmPwd.text = "123456"
    }
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode.text = country.dialingCode!
            self.countryName.text = country.countryName
            self.cons_contryName.constant = CGFloat(country.countryName.count * 8)
            
            /*print("country.countryName: ", country.countryName)
            print("country.countryCode: ", country.countryCode)
            print("country.digitCountrycode: ", country.digitCountrycode)
            print("country.dialingCode: ", country.dialingCode)*/
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
        
    }
    
    func loadLayout() {
        countryCode.text = "+1"
        countryName.text = "United States"
        if user!.type == PARAMS.SELLER{
            self.uiv_ssn.isHidden = false
        }else{
            self.uiv_ssn.isHidden = true
            self.cons_edtPassword.constant += 60
        }
        self.edtPhone.text = "" //  for  saving nil
    }
    
    func editInit() {
        setEdtPlaceholder(edtfirstName, placeholderText: "First name", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtlastName, placeholderText: "Last name", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtuserName, placeholderText: "User name", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtEmail, placeholderText: "Email ID", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtPhone, placeholderText: "Phone number", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtSsn, placeholderText: "Last 4 digits of SSN", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtPwd, placeholderText: "Password", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(confirmPwd, placeholderText: "Confirm Password", placeColor: UIColor.lightGray, padding: .left(45))
        setEdtPlaceholder(edtDob, placeholderText: "DOB", placeColor: UIColor.lightGray, padding: .left(45))
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 1
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
         self.assets.removeAll()
        self.assets = assets
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.gotoUploadProfile(image)
                })
            }
        }
        else {}
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_avatar.image = image
        }
    }
    
    @IBAction func gotoTerms(_ sender: Any) {
        self.gotoVC("TermsVC")
    }
    @IBAction func gotoPrivacy(_ sender: Any) {
        self.gotoVC("PrivacyVC")
    }
    /*@IBAction func onmaleRadioBoxPress(_ sender: GDCheckbox) {
        let state = sender.isOn ? "ON" : "OFF"
        if state == "ON"{
            self.cus_femaleRadio.isOn = false
        }else{
            self.cus_femaleRadio.isOn = true
        }
    }
    
    @IBAction func onfemaleRadioBoxPress(_ sender: GDCheckbox) {
        let state = sender.isOn ? "ON" : "OFF"
        if state == "ON"{
            self.cus_maleRadio.isOn = false
        }else{
            self.cus_maleRadio.isOn = true
        }
    }*/
    @IBAction func gotoDob(_ sender: Any) {
        self.gotoNavPresent1("CalnedarView", fullscreen: false)
    }
    
    @IBAction func signupBtnClicked(_ sender: Any) {
        firstName = self.edtfirstName.text ?? ""
        lastName = self.edtlastName.text ?? ""
        usertName = self.edtuserName.text ?? ""
        email = self.edtEmail.text ?? ""
        phone = self.countryCode.text! + self.edtPhone.text!
        dob = self.edtDob.text ?? ""
        ssn = self.edtSsn.text ?? ""
        password = self.edtPwd.text ?? ""
        confirmpassword = self.confirmPwd.text ?? ""
        
        if self.imageFils.count == 0{
            self.progShowInfo(true, msg: "Take a photo")
            return
        }
        
        if firstName.isEmpty{
            self.progShowInfo(true, msg: "Enter your First name.")
            return
        }
        if lastName.isEmpty{
            self.progShowInfo(true, msg: "Enter your Family name.")
            return
        }
        if usertName.isEmpty{
            self.progShowInfo(true, msg: "Enter your User name.")
            return
        }
        if email.isEmpty{
            self.progShowInfo(true, msg: "Enter your email.")
            return
        }
        if phone.isEmpty{
            self.progShowInfo(true, msg: "Enter your phone number")
            return
        }
        if dob.isEmpty{
            self.progShowInfo(true, msg: "Enter your DOB")
            return
        }
        
        if user?.type == PARAMS.SELLER{
            if ssn.isEmpty{
                self.progShowInfo(true, msg: "Enter your last 4 digits of SSN")
                return
            }
        }
        
        if password == ""{
            self.progShowInfo(true, msg: "Enter your password")
            return
        }
        
        if password.count < 6{
            self.progShowInfo(true, msg: "The Password should be at least 6 character")
            return
        }
        
        if confirmpassword == "" {
            self.progShowInfo(true, msg: "Enter your confirm password")
            return
        }
        
        if confirmpassword != password{
            self.progShowInfo(true, msg: "Confirm your password")
            return
        }
        
        if !isValidEmail(testStr: email){
            self.progShowInfo(true, msg: "Invalid email")
            return
        }
        
        if phone.count < 9 || phone.length > 15{
            self.progShowInfo(true, msg: "Invalid phone number")
            return
        }
        
        if !self.checkBox.isOn{
            self.progShowInfo(true, msg: "Are you agree to the Terms & Privacy Police?")
            return
        }
        
        else{
            //TODO: SEND OTP ACTION
            sign_photo = self.imageFils.first
            sign_firstName = firstName
            sign_lastName = lastName
            sign_userName = usertName
            sign_email = email
            sign_phone = self.countryCode.text! + self.edtPhone.text!
            sign_dob = dob
            sign_gender = "man"
            sign_ssn = ssn
            sign_password = password
            sign_device_token = Constants.deviceToken
            sign_type = user!.type
            self.sendOTP()
        }
    }
    
    func sendOTP()  {
        guard let phoneNum = edtPhone.text, phoneNum != "" else {
            self.showAlerMessage(message: "Invalid number")
            return
        }
        
        let phonenumber = self.countryCode.text! + self.edtPhone.text!
        
        Auth.auth().settings!.isAppVerificationDisabledForTesting = false
        //self.showLoadingView(vc: self)
        self.showLoadingView(vc: self)
        PhoneAuthProvider.provider().verifyPhoneNumber(phonenumber, uiDelegate: nil) { (verificationID, error) in
            //self.hideLoadingView()
            self.hideLoadingView()
                if let error = error {
                    let errStr = error.localizedDescription
                    let invalidPhoneTypes = ["TOO_LONG", "TOO_SHORT", "Invalid format"]
                    
                    if errStr == "MISSING_CLIENT_IDENTIFIER" {
                        self.showAlerMessage(message: "SMS quota exceeded")
                    } else if invalidPhoneTypes.contains(errStr) {
                        self.showAlerMessage(message: "Enter correct Phone Number")
                    } else {
                        self.showAlerMessage(message: "Network issue!")
                    }
                    
                    return
                }
          
            OTP_countryCode = self.countryCode.text!
            OTP_phonenumber = self.edtPhone.text!
            OTP_verifyID = verificationID
            self.dismissKeyboard()
            self.gotoNavPresent1("ConfirmOTPVC",fullscreen: false)
        }
    }
    
    @IBAction func gotoLogin(_ sender: Any) {
        self.navigationController?.popToRootViewController(animated: true)
    }
}

