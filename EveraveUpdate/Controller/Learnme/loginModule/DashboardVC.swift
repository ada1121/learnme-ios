//
//  DashboardVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class DashboardVC: BaseVC1 {

    @IBOutlet weak var topCaptionView: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        self.createGradientLabel(self.topCaptionView, letter: "Are you a:", fontsize: 30, position: 0)
        user?.clearUserInfo()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UserDefault.setString(key: PARAMS.TYPE, value: nil)
        UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
    }
    @IBAction func buyerBtnClicked(_ sender: Any) {
        //self.gotoStoryBoardVC("MainVC", fullscreen: true)
        user?.clearUserInfo()
        UserDefault.setString(key: PARAMS.TYPE, value: "buyer")
        user?.saveUserInfo()
        user?.loadUserInfo()
        //self.gotoVC("LoginNav")
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
        
    }
    @IBAction func sellerBtnClicked(_ sender: Any) {
        user?.clearUserInfo()
        UserDefault.setString(key: PARAMS.TYPE, value: "seller")
        user?.saveUserInfo()
        user?.loadUserInfo()
        self.gotoVC("LoginNav")
    }
}
