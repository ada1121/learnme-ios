//
//  StartSC1.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/12/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import CHIPageControl
import SwiftyJSON

class StartSC : BaseVC1 {

    internal let numberOfPages = 3
    var progress = 0.0
    
    @IBOutlet var pageControls: [CHIBasePageControl]!
    @IBOutlet weak var ui_collection: UICollectionView!
    @IBOutlet weak var ui_skipBut: UIButton!
      
      var datasource = [IntroModel]()
      
      let imgNames = ["w1", "w2", "w3"]
      let Conttitle = ["Welcome to learnme",
                  "Reinventing Services",
                  "Putting you in the driver's seat"]
      let Lbltop = [
          "Search and book live video appointments",
          "Book one on one personalized sessions with the",
          "Learn the top tips and tricks from industry leading"
      ]
    
    let Lblbottom = [
           "with service professionals worldwide.",
           "world’s best professionals without having to travel.",
           "specialists and get customized personal instructions."
       ]
    let lblBtn = [
        "Next",
        "Next",
        "Join"
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.pageControls.forEach { (control) in
            control.numberOfPages = self.numberOfPages
        }
        
        for i in 0 ..< imgNames.count {
            
            let one = IntroModel(imgBack: imgNames[i], title: Conttitle[i], lbltop: Lbltop[i], lblbottom: Lblbottom[i], lblbtn: lblBtn[i])
            datasource.append(one)
        }
    }
  
    @IBAction func onClickNext(_ sender: Any) {
          
          progress += 1.0
          if progress == 3.0 {
              self.gotoVC("DashboardVC")
              return
          }
          ui_collection.scrollToItem(at: IndexPath.init(row: Int(progress), section: 0) , at: .centeredHorizontally, animated: true)
          self.pageControls.forEach { (control) in
              control.progress = progress
          }
      }
    
    @IBAction func onClickSkip(_ sender: Any) {
        self.gotoVC("DashboardVC")
    }

    func showSkipButton() {
        if progress < 2.0 {
            ui_skipBut.isHidden = false
        } else {
            ui_skipBut.isHidden = true
        }
    }
    
}

extension StartSC : UICollectionViewDelegate, UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return datasource.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IntroCell", for: indexPath) as! IntroCell
        cell.entity = datasource[indexPath.row]
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let total = scrollView.contentSize.width - scrollView.bounds.width
        let offset = scrollView.contentOffset.x
        let percent = Double(offset / total)
        progress = percent * Double(self.numberOfPages - 1)
        
        self.pageControls.forEach { (control) in
            control.progress = progress
        }
        
        showSkipButton()
    }
}

extension StartSC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let w = collectionView.frame.size.width
        let h = collectionView.frame.size.height
        
        return CGSize(width: w, height: h)
    }
}
