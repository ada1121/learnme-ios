//
//  LoginVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView

class LoginVC: BaseVC1,ValidationDelegate, UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var lbl_signin: UILabel!
    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPwd: UITextField!
    
    let validator = Validator()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        navBarHidden()
        self.hideKeyboardWhenTappedAround()
        //loadLayout()
        lbl_signin.text = "Sign In"
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //createGradientView(signBtnView)
        //self.createGradientLabel(self.userName, letter: user!.surname!, fontsize: 20, position: 1)
    }
    
    @IBAction func gotoDashboard(_ sender: Any) {
        self.gotoVC("DashboardVC")
    }
    
    func editInit() {
        setEdtPlaceholder(edtEmail, placeholderText: "Email or Phone", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtPwd, placeholderText: "Password", placeColor: UIColor.lightGray, padding: .left(40))
    }
    
    func loadLayout() {
         //edtEmail.text = "bguess516@gmail.com"
        if user!.type == PARAMS.BUYER{
            edtEmail.text = "rrr@rrr.rrr"
            edtPwd.text = "123456"
        }else{
            edtEmail.text = "ppp@ppp.ppp"
            edtPwd.text = "123456"
        }
    }
    
    @IBAction func signInBtnClicked(_ sender: Any) {
        validator.registerField(edtEmail, errorLabel: nil , rules: [RequiredRule()])
        //validator.registerField(edtPwd, errorLabel: nil , rules: [RequiredRule(),AlphaNumericRule()])
        validator.styleTransformers(success:{ (validationRule) -> Void in

         }, error:{ (validationError) -> Void in
             print("error")
             validationError.errorLabel?.isHidden = false
             validationError.errorLabel?.text = validationError.errorMessage
         })
         validator.validate(self)
    }
    
    func validationSuccessful() {
        self.validationSuccess()
    }
          
    func validationFailed(_ errors: [(Validatable, ValidationError)]) {
        print("validation error")
    }
       
    func validationSuccess() {
    showLoadingView(vc: self)
       
    ApiManager.login(emailorphone: edtEmail.text!, password: edtPwd.text!){ (isSuccess, data) in
           self.hideLoadingView()
           
            if isSuccess{
                let data = JSON(data as Any)
                print("data =========>",data)
                user?.clearUserInfo()
                UserDefault.setString(key: PARAMS.PASSWORD, value: self.edtPwd.text!)
                
                user = UserModel(data)
                user?.password = self.edtPwd.text!
                user?.saveUserInfo()
                user?.loadUserInfo()
                dump(user, name: "userinfo =========>")
                /*info?.clearInfo()
                info = LoginInfo(self.edtEmail!.text!,pwd:self.edtPwd!.text!, type: UserDefault.getString(key: PARAMS.TYPE) ?? "buyer")
                info?.saveInfo()
                info?.loadInfo()*/
                //dump(info, name: "info===========>")
                self.loginSuccess()
            }
                
            else{
                if data == nil{
                   self.alertDisplay(alertController: self.alertMake("Connection Error!"))
                }
                else{
                   let result_code = data as! Int
                   if(result_code == INVALIDLOGIN){
                       self.alertDisplay(alertController: self.alertMake("Non Exist Email \n or Password Incorrect!"))
                   }
                   else{
                       self.alertDisplay(alertController: self.alertMake("Password Incorrect"))
                   }
                }
            }
        }
    }
    
    func display(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
       
    func loginSuccess() {
        
        if user?.type == PARAMS.SELLER{
            if user?.approve == "1"{
                    self.gotoVC("SellerMainVC")
            }else{
                if user?.profile_completed == "0"{
                    self.gotoStoryBoardVC("BusinessProfileVC", fullscreen: true)
                }else{
                    self.showAlerMessage( message: "Profile submitted successfully. We will contact you via email (ASAP) when approved.")
                }
            }
        }else{
            self.gotoStoryBoardVC("MainVC", fullscreen: true)
        }
    }
    
    @IBAction func gotoSignUp(_ sender: Any) {
        gotoNavPresent1("SignUpVC",fullscreen: true)
    }
    
    @IBAction func gotoForgot(_ sender: Any) {
       self.gotoNavPresent1("ForgotPwdVC", fullscreen: false)
    }
}
