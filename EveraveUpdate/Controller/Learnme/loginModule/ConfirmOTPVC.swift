//
//  ConfirmOTPVC.swift
//  IFHAM
//
//  Created by AngelDev on 5/29/20.
//  Copyright © 2020 AngelDev. All rights reserved.
//

import UIKit
import KAPinField
import FirebaseAuth
import SwiftyJSON

class ConfirmOTPVC: BaseVC1 {
    
    @IBOutlet weak var otpCodeView: KAPinField!
    @IBOutlet weak var butResendCode: UIButton!
    
    @IBOutlet weak var lblYourOTP: UILabel!
    @IBOutlet weak var lblPhoneNum: UILabel!
    @IBOutlet weak var lblIhave: UILabel!
    
    private var countryCode = ""
    private var phoneNum = ""
    var verificationID = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
    }
    
    func setupView() {
        
//        self.title = "Confirm OTP"
//        self.navigationController?.isNavigationBarHidden = false
        self.navigationController?.isNavigationBarHidden = true
        if let countrycode = OTP_countryCode, let phonenumber = OTP_phonenumber{
            countryCode = countrycode
            phoneNum = phonenumber
        }
        
        //lblUserType.text = "(" + userType.prefix(1).uppercased() + userType.dropFirst() + ")"
        lblYourOTP.text = "Enter your OTP"
        lblPhoneNum.text = "Enter the OTP sent to " + countryCode + phoneNum
        lblIhave.text = "Didn't receice OTP? "
        butResendCode.setTitle("Resent Code", for: .normal)
        setStyle()
    }
   
    func setStyle() {
        
        otpCodeView.properties.delegate = self
//        otpCodeView.properties.token = "-"
        otpCodeView.properties.animateFocus = true
        otpCodeView.text = ""
        otpCodeView.keyboardType = .numberPad
        otpCodeView.properties.numberOfCharacters = 6
        otpCodeView.appearance.tokenColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.tokenFocusColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.textColor = UIColor.black
        otpCodeView.appearance.font = .menlo(40)
        otpCodeView.appearance.kerning = 24
        otpCodeView.appearance.backOffset = 5
        otpCodeView.appearance.backColor = UIColor.clear
        otpCodeView.appearance.backBorderWidth = 1
        otpCodeView.appearance.backBorderColor = UIColor.black.withAlphaComponent(0.2)
        otpCodeView.appearance.backCornerRadius = 4
        otpCodeView.appearance.backFocusColor = UIColor.clear
        otpCodeView.appearance.backBorderFocusColor = UIColor.black.withAlphaComponent(0.8)
        otpCodeView.appearance.backActiveColor = UIColor.clear
        otpCodeView.appearance.backBorderActiveColor = UIColor.black
        otpCodeView.appearance.backRounded = false
        otpCodeView.becomeFirstResponder()
    }
   
    func refreshPinField() {

        otpCodeView.text = ""
//        UIPasteboard.general.string = targetCode
        setStyle()
    }
    
    
    @IBAction func didTapResendCode(_ sender: Any) {
        self.dismissKeyboard()
        //self.showLoadingView(vc: self, label: "Requesting...")
        self.showLoadingView(vc: self)
        Auth.auth().settings!.isAppVerificationDisabledForTesting = true
        Auth.auth().languageCode = "en";
        PhoneAuthProvider.provider().verifyPhoneNumber(countryCode + phoneNum, uiDelegate: nil) { (verificationID, error) in
            
            self.hideLoadingView()
            //self.hideLoadingView()
            
            if let error = error {
                let errStr = error.localizedDescription
                let invalidPhoneTypes = ["TOO_LONG", "TOO_SHORT", "Invalid format"]
                
                if errStr == "MISSING_CLIENT_IDENTIFIER" {
                    self.showAlerMessage(message: "SMS quota exceeded")
                } else if invalidPhoneTypes.contains(errStr) {
                    self.showAlerMessage(message: "Enter correct Phone Number")
                    self.showAlerMessage(message: "Enter correct Phone Number")
                } else {
                    self.showAlerMessage(message: "Network issue!")
                }
                
                return
            }
            OTP_verifyID = verificationID!
            //UserDefaults.standard.set(verificationID, forKey: CONSTANT.KEY_AUTHVERIFY_ID)
            // for setting action for remembering id
            self.refreshPinField()
        }
    }
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}


// Mark: - KAPinFieldDelegate
extension ConfirmOTPVC : KAPinFieldDelegate {
    
    func pinField(_ field: KAPinField, didChangeTo string: String, isValid: Bool) {
        if isValid {
            print("Valid input: \(string) ")
        } else {
            print("Invalid input: \(string) ")
            self.otpCodeView.animateFailure()
        }
    }
    
    func pinField(_ field: KAPinField, didFinishWith code: String) {
        
        print("didFinishWith : \(code)")
        if let verificationID = OTP_verifyID{
            self.verificationID = verificationID
        }
        //self.showLoadingView(vc: self, label: "Requesting...")
        self.showLoadingView(vc: self)
        self.dismissKeyboard()
        
        let credential = PhoneAuthProvider.provider().credential(withVerificationID: verificationID , verificationCode: code)
        
        //print("verifycode ==> ",verificationCode)
        Auth.auth().signIn(with: credential) { authData, error in

            if let error = error {
                self.hideLoadingView()
                //self.hideLoadingView()
                //self.showError(error.localizedDescription)
                self.showAlerMessage(message: error.localizedDescription)
                field.animateFailure()
                self.otpCodeView.becomeFirstResponder()
                return
            }
            //self.hideLoadingView()
            self.hideLoadingView()
            field.animateSuccess(with: "👍") {
                print("OK")
                self.showLoadingView(vc: self)
                if let photo = sign_photo, let firstName = sign_firstName,  let lastName = sign_lastName, let userName = sign_userName, let email = sign_email, let phone = sign_phone, let dob = sign_dob, let gender = sign_gender, let ssn = sign_ssn, let password = sign_password, let deviceToken = sign_device_token, let type = sign_type{
                    ApiManager.signup(photo: photo, first_name: firstName, last_name: lastName, user_name: userName, email: email, phone: phone, dob: dob, gender: gender, ssn: ssn, password: password, type: type) { (isSuccess, data) in
                        //self.hideLoadingView()
                        self.hideLoadingView()
                        if isSuccess{
                            let dict = JSON (data as Any)
                            let id = dict["id"].intValue
                            let picture = dict["picture"].stringValue
                            user!.clearUserInfo()
                            user!.id = id
                            user!.first_name = firstName
                            user!.last_name = lastName
                            user!.user_name = userName
                            user!.dob = dob
                            user!.gender = gender
                            user!.ssn = ssn
                            user!.email = email
                            user!.phone = phone
                            user!.password = password
                            user!.picture = picture
                            user!.profile_completed = "0"
                            user!.approve = "0"
                            user!.type = type
                            user!.token = deviceToken
                            user!.saveUserInfo()
                            user!.loadUserInfo()
                            
                            if user!.type == PARAMS.BUYER{
                                self.gotoStoryBoardVC("MainVC", fullscreen: true)
                            }else{
                                //self.navigationController?.popToRootViewController(animated: true)
                                self.gotoStoryBoardVC("BusinessProfileVC", fullscreen: true)
                            }
                        }else{
                            
                            if let data = data{
                                let statue = data  as! Int
                                if statue == 201{
                                    self.showAlerMessage(message: "Email already exist")
                                }else if statue == 202{
                                    self.showAlerMessage(message: "Phone number already exist")
                                }else {
                                    self.showAlerMessage(message: "Network issue!")
                                }
                            }else{
                                self.showAlerMessage(message: "Network issue!")
                            }
                            self.navigationController?.popToRootViewController(animated: true)
                        }
                    }
                }
            }
        }
    }
}
