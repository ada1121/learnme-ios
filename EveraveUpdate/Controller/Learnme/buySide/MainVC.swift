//
//  MainVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/14/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit
import Firebase
import FirebaseDatabase

class MainVC: BaseVC1  {
    
    @IBOutlet weak var txf_search: UITextField!
    @IBOutlet weak var col_mainView: UICollectionView!
    @IBOutlet weak var filterView: UIView!
    
    @IBOutlet weak var imv_badgeIcon: UIImageView!
    @IBOutlet weak var imv_chattingBadge: UIImageView!
    
    @IBOutlet weak var lbl_accptedbadge: UILabel!
    @IBOutlet weak var lbl_chattingBadge: UILabel!
    
    @IBOutlet weak var lbl_notyicon: UIImageView!
    @IBOutlet weak var lbl_msgNum: UILabel!
    
    @IBOutlet weak var lbl_popularservice: UILabel!
    @IBOutlet weak var cons_mainViewtop: NSLayoutConstraint!
    var consSet = false
    
    
    var num: Int = 0
    var visible: Bool = true
    
    let categoryArray =  ["All", "Barber Specialist", "Beautician Specialist", "Nails Specialist", "Eyebrow Specialist", "Makeup Specialist",
    "Tutoring Services", "ASMR", "Mukbang", "Personal Assistant", "Legal Services", "Public Figure", "Yoga Instructor", "Medical Services",
    "Dental Services", "Personal Trainer", "Veternarian", "Pet Groomer", "Pet Trainer", "Nutritionist", "Culinary Assistance",
    "Tour Guide", "Accounting", "Auto Services", "Event Planning", "Dance Instructor", "Video Game Instructor", "Comedy Services",
    "Interior Design", "Security Virtual Patrol", "Shoe Repair Assistance", "DIY Expert", "Music Lessons", "Fashion Designer"]

    var cellHeight = 50
    
    //Variables for search
     var searchActive : Bool = false
     var data :[String] = []
     var data1 :[String] = []
     var data2 :[String] = []
     var data3 :[String] = []
     var data4 :[String] = []
     var filtered:[String] = []
     var filtered1:[String] = []
     var filtered2:[String] = []
     var filtered3:[String] = []
     var filtered4:[String] = []
    
    var ds_sellers = [SellerModel]()
    var ds_filtered = [SellerModel]()
    var msgDatasource = [MessageSwipeModel]()
    var msgDatasource1 = [MessageSwipeModel]()
    var newChatHandle: UInt?
    
    override func viewWillAppear(_ animated: Bool) {
        
        if user!.isValid{
            self.getDataSource()
        }

        self.userlistListner()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if !user!.isValid{
           DispatchQueue.main.asyncAfter(deadline: .now() + 0.5, execute: {
                   self.lbl_popularservice.isHidden = true
                   self.gotoVCModal("SigninRequestVC")
               }
           )
        }
        
        self.setEdtPlaceholder(self.txf_search, placeholderText: "Search", placeColor: .lightGray, padding: .left(40))
        self.imv_badgeIcon.isHidden = true
        self.lbl_accptedbadge.text = ""
        
        txf_search.addPadding(.left(40))
        txf_search.delegate = self
        txf_search.addTarget(self, action: #selector(MainVC.textFieldDidChange(_:)),
        for: UIControl.Event.editingChanged)
        //col_mainView.isHidden = true
    }
    
    func userlistListner()  {
        self.msgDatasource.removeAll()
        let userRoomid = "u" + "\(user?.id ?? 0)"
        newChatHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            guard userlist.id != nil else{
                //self.hideLoadingView()
                return

            }
            
            self.msgDatasource.append(userlist)
            var ids = [String]()
            for one in self.msgDatasource{
                ids.append(one.id!)
            }
            //print(ids.removeDuplicates())
            var num = 0
            self.msgDatasource1.removeAll()
            for  one in ids.removeDuplicates(){
                num += 1
                self.msgDatasource1.append(self.getDataFromID(one))
                if num == ids.removeDuplicates().count{
                    var unRead = 0
                    var num = 0
                    for one in self.msgDatasource1{
                        num += 1
                        if one.unreadMsgNum != "0" && one.unreadMsgNum.toInt() ?? 0 > 0{
                            unRead += 1
                        }
                        if num == self.msgDatasource1.count{
                            if unRead > 0{
                                self.lbl_msgNum.isHidden = false
                                self.lbl_msgNum.text = "\(unRead)"
                                self.lbl_notyicon.isHidden = false
                                self.view.layoutIfNeeded()
                            }else{
                                 self.lbl_msgNum.isHidden = true
                                 self.lbl_msgNum.text = ""
                                 self.lbl_notyicon.isHidden = true
                            }
                        }
                    }
                }
            }
          }
        )
    }
    
    func getDataFromID(_ id: String) -> MessageSwipeModel {
        var returnModel : MessageSwipeModel?
        for one in self.msgDatasource{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? MessageSwipeModel()
    }
    
    func getDataSource() {
        self.showLoadingView(vc: self)
        ApiManager.getSellers { (isSuccess, data, badge) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_sellers.removeAll()
                let str_badge = badge
                if let Badge = str_badge{
                    if Badge == "0"{
                        self.imv_badgeIcon.isHidden = true
                        self.lbl_accptedbadge.isHidden = true
                        self.lbl_accptedbadge.text = ""
                    }else{
                        self.imv_badgeIcon.isHidden = false
                        self.lbl_accptedbadge.isHidden = false
                        self.lbl_accptedbadge.text = Badge
                    }
                }
                
                let dict = JSON(data as Any)
                //dump(dict, name: "dict=====>")
                let seller_info = dict["seller_info"].arrayObject
                if let sellerinfo = seller_info{
                    for one in sellerinfo{
                        let sellerModel = SellerModel()
                        let JsonOne = JSON(one as Any)
                        sellerModel.id = JsonOne[PARAMS.ID].stringValue
                        sellerModel.des = JsonOne[PARAMS.DESCRIPTION].stringValue
                        sellerModel.rating = JsonOne[PARAMS.RATING].floatValue
                        sellerModel.first_name = JsonOne[PARAMS.FIRST_NAME].stringValue
                        sellerModel.last_name = JsonOne[PARAMS.LAST_NAME].stringValue
                        sellerModel.user_name = JsonOne["user_name"].stringValue
                        sellerModel.picture = JsonOne[PARAMS.PICTURE].stringValue
                        sellerModel.state = JsonOne[PARAMS.STATE].stringValue
                        sellerModel.timezone = JsonOne[PARAMS.TIMEZONE].stringValue
                        sellerModel.tools = JsonOne[PARAMS.TOOLS].stringValue
                        sellerModel.languages = JsonOne[PARAMS.LANGUAGES].stringValue
                        sellerModel.count = JsonOne[PARAMS.COUNT].stringValue
                        
                        let portfolioItems = JsonOne["portofolio_info"].arrayObject
                        sellerModel.portfolioList.removeAll()
                        if let portfolioitems = portfolioItems{
                            for one in portfolioitems{
                                let json = JSON(one)
                                sellerModel.portfolioList.append(PortfolioModel(json))
                            }
                        }
                        
                        let availableItems = JsonOne["availabletime_info"].arrayObject
                        if let availableitems = availableItems{
                            for one in availableitems{
                                let json = JSON(one)
                                sellerModel.availableTimeModel = AvailableTimeModel(json)
                            }
                        }
                        
                        let serviceItems = JsonOne["service_info"].arrayObject
                        sellerModel.serviceModelList.removeAll()
                        if let serviceitems =  serviceItems{
                            for one in serviceitems{
                                let json = JSON(one)
                                let servicemodel = ServiceModel()
                                
                                if json["name"].stringValue != ""{
                                    servicemodel.name = json[PARAMS.NAME].stringValue
                                }
                                if json["id"].stringValue != ""{
                                    servicemodel.id = json[PARAMS.ID].stringValue
                                }
                                if json["t_15"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "15"
                                    timepricemodel.price = json["t_15"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_15 = json["t_15"].stringValue
                                }
                                
                                if json["t_30"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "30"
                                    timepricemodel.price = json["t_30"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_30 = json["t_30"].stringValue
                                }
                                
                                if json["t_45"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "45"
                                    timepricemodel.price = json["t_45"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_45 = json["t_45"].stringValue
                                }
                                
                                if json["t_60"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "60"
                                    timepricemodel.price = json["t_60"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_60 = json["t_60"].stringValue
                                }
                                sellerModel.serviceModelList.append(servicemodel)
                            }
                        }
                        self.ds_sellers.append(sellerModel)
                        self.ds_filtered.append(sellerModel)
                        self.data.append(sellerModel.user_name!)
                        self.data1.append(sellerModel.first_name!)
                        self.data2.append(sellerModel.last_name!)
                        //self.data3.append((sellerModel.serviceModelList.first?.name!)!)
                        //self.data4.append(miniJSON[PARAMS.MINIDESCRIPTION].stringValue)
                    }
                }
                self.consSet = false
                self.col_mainView.reloadData()
            }
        }
    }
    
    @IBAction func gotoFilter(_ sender: Any) {
        visible = !visible
        displayFilter()
    }
    
    func displayFilter() {
        self.filterView.isHidden = visible
    }
    
    func getSellerModelFromServiceName(_ serviceName: String) -> [SellerModel] {
        var ds_filteredfromServiceName = [SellerModel]()
        var num = 0
        for one in self.ds_sellers{
            num += 1
            var matched = false
            for two in one.serviceModelList{
                if serviceName == two.name{
                    ds_filteredfromServiceName.append(one)
                    matched = true
                }
                if matched{
                    break
                }
            }
            if num == self.ds_sellers.count{
                break
            }
        }
        return ds_filteredfromServiceName
    }
   
    
    @IBAction func gotoProfilePage(_ sender: Any) {
        if user!.isValid{
            rootcontrollerName = rootcontrollerNames.MainVC.rawValue
            self.gotoVC("EditProfileVC")
        }else{
            self.gotoVCModal("SigninRequestVC")
        }
        
    }
    @IBAction func gotoMessagePage(_ sender: Any) {
        if user!.isValid{
             self.gotoVC("MessageViewVC")
        }else{
            self.gotoVCModal("SigninRequestVC")
        }
        
    }
    @IBAction func gotoBookingPage(_ sender: Any) {
        if user!.isValid{
             self.self.gotoStoryBoardVC("BookingVC", fullscreen: true)
        }else{
            self.gotoVCModal("SigninRequestVC")
        }
    }
    @IBAction func gotoHomePage(_ sender: Any) {
        if user!.isValid{
             return
        }else{
            self.gotoVCModal("SigninRequestVC")
        }
        
    }
    @IBAction func gotoSettingpage(_ sender: Any) {
        if user!.isValid{
             self.gotoVC("SettingsVC")
        }else{
            self.gotoVCModal("SigninRequestVC")
        }
    }
    @IBAction func gotoHelpPage(_ sender: Any) {
        self.gotoVC("HelpVC")
    }
}

extension MainVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        //if searchActive{
            return self.ds_filtered.count
        //}else{
            //return self.ds_sellers.count
        //}
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "mainViewCell", for: indexPath) as! mainViewCell
       // if searchActive{
            cell.entity = self.ds_filtered[indexPath.row]
        //}else{
             //cell.entity = self.ds_sellers[indexPath.row]
        //}
       
        //cell.delegate = self
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        sellerModel = nil // init for next setting
        //if searchActive{
            sellerModel = ds_filtered[indexPath.row]
        //}else{
            //sellerModel = ds_sellers[indexPath.row]
        //}
        self.gotoStoryBoardVC("SellerDetailVC", fullscreen: true)
    }
}

extension MainVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let w = collectionView.frame.size.width
            let h: CGFloat = 85
            return CGSize(width: w, height: h)
        }
}

extension MainVC : UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categoryArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: categoryCell.self)) as! categoryCell
        cell.sampleMeal.text = categoryArray[indexPath.row]
        cell.imv_tick.isHidden = false
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.filterView.isHidden = true
        self.visible = self.filterView.isHidden
     
        self.showLoadingView(vc: self)
        
           //num = indexPath.row == 0 ? 9 : indexPath.row % 10
        var categoryName = ""
        if indexPath.row == 0{
            categoryName = "all"
        }else{
             //self.ds_filtered = self.getSellerModelFromServiceName(categoryArray[indexPath.row])
            categoryName = categoryArray[indexPath.row]
        }
        ApiManager.getSellersCategory(categoryName) { (isSuccess, data, badge) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_sellers.removeAll()
                self.ds_filtered.removeAll()
                let str_badge = badge
                if let Badge = str_badge{
                    if Badge == "0"{
                        self.imv_badgeIcon.isHidden = true
                        self.lbl_accptedbadge.isHidden = true
                        self.lbl_accptedbadge.text = ""
                    }else{
                        self.imv_badgeIcon.isHidden = false
                        self.lbl_accptedbadge.isHidden = false
                        self.lbl_accptedbadge.text = Badge
                    }
                }
                
                let dict = JSON(data as Any)
                //dump(dict, name: "dict=====>")
                let seller_info = dict["seller_info"].arrayObject
                if let sellerinfo = seller_info{
                    for one in sellerinfo{
                        let sellerModel = SellerModel()
                        let JsonOne = JSON(one as Any)
                        sellerModel.id = JsonOne[PARAMS.ID].stringValue
                        sellerModel.des = JsonOne[PARAMS.DESCRIPTION].stringValue
                        sellerModel.rating = JsonOne[PARAMS.RATING].floatValue
                        sellerModel.first_name = JsonOne[PARAMS.FIRST_NAME].stringValue
                        sellerModel.last_name = JsonOne[PARAMS.LAST_NAME].stringValue
                        sellerModel.user_name = JsonOne["user_name"].stringValue
                        sellerModel.picture = JsonOne[PARAMS.PICTURE].stringValue
                        sellerModel.state = JsonOne[PARAMS.STATE].stringValue
                        sellerModel.timezone = JsonOne[PARAMS.TIMEZONE].stringValue
                        sellerModel.tools = JsonOne[PARAMS.TOOLS].stringValue
                        sellerModel.languages = JsonOne[PARAMS.LANGUAGES].stringValue
                        
                        let portfolioItems = JsonOne["portofolio_info"].arrayObject
                        sellerModel.portfolioList.removeAll()
                        if let portfolioitems = portfolioItems{
                            for one in portfolioitems{
                                let json = JSON(one)
                                sellerModel.portfolioList.append(PortfolioModel(json))
                            }
                        }
                        
                        let availableItems = JsonOne["availabletime_info"].arrayObject
                        if let availableitems = availableItems{
                            for one in availableitems{
                                let json = JSON(one)
                                sellerModel.availableTimeModel = AvailableTimeModel(json)
                            }
                        }
                        
                        let serviceItems = JsonOne["service_info"].arrayObject
                        sellerModel.serviceModelList.removeAll()
                        if let serviceitems =  serviceItems{
                            for one in serviceitems{
                                let json = JSON(one)
                                let servicemodel = ServiceModel()
                                
                                if json["name"].stringValue != ""{
                                    servicemodel.name = json[PARAMS.NAME].stringValue
                                }
                                if json["id"].stringValue != ""{
                                    servicemodel.id = json[PARAMS.ID].stringValue
                                }
                                if json["t_15"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "15"
                                    timepricemodel.price = json["t_15"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_15 = json["t_15"].stringValue
                                }
                                
                                if json["t_30"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "30"
                                    timepricemodel.price = json["t_30"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_30 = json["t_30"].stringValue
                                }
                                
                                if json["t_45"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "45"
                                    timepricemodel.price = json["t_45"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_45 = json["t_45"].stringValue
                                }
                                
                                if json["t_60"].stringValue != ""{
                                    let timepricemodel = TimePriceModel()
                                    timepricemodel.time = "60"
                                    timepricemodel.price = json["t_60"].stringValue
                                    servicemodel.timePriceModelList.append(timepricemodel)
                                    servicemodel.t_60 = json["t_60"].stringValue
                                }
                                sellerModel.serviceModelList.append(servicemodel)
                            }
                        }
                        self.ds_sellers.append(sellerModel)
                        self.ds_filtered.append(sellerModel)
                        self.data.append(sellerModel.user_name!)
                        self.data1.append(sellerModel.first_name!)
                        self.data2.append(sellerModel.last_name!)
                        //self.data3.append((sellerModel.serviceModelList.first?.name!)!)
                        //self.data4.append(miniJSON[PARAMS.MINIDESCRIPTION].stringValue)
                    }
                }
                
                if !self.consSet{
                    self.cons_mainViewtop.constant -= 40
                    self.lbl_popularservice.isHidden = true
                }
                self.consSet = true
                self.col_mainView.reloadData()
            }
        }
    }
}

class categoryCell: UITableViewCell {
    @IBOutlet weak var sampleMeal: UILabel!
    @IBOutlet weak var imv_tick: UIImageView!
}

extension MainVC: UITextFieldDelegate {
    @objc func textFieldDidChange(_ textField: UITextField) {
        
        let searchText  = textField.text
        if searchText == ""{
            self.col_mainView.isHidden = false
            self.ds_filtered = ds_sellers
            self.col_mainView.reloadData()
        }else{
            filtered = data.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
                /*print("range =======",range)
                print("rangeLocation =======",range.location)
                print("Nsfound =======",range.location != NSNotFound)*/
                return range.location != NSNotFound
            })
            
            filtered1 = data1.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
                /*print("range =======",range)
                print("rangeLocation =======",range.location)
                print("Nsfound =======",range.location != NSNotFound)*/
                return range.location != NSNotFound
            })
            
            filtered2 = data2.filter({ (text) -> Bool in
                let tmp: NSString = text as NSString
                let range = tmp.range(of: searchText!, options: NSString.CompareOptions.caseInsensitive)
                /*print("range =======",range)
                print("rangeLocation =======",range.location)
                print("Nsfound =======",range.location != NSNotFound)*/
                return range.location != NSNotFound
            })
            
            
            if(filtered.count == 0 && filtered1.count == 0 && filtered2.count == 0){
                searchActive = false;
            } else {
                searchActive = true;
            }
            if searchActive{
                col_mainView.isHidden = false
                self.ds_filtered.removeAll()
                
                let ids = self.getDataSourceidFromFilterd(filtered: filtered, type: PARAMS.USERNAME)
                    + self.getDataSourceidFromFilterd(filtered: filtered1, type: PARAMS.FIRST_NAME)
                    + self.getDataSourceidFromFilterd(filtered: filtered2, type: PARAMS.LAST_NAME)
                
                for one in ids.removeDuplicates(){
                    self.ds_filtered.append(self.getDataFromID(one))
                    self.col_mainView.reloadData()
                }
                
            }
            else{
                self.col_mainView.isHidden = true
            }
        }
    }
    
    func getDataSourceidFromFilterd(filtered: [String],type: String) -> [String] {
        var dataSourceid = [String]()
        var dataSourceid1 = [String]()
        var dataSourceid2 = [String]()
        
        for one in self.ds_sellers{
            for two in filtered{
                if type == PARAMS.USERNAME{
                    if one.user_name == two{
                        dataSourceid.append(one.id!)
                    }
                }
                
                else if type == PARAMS.FIRST_NAME{
                    if one.first_name == two{
                        dataSourceid1.append(one.id!)
                    }
                }
                
                else if type == PARAMS.LAST_NAME{
                    if one.last_name == two{
                        dataSourceid2.append(one.id!)
                    }
                }
            }
        }
        
        dataSourceid += dataSourceid1
        dataSourceid += dataSourceid2
        
        print(dataSourceid.removeDuplicates())
        return dataSourceid.removeDuplicates()
    }
    
    func getDataFromID(_ id: String) -> SellerModel {
        var returnModel : SellerModel?
        for one in self.ds_sellers{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? SellerModel()
    }
}

extension Array where Element:Equatable {
    func removeDuplicates() -> [Element] {
        var result = [Element]()

        for value in self {
            if result.contains(value) == false {
                result.append(value)
            }
        }
        return result
    }
}

