//
//  BookingVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit
import Stripe

class BookingVC: BaseVC1,STPPaymentCardTextFieldDelegate  {
   
    @IBOutlet weak var col_mainView: UICollectionView!
    @IBOutlet weak var uiv_stripeBack: UIView!
    @IBOutlet weak var uiv_bankDetail: UIView!
    @IBOutlet weak var cus_stripeCheckout: STPPaymentCardTextField! { didSet {
        cus_stripeCheckout.postalCodeEntryEnabled = false
        }}
    @IBOutlet weak var edt_email: UITextField!
    var selectedBookingindex = 0
    var paymentRequest = false
    
    
    var  ds_booking = [BookingModel]()
    var dicPayData = ["",""]
    
    override func viewWillAppear(_ animated: Bool) {
        self.getDataSource()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadLayout()
    }
    
    private func getTokenandProcess(){
        
        let cardParams = STPCardParams()
        cardParams.number = cus_stripeCheckout?.cardNumber
        cardParams.expMonth = (cus_stripeCheckout?.expirationMonth)!
        cardParams.expYear = (cus_stripeCheckout?.expirationYear)!
        cardParams.cvc = cus_stripeCheckout?.cvc
        STPAPIClient.shared().createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
            guard let token = token, error == nil else {
                return
            }
            let bookingmodel = self.ds_booking[self.selectedBookingindex]
            let email = self.edt_email.text
            let amount = bookingmodel.service_cost!.toInt()! * 100
            self.showLoadingView(vc: self)
            ApiManager.paymentProcess(stripe_token: token.tokenId, booking_id: bookingmodel.id!, amount: "\(amount)", email: email!, seller_id: bookingmodel.seller_id!, seller_name: bookingmodel.seller_name!, service_name: bookingmodel.service_name!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.showAlerMessage(message: "Accepted!")
                    self.getDataSource()
                    self.paymentRequest = false
                    self.uiv_bankDetail.isHidden = true
                    self.uiv_stripeBack.isHidden = true
                    self.cus_stripeCheckout.clear()
                }else{
                    self.showAlerMessage(message: "Error!")
                    self.paymentRequest = false
                    self.uiv_bankDetail.isHidden = true
                    self.uiv_stripeBack.isHidden = true
                    self.cus_stripeCheckout.clear()
                }
            }
        }
    }
    
    func loadLayout()  {
        self.uiv_bankDetail.isHidden = true
        self.uiv_stripeBack.isHidden = true
        self.setEdtPlaceholder(edt_email, placeholderText: "Email", placeColor: .lightGray, padding: .left(40))
    }
   
    @IBAction func payBtnClicked(_ sender: Any) {
        let email = self.edt_email.text
        if email == ""{
            self.showAlerMessage(message: "Enter your email")
            return
        }
        
        if !email!.isValidEmail(){
            self.showAlerMessage(message: "Invalid email")
            return
        }
        
        if self.cus_stripeCheckout.isValid{
            if paymentRequest{
                return
            }
            paymentRequest = true
            self.getTokenandProcess()
        }else {
            self.showAlerMessage(message: "Please input your valid payment information")
        }
    }
    
    @IBAction func closeBtnClicked(_ sender: Any) {
        self.uiv_bankDetail.isHidden = true
        self.uiv_stripeBack.isHidden = true
        /*self.cus_stripeCheckout.cardNumber = ""
        self.cus_stripeCheckout.expirationMonth = ""
        self.cus_stripeCheckout.expirationYear = ""
        self.cus_stripeCheckout.cvc = ""*/
        self.cus_stripeCheckout.clear()
    }
    
    @IBAction func appointmentBtnClicked(_ sender: Any) {
         return
    }
     
     @IBAction func historyBtnClicked(_ sender: Any) {
        self.self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
     }
    
    @IBAction func gotoBack(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    
    func getDataSource() {
        self.showLoadingView(vc: self)
        ApiManager.getBooking { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_booking.removeAll()
                let json = JSON(data as Any)
                let booking_info = json["booking_info"].arrayObject
                
                if let bookingInfo = booking_info{
                    var num = 0
                    for one in bookingInfo{
                        num += 1
                        let jsonONE = JSON(one as Any)
                        if jsonONE[PARAMS.STATE] == "0" || jsonONE[PARAMS.STATE] == "1" || jsonONE[PARAMS.STATE] == "2"{
                            self.ds_booking.append(BookingModel(jsonONE))
                        }
                        if num == bookingInfo.count{
                            self.col_mainView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

extension BookingVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ds_booking.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell", for: indexPath) as! BookingCell
    
        cell.delegate = self
    
        cell.entity = self.ds_booking[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedBooking = nil
        selectedBooking = ds_booking[indexPath.row]
        self.gotoVC("BookingDetailVC")
    }
}

extension BookingVC : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            let w = collectionView.frame.size.width
            let h: CGFloat = 90
            return CGSize(width: w, height: h)
        }
}

extension BookingVC : SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        
        guard orientation == .right else {
           return nil
        }
        
        let payAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            //self.progShowInfo(true, msg: "payed")
            self.uiv_bankDetail.isHidden = false
            self.uiv_stripeBack.isHidden = false
            self.selectedBookingindex = indexPath.row
        }

        let cancelAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            if self.ds_booking[indexPath.row].state != "2"{//
                let bookingmodel = self.ds_booking[indexPath.row]
                self.showLoadingView(vc: self)
                ApiManager.removeBooking(id: bookingmodel.id!) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        //self.showAlerMessage(message: "Canceled")
                        //DispatchQueue.main.async { self.getDataSource()}
                        self.gotoStoryBoardVC("BookingVC", fullscreen: true)
                    }else{
                        //self.showAlerMessage(message: "Canceled")
                        self.showAlerMessage(message: "Network issue")
                    }
                }
            }else{
                // TODO: goto refund vc
                refund_state = "5"
                refundBooking = self.ds_booking[indexPath.row]
                self.gotoVC("RefundVC")
            }
        }
               // customize swipe action
        //acceptAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        //rejectAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

               // customize the action appearance
        payAction.image = UIImage(named: "pay_set")
        cancelAction.image = UIImage(named: "cancel_set")

        payAction.backgroundColor = UIColor.init(named: "colorSwipeCell")
        cancelAction.backgroundColor = UIColor.init(named: "colorSwipeCell")

        //acceptAction.title = NSLocalizedString("Conference", comment: "")
        //rejectAction.title = NSLocalizedString("Download", comment: "")
        //videoAction.textColor = UIColor.lightGray
        //downLoadAction.textColor = UIColor.lightGray

        /*if self.ds_booking[indexPath.row].state == "Pending"{
            return [rejectAction,acceptAction]
        }
        
        return [rejectAction]*/
        switch self.ds_booking[indexPath.row].state {
        case "0":
            return [cancelAction]
        case "1":
            return [cancelAction, payAction]
        case "2":
            return [cancelAction]
        
        default:
            return [cancelAction]
        }
    }
}

