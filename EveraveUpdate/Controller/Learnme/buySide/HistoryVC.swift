//
//  HistoryVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit

class HistoryVC: BaseVC1  {
    
   
    @IBOutlet weak var col_mainView: UICollectionView!
    var  ds_history = [BookingModel]()
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.getDataSource()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func appointmentBtnClicked(_ sender: Any) {
         self.self.gotoStoryBoardVC("BookingVC", fullscreen: true)
     }
     
     @IBAction func historyBtnClicked(_ sender: Any) {
        return
     }
    
    @IBAction func gotoBack(_ sender: Any) {
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    func getDataSource() {
        self.showLoadingView(vc: self)
        ApiManager.getBooking { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                let json = JSON(data as Any)
                let booking_info = json["booking_info"].arrayObject
                
                if let bookingInfo = booking_info{
                    var num = 0
                    for one in bookingInfo{
                        num += 1
                        let jsonONE = JSON(one as Any)
                        if jsonONE[PARAMS.STATE] == "3" || jsonONE[PARAMS.STATE] == "4" || jsonONE[PARAMS.STATE] == "5"{
                            self.ds_history.append(BookingModel(jsonONE))
                        }
                        if num == bookingInfo.count{
                            self.col_mainView.reloadData()
                        }
                    }
                }
            }
        }
    }
}

extension HistoryVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.ds_history.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookingCell", for: indexPath) as! BookingCell
    
        //cell.delegate = self
    
        cell.entity = self.ds_history[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        historyBooking = nil
        historyBooking = ds_history[indexPath.row]
        self.gotoVC("HistoryDetailVC")
    }
}

extension HistoryVC : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width
        let h: CGFloat = 90
        return CGSize(width: w, height: h)
    }
}


