//
//  AppointmentVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/13/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import UIKit
import ExpyTableView
import ExpandableCell
import SwiftyJSON

class AppointmentVC: BaseVC1,UITextViewDelegate {
    
    @IBOutlet weak var txv_feedback: UITextView!
    @IBOutlet weak var lbl_categoryName: UILabel!
    @IBOutlet weak var lbl_timePrice: UILabel!
    @IBOutlet weak var calendarViewV: CalendarView!
    @IBOutlet weak var col_timesShow: UICollectionView!
    @IBOutlet weak var cons_col_timesShowHeight: NSLayoutConstraint!
    var heredate: Date? = nil
    var booktime: String? = nil
    var timesList = [timeModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.txv_feedback.delegate = self
        loadLaoyout()
        setCalendarView()
    }
    
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
            
            super.viewDidAppear(animated)
            
            let today = Date()
            
            var tomorrowComponents = DateComponents()
            tomorrowComponents.day = 0 // selected date setting
            
            
    //        let tomorrow = self.calendarViewV.calendar.date(byAdding: tomorrowComponents, to: today)!
            //self.calendarViewV.selectDate(nil)

            #if KDCALENDAR_EVENT_MANAGER_ENABLED
            self.calendarViewV.loadEvents() { error in
                if error != nil {
                    let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                    let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                }
            }
            #endif
            
            if today.dayofTheWeek != ""{
                print("++++++++++++++,\(today.dayofTheWeek)")
            }
            
            self.calendarViewV.setDisplayDate(today)
        
        }
    
    func loadLaoyout() {
        if let serviceName = sellerSeriveName{
            self.lbl_categoryName.text = serviceName
        }
        if let servicetime = sellerServieTime, let serviceprice = sellerServiePrice{
            self.lbl_timePrice.text = servicetime + "min"  + " " + ":" + " " + "$" + serviceprice
        }
        self.txv_feedback.text = "Please write your note ..."
        self.txv_feedback.textColor = UIColor.lightGray
        cons_col_timesShowHeight.constant = 20
        print("timezone =======> ",localTimeZoneIdentifier)
    }
    
    func setCalendarView()  {
        let style = CalendarView.Style()
        style.cellShape                = .square
        style.cellColorDefault         = UIColor.clear
        style.cellColorToday           = UIColor.systemRed
        style.cellSelectedBorderColor  = UIColor.black
        style.cellEventColor           = UIColor.clear
        style.headerTextColor          = UIColor.darkGray
        style.cellTextColorDefault     = UIColor.black
        //style.cellTextColorToday       = UIColor.white
        style.cellTextColorToday       = .lightGray
        style.cellTextColorWeekend     = UIColor.black
        //style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
        style.cellColorOutOfRange      = .lightGray
        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday
        style.locale                   = Locale(identifier: localTimeZoneIdentifier)
        print("currentLocale==>",Locale.current)
        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 24.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        calendarViewV.style = style
        calendarViewV.dataSource = self
        calendarViewV.delegate = self
        calendarViewV.direction = .horizontal
        calendarViewV.multipleSelectionEnable = false
        calendarViewV.marksWeekends = true
        calendarViewV.backgroundColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txv_feedback.textColor == UIColor.lightGray {
            txv_feedback.text = nil
            txv_feedback.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txv_feedback.text.isEmpty {
            txv_feedback.text = "Please write your note ..."
            txv_feedback.textColor = UIColor.lightGray
        }
    }
    
    func setColTimeShow(_ weekString: String?)  {
        if weekString!.contains(","){
            let left = String(weekString!.split(separator: ",").first!).trim()
            let right = String(weekString!.split(separator: ",").last!).trim()
            let leftstartTime: String = String(left.split(separator: "~").first!).trim()
            var leftlastTime: String = String(left.split(separator: "~").last!).trim()
            
            if leftlastTime == "12 AM"{
                leftlastTime = "11:59 PM"
            }
            
            let nowyearmondate = getonlyDateFromTimeStamp(Int64(NSDate().timeIntervalSince1970 * 1000))
            
            var leftstartTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + leftstartTime)
            
            var leftlastTimeStamp : Int = 0
            
            if leftlastTime == "11:59 PM"{
                leftlastTimeStamp = getTimeStampValueFromDateandMinString(nowyearmondate + " " + leftlastTime)
            }else{
                leftlastTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + leftlastTime)
            }
            
            self.timesList.removeAll()
            while leftstartTimeStamp < leftlastTimeStamp {
                
                let hourandmin = getHourandMinFromTimeStamp(Int64(leftstartTimeStamp))
                self.timesList.append(timeModel(hourandmin))
                leftstartTimeStamp += 1800000
            }
            
            let rightstartTime: String = String(right.split(separator: "~").first!).trim()
            var rightlastTime: String = String(right.split(separator: "~").last!).trim()
            
            if rightlastTime == "12 AM"{
                rightlastTime = "11:59 PM"
            }
            
            var rightstartTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + rightstartTime)
            
            var rightlastTimeStamp : Int = 0
            
            if rightlastTime == "11:59 PM"{
                rightlastTimeStamp = getTimeStampValueFromDateandMinString(nowyearmondate + " " + rightlastTime)
            }else{
                rightlastTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + rightlastTime)
            }
            
            while rightstartTimeStamp < rightlastTimeStamp {
                let hourandmin = getHourandMinFromTimeStamp(Int64(rightstartTimeStamp))
                self.timesList.append(timeModel(hourandmin))
                rightstartTimeStamp += 1800000
            }
            self.cons_col_timesShowHeight.constant = 40
            self.col_timesShow.reloadData()
        }else{
            let leftstartTime: String = String(weekString!.split(separator: "~").first!).trim()
            var leftlastTime: String = String(weekString!.split(separator: "~").last!).trim()
            
            if leftlastTime == "12 AM"{
                leftlastTime = "11:59 PM"
            }
            
            let nowyearmondate = getonlyDateFromTimeStamp(Int64(NSDate().timeIntervalSince1970 * 1000))
            
            var leftstartTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + leftstartTime)
            
            var leftlastTimeStamp : Int = 0
            
            if leftlastTime == "11:59 PM"{
                leftlastTimeStamp = getTimeStampValueFromDateandMinString(nowyearmondate + " " + leftlastTime)
            }else{
                leftlastTimeStamp = getTimeStampValueFromDateString(nowyearmondate + " " + leftlastTime)
            }
            
            self.timesList.removeAll()
            while leftstartTimeStamp < leftlastTimeStamp {
                
                let hourandmin = getHourandMinFromTimeStamp(Int64(leftstartTimeStamp))
                self.timesList.append(timeModel(hourandmin))
                leftstartTimeStamp += 1800000
            }
            self.cons_col_timesShowHeight.constant = 40
            self.col_timesShow.reloadData()
        }
    }
    @IBAction func goBooking(_ sender: Any) {
        if heredate == nil{
            self.showAlerMessage(message: "Please select your Appointment Date")
            return
        }
        if booktime == nil || booktime == ""{
            self.showAlerMessage(message: "Please select your Appointment Time")
            return
        }else{
            let dateString : String = "\(heredate!)"
            let components = dateString.split(withMaxLength: 10)
            let selecteddate = String(components[0])
            //print(selecteddate)//2020-06-30
            let timestamp = getTimeStampDateandMinString(selecteddate + " " + booktime!)
            //print(timestamp)
            if let sellermodel = sellerModel{
                var category = ""
                var time = ""
                var price = ""
                if let serviceName = sellerSeriveName{
                    category = serviceName
                }
                if let serviceName = sellerSeriveName{
                    category = serviceName
                }
                if let servicetime = sellerServieTime, let serviceprice = sellerServiePrice{
                    time = servicetime
                    price = serviceprice
                }
                let des = self.txv_feedback.text
                self.showLoadingView(vc: self)
                
                ApiManager.booking(seller_id: sellermodel.id!, seller_name: sellermodel.user_name! , seller_picture: sellermodel.picture!, service_name: category, service_time: time, service_cost: price, service_date: timestamp, des: des ?? "") { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        self.booktime = nil
                        self.col_timesShow.reloadData()
                        let status = data as! Int
                        if status == 200{
                            let alertController = UIAlertController(title: nil, message:"Request Submitted.\nPlease check your Appointment Icon for Confirmation Status.", preferredStyle: .alert)
                            let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                                self.gotoStoryBoardVC("MainVC", fullscreen: true)
                            }
                            alertController.addAction(action1)
                            self.present(alertController, animated: true, completion: nil)
                            
                        }else{
                            self.showAlerMessage(message: "Appointment Time Has Been Taken.")
                        }
                    }else{
                    }
                }
            }
        }
        //self.showAlerMessage(message: "Request Submitted!")
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        //self.gotoStoryBoardVC("AppointmentVC", fullscreen: true)
        self.gotoStoryBoardVC("SellerDetailVC", fullscreen: true)
    }
}

extension AppointmentVC: CalendarViewDataSource {
    // MARK:here we can add start limited day
    func startDate() -> Date {
        var dateComponents = DateComponents()
        //dateComponents.month = -1
        let seconds = TimeZone.current.secondsFromGMT()
        print("seconds =====> ",seconds)
        let hour = seconds / 3600
        dateComponents.hour = hour
        let today = Date()
        let threeMonthsAgo = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)! // here can set the start date
        return threeMonthsAgo
    }
    // MARK: here we can add start limited day
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = 1200
        let today = Date()
        let twoYearsFromNow = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)!
        return twoYearsFromNow
    }
}

extension AppointmentVC: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        heredate = date
        //print("Did Select: \(date) with \(events.count) events")
        /*for event in events {
            //print("\t\"\(event.title)\" - Starting at:\(event.startDate)")
        }*/
        //MARK: add time event
        let todayweek = date.dayNumberOfWeek()
        
        /*if let availabletimemodel = sellerAvaillableTimemodel{
            if availabletimemodel.sun != "Closed"{
                if todayweek == 1{
                    setColTimeShow(availabletimemodel.sun)
                }
            }
            
            if availabletimemodel.mon != "Closed"{
                if todayweek == 2{
                    setColTimeShow(availabletimemodel.mon)
                }
            }
            
            if availabletimemodel.tue != "Closed"{
                if todayweek == 3{
                    setColTimeShow(availabletimemodel.tue)
                }
            }
            
            if availabletimemodel.wed != "Closed"{
                if todayweek == 4{
                    setColTimeShow(availabletimemodel.wed)
                }
            }
            
            if availabletimemodel.thr != "Closed"{
                if todayweek == 5{
                    setColTimeShow(availabletimemodel.thr)
                }
            }
            
            if availabletimemodel.fri != "Closed"{
                if todayweek == 6{
                    setColTimeShow(availabletimemodel.fri)
                }
            }
            
            if availabletimemodel.sat != "Closed"{
                if todayweek == 7{
                    setColTimeShow(availabletimemodel.sat)
                }
            }
        }*/
        if let availabletimemodel = sellerAvaillableTimemodel{
            
            
            if availabletimemodel.mon != "Closed"{
                if todayweek == 1{
                    setColTimeShow(availabletimemodel.mon)
                }
            }
            
            if availabletimemodel.tue != "Closed"{
                if todayweek == 2{
                    setColTimeShow(availabletimemodel.tue)
                }
            }
            
            if availabletimemodel.wed != "Closed"{
                if todayweek == 3{
                    setColTimeShow(availabletimemodel.wed)
                }
            }
            
            if availabletimemodel.thr != "Closed"{
                if todayweek == 4{
                    setColTimeShow(availabletimemodel.thr)
                }
            }
            
            if availabletimemodel.fri != "Closed"{
                if todayweek == 5{
                    setColTimeShow(availabletimemodel.fri)
                }
            }
            
            if availabletimemodel.sat != "Closed"{
                if todayweek == 6{
                    setColTimeShow(availabletimemodel.sat)
                }
            }
            if availabletimemodel.sun != "Closed"{
                if todayweek == 7{
                    setColTimeShow(availabletimemodel.sun)
                }
            }
        }
    }

    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        print(self.calendarViewV.selectedDates)
        //self.datePicker.setDate(date, animated: true)
    }
}

//MARK: UICollectionViewDataSource
extension AppointmentVC: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return timesList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "timeCell", for: indexPath) as! timeCell
        cell.entity = timesList[indexPath.row]
        return cell
    }
}

extension AppointmentVC: UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        booktime = timesList[indexPath.row].str_time
    }
}

extension AppointmentVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let w = collectionView.frame.size.width / 4
        let h = w / 2
        return CGSize(width: w, height: h)
    }
}

