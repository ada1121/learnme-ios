//
//  EditProfileVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.

import Foundation
import UIKit
import SwiftValidator
import IQKeyboardManagerSwift
import SwiftyJSON
import SwiftyUserDefaults
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import SKCountryPicker
import Photos
import DKImagePickerController
import DKCamera

class EditProfileVC : BaseVC1, UITextFieldDelegate,UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cons_contryName: NSLayoutConstraint!

    @IBOutlet weak var signBtnView: UIView!
    @IBOutlet weak var signCaption: UILabel!
    
    @IBOutlet weak var edtfirstName: UITextField!
    @IBOutlet weak var edtlastName: UITextField!
    @IBOutlet weak var edtuserName: UITextField!
    @IBOutlet weak var edtEmail: UITextField!
    @IBOutlet weak var edtPhone: UITextField!
    @IBOutlet weak var edtDob: UITextField!

    @IBOutlet weak var imv_avatar: UIImageView!
    
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    var firstName = ""
    var lastName = ""
    var usertName = ""
    var email = ""
    var phone = ""
    var dob = ""
    var ssn = ""
    
    override func viewWillAppear(_ animated: Bool) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
        loadLayout()
        imv_avatar.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
    }
    
    func loadLayout(){
        countryCode.text = "+1"
        countryName.text = "United States"
        signCaption.text = "UPDATE"
        
        let url = URL(string: user!.picture ?? "")
        self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
        self.edtfirstName.text = user?.first_name
        self.edtlastName.text = user?.last_name
        self.edtuserName.text = user?.user_name
        self.edtEmail.text = user?.email
        self.edtPhone.text = user?.phone
        self.edtDob.text = user?.dob
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 1
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
         self.assets.removeAll()
        self.assets = assets
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.gotoUploadProfile(image)
                })
            }
        }
        else {}
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_avatar.image = image
        }
    }
    
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode.text = country.dialingCode!
            self.countryName.text = country.countryName
            self.cons_contryName.constant = CGFloat(country.countryName.count * 8)
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        //self.dismiss(animated: false, completion: nil)
        if let rootconroller = rootcontrollerName{
            if rootconroller == rootcontrollerNames.MainVC.rawValue{
                self.gotoStoryBoardVC("MainVC", fullscreen: true)
            }else{
                self.gotoVC("SettingsVC")
            }
        }
    }
    
    @IBAction func nextBtnClicked(_ sender: Any) {
        firstName = self.edtfirstName.text ?? ""
        lastName = self.edtlastName.text ?? ""
        usertName = self.edtuserName.text ?? ""
        email = self.edtEmail.text ?? ""
        phone = self.edtPhone.text ?? ""
        dob = self.edtDob.text ?? ""
        
        if firstName.isEmpty{
            self.progShowInfo(true, msg: "Enter your First name.")
            return
        }
        if lastName.isEmpty{
            self.progShowInfo(true, msg: "Enter your Family name.")
            return
        }
        if usertName.isEmpty{
            self.progShowInfo(true, msg: "Enter your User name.")
            return
        }
        if email.isEmpty{
            self.progShowInfo(true, msg: "Enter your email.")
            return
        }
        if phone.isEmpty{
            self.progShowInfo(true, msg: "Enter your phone number")
            return
        }
        if dob.isEmpty{
            self.progShowInfo(true, msg: "Enter your DOB")
            return
        }
        
        if !isValidEmail(testStr: email){
            self.progShowInfo(true, msg: "Invalid email")
            return
        }else{
            self.showLoadingView(vc: self)
            let gender = "male"
            ApiManager.editProfile(photo: imageFils.first ?? "", first_name: firstName, last_name: lastName, user_name: usertName, email: email, phone: phone, dob: dob, gender: gender, ssn: ssn) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let user_image = data as! String
                    user?.first_name = self.firstName
                    user?.last_name = self.lastName
                    user?.user_name = self.usertName
                    user?.email = self.email
                    user?.phone = self.phone
                    user?.dob = self.dob
                    user?.ssn = self.ssn
                    if !user_image.isEmpty{
                        user?.picture = user_image
                    }
                    user?.saveUserInfo()
                    user?.loadUserInfo()
                    //self.progShowInfo(true, msg: "Updated successfully!")
                    self.showAlerMessage(message: "Updated successfully!")
                }else{
                    //self.progShowInfo(true, msg: "Network issue!")
                    self.showAlerMessage(message: "Network issue!")
                }
            }
        }
    }
    
    func editInit() {
        setEdtPlaceholder(edtfirstName, placeholderText: "Firstname", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtlastName, placeholderText: "Lastname", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtuserName, placeholderText: "Username", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtEmail, placeholderText: "Email ID", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtPhone, placeholderText: "Phone number", placeColor: UIColor.lightGray, padding: .left(40))
        setEdtPlaceholder(edtDob, placeholderText: "DOB", placeColor: UIColor.lightGray, padding: .left(40))
        
    }
    
}


