//
//  SigninRequestVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class SigninRequestVC: BaseVC1 {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func okBtnClicked(_ sender: Any) {
        UserDefault.setString(key: PARAMS.TYPE, value: "buyer")
        self.gotoVC("LoginNav")
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
