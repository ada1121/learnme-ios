//
//  SellerDetailVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import ExpyTableView
import ExpandableCell
import SwiftyJSON
import Cosmos

class SellerDetailVC: BaseVC1 {
    
    @IBOutlet weak var ServicesView: ExpyTableView!
    @IBOutlet weak var tableViewheight: NSLayoutConstraint!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_sellerName: UILabel!
    @IBOutlet weak var lbl_serviceCategory: UILabel!
    @IBOutlet weak var lbl_currentTime: UILabel!
    @IBOutlet weak var btn_message: UIButton!
    @IBOutlet weak var lbl_aboutme: UILabel!
    @IBOutlet weak var lbl_toolsneeded: UILabel!
    @IBOutlet weak var lbl_languages: UILabel!
    @IBOutlet weak var lbl_montime: UILabel!
    @IBOutlet weak var lbl_tuetime: UILabel!
    @IBOutlet weak var lbl_wedtime: UILabel!
    @IBOutlet weak var lbl_thrtime: UILabel!
    @IBOutlet weak var lbl_fritime: UILabel!
    @IBOutlet weak var lbl_sattime: UILabel!
    @IBOutlet weak var lbl_suntime: UILabel!
    @IBOutlet weak var col_reviews: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var lbl_reviews: UILabel!
    @IBOutlet weak var lbl_portfolios: UILabel!
    @IBOutlet weak var lbl_ratingVal: UILabel!
    @IBOutlet weak var lbl_count: UILabel!
    
    var CategoriesList = [ServiceModel]()
    var ds_reviews = [ReviewModel]()
    var ds_portfolios = [PortfolioModel]()
    var cellHeight = 40 // expandable table view cell height
    
    var newMon = "Closed"
    var newTue = "Closed"
    var newWed = "Closed"
    var newThu = "Closed"
    var newFri = "Closed"
    var newSat = "Closed"
    var newSun = "Closed"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(self.orientationDidChange), name: UIDevice.orientationDidChangeNotification, object: nil)
        toptabType = topTab.review.rawValue
        setSellerProfile()
        if let sellermodel = sellerModel{
            self.lbl_currentTime.text = getLocalTimeStringwithTimezone(sellermodel.timezone!)
            //self.cus_ratingView.rating = Double(sellermodel.rating)
            self.lbl_ratingVal.text = "\(sellermodel.rating ?? 0.0)"
            if sellermodel.count?.toInt() == 0 || sellermodel.count == ""{
                self.lbl_count.text = "(0 Reviews)"
            }else{
                self.lbl_count.text = "(" + sellermodel.count! + " " + "Reviews)"
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        getDataSource4Service()
        getDataSource4Reviews()
        //getDataSource4Portfolios()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        // selected catview reload data
        ServicesView.rowHeight = 40.0 // table view row height defined
        ServicesView.expandingAnimation = .bottom
        ServicesView.collapsingAnimation = .top
        ServicesView.tableFooterView = UIView()
        // selectedCattable View set
        ServicesView.dataSource = self
        ServicesView.delegate = self
        ServicesView.reloadData()
        tableViewheight.constant = ServicesView.contentSize.height
        self.view.layoutIfNeeded()
    }
    
    func localTime(in timeZone: String) -> String {
        let f = ISO8601DateFormatter()
        f.formatOptions = [.withInternetDateTime]
        f.timeZone = TimeZone(identifier: timeZone)
        return f.string(from: Date())
    }
    
    @objc func orientationDidChange() {
        switch UIDevice.current.orientation {
        case .portrait, .portraitUpsideDown, .landscapeLeft, .landscapeRight: ServicesView.reloadSections(IndexSet(Array(ServicesView.expandedSections.keys)), with: .none)
        default:break
        }
    }
    
    func setSellerProfile() {
        if let sellermodel = sellerModel{
            self.lbl_sellerName.text = sellermodel.user_name
            var num = 0
            var serviceCategories = ""
            for one in sellermodel.serviceModelList{
                num += 1
                if num != sellermodel.serviceModelList.count{
                    serviceCategories = serviceCategories + one.name! + ","
                }else{
                    serviceCategories = serviceCategories + one.name!
                }
            }
            
            self.lbl_serviceCategory.text = serviceCategories
            let url = URL(string: sellermodel.picture ?? "")
            self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
            self.lbl_aboutme.text = sellermodel.des
            self.lbl_toolsneeded.text = sellermodel.tools
            self.lbl_languages.text = sellermodel.languages
            
            if let availabletimeModel = sellermodel.availableTimeModel{
                
                let mon = availabletimeModel.mon
                let tue = availabletimeModel.tue
                let wed = availabletimeModel.wed
                let thu = availabletimeModel.thr
                let fri = availabletimeModel.fri
                let sat = availabletimeModel.sat
                let sun = availabletimeModel.sun
                
                print("mon====>",mon as Any)
                print("tue====>",tue as Any)
                print("wed====>",wed as Any)
                print("thu====>",thu as Any)
                print("fri====>",fri as Any)
                print("sat====>",sat as Any)
                print("sun====>",sun as Any)
                
                let flowspace = " " + "~" + " "

                if mon != "Closed"{
                    let startTime: String = String(mon!.split(separator: "~").first!).trim()
                    let lastTime: String = String(mon!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 1{ // preday
                        newSun = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 1 && lastdate.dayNumberOfWeek() == 2{// preday - currentday
                        newSun = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                        newMon = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 2 && lastdate.dayNumberOfWeek() == 2{
                        newMon = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 2 && lastdate.dayNumberOfWeek() == 3{// currentday - nextday
                        newMon = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                        newTue = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 3{ // next day
                        newTue = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if tue != "Closed"{// mon week = 3
                    let startTime: String = String(tue!.split(separator: "~").first!).trim()
                    let lastTime: String = String(tue!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 2{ // preday
                        newMon = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 2 && lastdate.dayNumberOfWeek() == 3{// preday - currentday
                        if newMon == "Closed"{
                            newMon = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newTue = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newMon = newMon + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newTue = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }
                    }else if startdate.dayNumberOfWeek() == 3 && lastdate.dayNumberOfWeek() == 3{
                        newTue = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 3 && lastdate.dayNumberOfWeek() == 4{// currentday - nextday
                        if newTue == "Closed"{
                            newTue = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newWed = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newTue = newTue + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newWed = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }


                    }else if startdate.dayNumberOfWeek() == 4{ // next day
                        newWed = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if wed != "Closed"{
                    let startTime: String = String(wed!.split(separator: "~").first!).trim()
                    let lastTime: String = String(wed!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 3{ // preday
                        newTue = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 3 && lastdate.dayNumberOfWeek() == 4{// preday - currentday
                        if newTue == "Closed"{
                            newTue = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newWed = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newTue = newTue + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newWed = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }

                    }else if startdate.dayNumberOfWeek() == 4 && lastdate.dayNumberOfWeek() == 4{
                        newWed = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 4 && lastdate.dayNumberOfWeek() == 5{// currentday - nextday
                        if newWed == "Closed"{
                            newWed = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newThu = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newWed = newWed + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newThu = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }

                    }else if startdate.dayNumberOfWeek() == 5{ // next day
                        newThu = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if thu != "Closed"{
                    let startTime: String = String(thu!.split(separator: "~").first!).trim()
                    let lastTime: String = String(thu!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 4{ // preday
                        newWed = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 4 && lastdate.dayNumberOfWeek() == 5{// preday - currentday
                        if newWed == "Closed"{
                            newWed = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newThu = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newWed = newWed + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newThu = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }
                    }else if startdate.dayNumberOfWeek() == 5 && lastdate.dayNumberOfWeek() == 5{
                        newThu = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 5 && lastdate.dayNumberOfWeek() == 6{// currentday - nextday
                        if newThu == "Closed"{
                            newThu = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newFri = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newThu = newThu + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newFri = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }

                    }else if startdate.dayNumberOfWeek() == 6{ // next day
                        newFri = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if fri != "Closed"{
                    let startTime: String = String(fri!.split(separator: "~").first!).trim()
                    let lastTime: String = String(fri!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 5{ // preday
                        newThu = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 5 && lastdate.dayNumberOfWeek() == 6{// preday - currentday
                        if newThu == "Closed"{
                            newThu = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newFri = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newThu = newThu + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newFri = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }
                    }else if startdate.dayNumberOfWeek() == 6 && lastdate.dayNumberOfWeek() == 6{
                        newFri = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 6 && lastdate.dayNumberOfWeek() == 7{// currentday - nextday
                        if newFri == "Closed"{
                            newFri = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSat = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newFri = newFri + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSat = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }
                    }else if startdate.dayNumberOfWeek() == 7{ // next day
                        newSat = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if sat != "Closed"{
                    let startTime: String = String(sat!.split(separator: "~").first!).trim()
                    let lastTime: String = String(sat!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 6{ // preday
                        newFri = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 6 && lastdate.dayNumberOfWeek() == 7{// preday - currentday
                        if newFri == "Closed"{
                            newFri = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSat = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newFri = newFri + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSat = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }

                    }else if startdate.dayNumberOfWeek() == 7 && lastdate.dayNumberOfWeek() == 7{
                        newSat = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 7 && lastdate.dayNumberOfWeek() == 1{// currentday - nextday
                        if newSat == "Closed"{
                            newSat = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }else{
                            newSat = newSat + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                        }
                    }else if startdate.dayNumberOfWeek() == 1{ // next day
                        newSun = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }

                if sun != "Closed"{
                    let startTime: String = String(sun!.split(separator: "~").first!).trim()
                    let lastTime: String = String(sun!.split(separator: "~").last!).trim()
                    let startdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(startTime.toInt()!)))!
                    let lastdate : Date = getDateFromDateString(getonlyDateFromTimeStamp(Int64(lastTime.toInt()!)))!
                    if lastdate.dayNumberOfWeek() == 7{ // preday
                        newSat = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 7 && lastdate.dayNumberOfWeek() == 1{// preday - currentday
                        if newSat == "Closed"{
                            newSat = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            if newSun == "Closed"{
                                newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                            }else{
                                newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000)) + newSun
                            }

                        }else{
                            newSat = newSat + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            if newSun == "Closed"{
                                newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                            }else{
                                newSun = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000)) + newSun
                            }
                        }

                    }else if startdate.dayNumberOfWeek() == 1 && lastdate.dayNumberOfWeek() == 1{
                        newSun = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }else if startdate.dayNumberOfWeek() == 1 && lastdate.dayNumberOfWeek() == 2{// currentday - nextday
                        if newSun == "Closed"{
                            newSun = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            if newMon == "Closed"{
                                newMon = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                            }else{
                                newMon = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000)) + "," + " " + newMon
                            }

                        }else{
                            newSun = newSun + "," + " " + gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + "12 AM"
                            if newMon == "Closed"{
                                newMon = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                            }else{
                                newMon = "12 AM" + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000)) + "," + " " + newMon
                            }

                        }

                    }else if startdate.dayNumberOfWeek() == 2{ // next day
                        newMon = gethourTimeFromTimeStamp(Int64(startTime.toInt()!)) + flowspace + gethourTimeFromTimeStamp(Int64(lastTime.toInt()! + 60000))
                    }
                }
                self.lbl_montime.text = newMon
                self.lbl_tuetime.text = newTue
                self.lbl_wedtime.text = newWed
                self.lbl_thrtime.text = newThu
                self.lbl_fritime.text = newFri
                self.lbl_sattime.text = newSat
                self.lbl_suntime.text = newSun
            }
        }
    }
    
    func getDataSource4Service() {
       if let sellermodel = sellerModel {
           self.CategoriesList = sellermodel.serviceModelList
        
        for i in 0 ..< CategoriesList.count {
//            public fileprivate(set) var expandedSections: [Int: Bool] = [:]
            ServicesView.expandedSections[i] = true
        }
       }
    }
    
    func getDataSource4Reviews() {
        self.showLoadingView(vc: self)
        if let sellermodel = sellerModel{
            ApiManager.getReview(seller_id: sellermodel.id!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let dict = JSON(data as Any)
                    //dump(dict, name: "dict=====>")
                    self.ds_reviews.removeAll()
                    let review_info = dict["review_info"].arrayObject
                    if let reviewinfo = review_info{
                        var num = 0
                        for one in reviewinfo{
                            num += 1
                            let json = JSON(one)
                            self.ds_reviews.append(ReviewModel(json))
                            if num == reviewinfo.count{
                                self.col_reviews.reloadData()
                                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                                layout.minimumInteritemSpacing = 0
                                layout.minimumLineSpacing = 0
                                let height =  self.col_reviews.collectionViewLayout.collectionViewContentSize.height
                                self.collectionViewHeight.constant = height
                                self.view.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
        }
    }
    
    func getDataSource4Portfolios() {
        self.showLoadingView(vc: self)
        if let sellermodel = sellerModel{
            ApiManager.getPortofolio(user_id: sellermodel.id!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let dict = JSON(data as Any)
                    //dump(dict, name: "dict=====>")
                    self.ds_portfolios.removeAll()
                    let portfolio_info = dict["portofolios"].arrayObject
                    if let portfolioinfo = portfolio_info{
                        var num = 0
                        for one in portfolioinfo{
                            num += 1
                            let json = JSON(one)
                            self.ds_portfolios.append(PortfolioModel(json))
                            if num == portfolioinfo.count{
                                self.col_reviews.reloadData()
                                let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
                                layout.minimumInteritemSpacing = 0
                                layout.minimumLineSpacing = 0
                                let height =  self.col_reviews.collectionViewLayout.collectionViewContentSize.height
                                self.collectionViewHeight.constant = height
                                self.view.layoutIfNeeded()
                            }
                        }
                    }
                }
            }
        }
    }
    
    @IBAction func gotoAppointment(_ sender: Any) {
        let btn_appointment = sender as! UIButton
        let section = btn_appointment.tag
        let row = btn_appointment.superview?.tag
        
        sellerAvaillableTimemodel = nil
        sellerSeriveName = nil
        sellerServieTime = nil
        sellerServiePrice = nil
        sellerAvaillableTimemodel = AvailableTimeModel(mon: lbl_montime.text, tue: lbl_tuetime.text, wed: lbl_wedtime.text, thr: lbl_thrtime.text, fri: lbl_fritime.text, sat: lbl_sattime.text, sun: lbl_suntime.text)
        
        sellerSeriveName = self.CategoriesList[section].name
        sellerServieTime = self.CategoriesList[section].timePriceModelList[row! - 1].time!
        sellerServiePrice = CategoriesList[section].timePriceModelList[row! - 1].price!
            
        self.gotoStoryBoardVC("AppointmentVC", fullscreen: true)
        
    }
    
    @IBAction func goBack(_ sender: Any) {
        //self.dismiss(animated: false, completion: nil)
        self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
    
    @IBAction func gotoReviews(_ sender: Any) {
        self.lbl_reviews.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.lbl_reviews.textColor = .black
        self.lbl_portfolios.font = UIFont.systemFont(ofSize: 15.0)
        self.lbl_portfolios.textColor = .lightGray
        self.collectionViewHeight.constant = 0
        toptabType = topTab.review.rawValue
        self.getDataSource4Reviews()
    }
    
    @IBAction func gotoPortfolios(_ sender: Any) {
        self.lbl_portfolios.font = UIFont.boldSystemFont(ofSize: 15.0)
        self.lbl_portfolios.textColor = .black
        self.lbl_reviews.font = UIFont.systemFont(ofSize: 15.0)
        self.lbl_reviews.textColor = .lightGray
        self.collectionViewHeight.constant = 0
        toptabType = topTab.portfolio.rawValue
        self.getDataSource4Portfolios()
    }
    
    @IBAction func gotoMessage(_ sender: Any) {
        
        if let sellermodel = sellerModel{
            chatting_option = 1
            friendID = sellermodel.id
            msgPartnerName = sellermodel.user_name ?? "User"
            msgPartnerPicture = sellermodel.picture ?? ""
        }
        self.gotoVC("MessageSendVC")
    }
}

//MARK: ExpyTableViewDataSourceMethods
extension SellerDetailVC : ExpyTableViewDataSource {
    func tableView(_ tableView: ExpyTableView, canExpandSection section: Int) -> Bool {
        return true
    }
    
    func tableView(_ tableView: ExpyTableView, expandableCellForSection section: Int) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: SelCatCell.self)) as! SelCatCell
        cell.labelCatName.text = CategoriesList[section].name
        cell.layoutMargins = UIEdgeInsets.zero
        cell.showSeparator()
        return cell
    }
}

// Selected category autodimention
extension SellerDetailVC {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        //print("DID SELECT row: \(indexPath.row), section: \(indexPath.section)")
    }
    /*func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }*/
}

//MARK: UITableView Data Source Methods
extension SellerDetailVC {
    func numberOfSections(in tableView: UITableView) -> Int {
        //print("\(CategoriesList.count)")
        return CategoriesList.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print("Row count for section \(section) is \(CategoriesList[section].miniFood.count)")
        return CategoriesList[section].timePriceModelList.count + 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: DetailValTableViewCell.self)) as! DetailValTableViewCell
        //  expandable TableView changePart
        cell.labelselectedName.text = CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].time! + "min"
        cell.labelselectedvalue.text = "$" + CategoriesList[indexPath.section].timePriceModelList[(indexPath.row - 1)].price!
        cell.btn_appointment.tag = indexPath.section
        cell.btn_appointment.superview?.tag = indexPath.row
        cell.layoutMargins = UIEdgeInsets.zero
         //cell.showSeparator()
        cell.hideSeparator()
        cellHeight = Int(cell.bounds.size.height)
        return cell
    }
}


//MARK: ExpyTableView delegate methods
extension SellerDetailVC : ExpyTableViewDelegate {
    func tableView(_ tableView: ExpyTableView, expyState state: ExpyState, changeForSection section: Int) {
    
        switch state {
        case .willExpand:
            return
            
        case .willCollapse:
            return
            
        case .didExpand:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant += CGFloat(detailnum * cellHeight )
            tableViewheight.constant = ServicesView.contentSize.height
            
        case .didCollapse:
            let detailnum = CategoriesList[section].timePriceModelList.count
            tableViewheight.constant -= CGFloat(detailnum * cellHeight)
            tableViewheight.constant = ServicesView.contentSize.height
        }
    }
}

extension UITableViewCell {
    
    func showSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    func hideSeparator() {
        DispatchQueue.main.async {
            self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
        }
    }
}

extension SellerDetailVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                return self.ds_reviews.count
            }else{
                return self.ds_portfolios.count
            }
        }else{
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reviewCell", for: indexPath) as! reviewCell
                cell.entity = self.ds_reviews[indexPath.row]
                return cell
            }else{
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "portfolioCell", for: indexPath) as! portfolioCell
                cell.entity = self.ds_portfolios[indexPath.row]
                return cell
            }
        }else{
            return UICollectionViewCell.init()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
    }
}

extension SellerDetailVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if let toptabtype = toptabType {
            if toptabtype == topTab.review.rawValue{
                let w = collectionView.frame.size.width
                let formatter = NumberFormatter()
                formatter.numberStyle = NumberFormatter.Style.decimal
                formatter.roundingMode = NumberFormatter.RoundingMode.ceiling
                formatter.maximumFractionDigits = 0
                
                let content = ds_reviews[indexPath.row].review
                let font = UIFont.systemFont(ofSize: 12)
                let title_width = content!.size(OfFont: font).width // size: {w: 98.912 h: 14.32}
                let title_height = content!.size(OfFont: font).height
                let rate : CGFloat = title_width / (UIScreen.main.bounds.width - 20)
                
                let roundedValue = formatter.string(for: rate)
                let prefix_number = roundedValue?.toInt()
                let toptitle_height = title_height * CGFloat(prefix_number!)
                
                let h: CGFloat = 90 + toptitle_height
                return CGSize(width: w, height: h)
            }else{
                let w = collectionView.frame.size.width
                let h = w / 4 * 3
                return CGSize(width: w, height: h)
            }
        }
        else{
            return CGSize(width: 0, height: 0)
        }
    }
}
