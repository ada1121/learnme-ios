//
//  MessageSendVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/26/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Firebase
import Foundation
import IQKeyboardManagerSwift
import SwiftyJSON
import GDCheckbox
import KRProgressHUD
import KRActivityIndicatorView
import SKCountryPicker
import Photos
import DKImagePickerController
import DKCamera
import FirebaseAuth
import FirebaseStorage
import FirebaseDatabase

class MessageSendVC: BaseVC1, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var avartarImg: UIImageView!
    @IBOutlet weak var msgTextField: UITextField!
    @IBOutlet weak var cons_tblHeight: NSLayoutConstraint!
    @IBOutlet weak var tbvChat: UITableView?
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var uiv_postView: UIView!
    @IBOutlet weak var imv_Post: UIImageView!
    @IBOutlet weak var imv_Attach: UIImageView!
    
    let pickerController = DKImagePickerController()
    var imageFils = [String]()
    var assets: [DKAsset] = []
    
    var newChatHandle: UInt?
    var newStatusHandle: UInt?
    var msgdataSource =  [ChatModel]()
    var statuesList =  [StatusModel]()
    let cellSpacingHeight: CGFloat = 10 // cell line spacing must use section instead of row
    let ref = Database.database().reference()
    let meListroomId = "u" + "\(user!.id ?? 0)"
    
    var mestatusroomID = ""
    var partnerstatusroomID = ""
    var partnerListroomId = ""
    
    let pathList = Database.database().reference().child("list")
    let pathStatus = Database.database().reference().child("status")
    var partnerOnline = false
    var unreadMessageNum = 0
    var downloadURL: URL?
    var working4imagesend = false
    var working4textsend = false
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        var roomID = ""
        if chatting_option == 1{ // from other conroller
             roomID   = "\(user!.id ?? 0)" + "_" +  friendID!
            partnerstatusroomID = "\(user!.id ?? 0)" + "_" + friendID!
            partnerListroomId = "u" + friendID!
            mestatusroomID = friendID! + "_" + "\(user!.id ?? 0)"
            
        }
        else{ // from chat list controller
            roomID = "\(user!.id ?? 0)" + "_" +  "\(msgIndexId)"
            partnerstatusroomID = "\(user!.id ?? 0)" + "_" + msgIndexId
            partnerListroomId = "u" + msgIndexId
            mestatusroomID = msgIndexId + "_" + "\(user!.id ?? 0)"
        }
        editInit()
        setUIinit()
        self.setUnreadMessageNum4me()
        self.setOnlinestatus4me()
        imv_Attach.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        self.fireBaseNewChatListner(roomID)
        tbvChat?.delegate = self
        tbvChat?.dataSource = self
        self.tbvChat?.allowsSelection = true
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    // add attach image
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.openProfile()
    }
    
    func openProfile() {
        self.initDkimgagePicker()
        pickerController.maxSelectableCount = 1
        pickerController.dismissCamera()
        pickerController.showsCancelButton = true
        pickerController.setSelectedAssets(assets: assets)
        pickerController.didSelectAssets = {(assets: [DKAsset]) in self.onSelectAssets(assets: assets)}
        pickerController.modalPresentationStyle = .fullScreen
        present(pickerController, animated: true)
    }
    
    func initDkimgagePicker(){
       pickerController.assetType = .allAssets
       pickerController.allowSwipeToSelect = true
       pickerController.sourceType = .both
    }
    
    func onSelectAssets(assets : [DKAsset]) {
         self.assets.removeAll()
        self.assets = assets
        if assets.count > 0 {
            for asset in assets {
                asset.fetchOriginalImage(options: nil, completeBlock: { image, info in
                    self.uiv_postView.isHidden = false
                    self.gotoUploadProfile(image)
                })
            }
        }
        else {}
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_Post.image = image
            
            self.uploadFile2Firebase(imageFils.first!) { (isSuccess, downloadURL) in
               
                if isSuccess{
                    guard  let downloadURL = downloadURL else {
                        return
                    }
                    self.downloadURL = downloadURL
                }else{
                }
            }
        }
    }
    
    func setUIinit()  {
        self.tbvChat?.separatorStyle = .none
        self.tbvChat?.estimatedRowHeight = 80
        
        self.lbl_userName.text = msgPartnerName
        let url = URL(string: msgPartnerPicture)
        self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
        self.uiv_postView.isHidden = true
    }
    
    func setUnreadMessageNum4me(){
        let usersRef = self.pathList.child(self.meListroomId).child(self.partnerListroomId)
            usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild("unread"){
               self.pathList.child(self.meListroomId).child(self.partnerListroomId).child("unread").setValue("0")
            }else{
                print("false")
            }
        })
    }
    
    func setOnlinestatus4me()  {
        //if chatting_option == 0{
        pathStatus.child(mestatusroomID).removeValue()
        var statusObject = [String: String]()
        statusObject["online"] = "online"
        statusObject["sender_id"] = "\(user!.id ?? 0)"
        statusObject["time"] = "\(Int(NSDate().timeIntervalSince1970) * 1000)"
        pathStatus.child(mestatusroomID).childByAutoId().setValue(statusObject)
       // }
    }
    
    func getPartnerOnlineStatus(completion :  @escaping (_ success: Bool, _ onineval: Bool) -> ()){
        let usersRef = pathStatus.child(partnerstatusroomID)
        let queryRef = usersRef.queryOrdered(byChild: "sender_id")
        queryRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.exists(){
                for snap in snapshot.children {
                    let userSnap = snap as! DataSnapshot
                    //let uid = userSnap.key //the uid of each user
                    let userDict = userSnap.value as! [String:AnyObject]
                    let online = userDict["online"] as! String
                    if online != ""{
                        let status = online == "online" ? true : false
                        completion(true, status)
                    }else{
                        completion(true, false)
                    }
                }
            }else{
                completion(true, false)
            }
        })
    }
    
    func getPartnerUnreadMessages(completion :  @escaping (_ success: Bool, _ number: String) -> ()){
            let usersRef = pathList.child(partnerListroomId).child(meListroomId)
            usersRef.observeSingleEvent(of: .value, with: { (snapshot) in
            if snapshot.hasChild("unread"){
             if let userDict = snapshot.value as? [String:AnyObject]{
                if let number = userDict["unread"] as? String{
                    let numBer = number
                    completion(true,numBer)
                }
             }else{
                 completion(true,"0")
             }
            }else{
                completion(true,"0")
            }
        })
    }
    
    func fireBaseNewChatListner(_ roomId : String)  {
        //self.showLoadingView(vc: self)HUDHUD()
        var num  = 0
        newChatHandle = FirebaseAPI.setMessageListener(roomId){ (chatModel) in
            //self.hideLoadingView()
            //dump(chatModel, name: "Chatmodel=====>")
            num += 1
            self.msgdataSource.append(chatModel)
            if num == self.msgdataSource.count{
                self.tbvChat?.reloadData()
                self.tbvChat?.scrollToBottomRow()
            }
        }
    }
    
    func editInit()  {
        msgTextField.addPadding(.left(16))
        msgTextField.attributedPlaceholder = NSAttributedString(string: "Your message",attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.setOfflinestatus4me()
        self.dismiss(animated : false)
    }
    
    func setOfflinestatus4me()  {
        //if chatting_option == 0{
            pathStatus.child(mestatusroomID).removeValue()
            var statusObject = [String: String]()
            statusObject["online"] = "offline"
            statusObject["sender_id"] = "\(user!.id ?? 0)"
            statusObject["time"] = "\(Int(NSDate().timeIntervalSince1970) * 1000)"
            pathStatus.child(mestatusroomID).childByAutoId().setValue(statusObject)
        //}
    }
    
    @IBAction func sendAction(_ sender: Any) {
        if !working4textsend{
            if checkValid() {
                self.getPartnerUnreadMessages { (isSuccess, num) in
                    if isSuccess{
                        self.unreadMessageNum = num.toInt() ?? 0
                        print("unreadMessageNum==>",self.unreadMessageNum)
                        self.getPartnerOnlineStatus { (isSuccess, online) in
                            if isSuccess{
                                print("successstate==>",online)
                                self.doSend(online)
                            }else{
                                print("failedstate===>",online)
                                self.showToast("Network issue")
                            }
                        }
                    }
                }
            }
        }
    }
    
    func setNewUnreadMessage4partner(number: Int, completion: @escaping (_ success: Bool) -> ()) {
        //if chatting_option == 0{
            pathList.child(partnerListroomId).child(meListroomId).child("unread").setValue("\(number)")
            completion(true)
        //}else{
            //completion(true)
        //}
    }
    
    func checkValid() -> Bool {
        self.view.endEditing(true)
        if msgTextField.text!.isEmpty {
            return false
        }
        return true
    }
    
    func doSend(_ online: Bool) {
        
        let msgcontent = msgTextField.text!
        self.msgTextField.text! = ""
        
        let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
        var chatObject = [String: String]()
        // MARK: for message object for me - chatObject
        chatObject["message"]     = msgcontent
        chatObject["image"]       = ""
        chatObject["photo"]       = "\(user!.picture ?? "")"
        chatObject["sender_id"]   = "\(user!.id ?? 0)"
        chatObject["time"]        = "\(timeNow)" as String
        chatObject["name"]        = "\(user!.user_name ?? "")"
        var roomId1 = ""
        if chatting_option == 1{
            roomId1   = "\(user!.id ?? 0)" + "_" +  friendID!
       }
       else{
           roomId1 = "\(user!.id ?? 0)" + "_" + "\(msgIndexId)"
       }// MARK: for me message node
       
        FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
            
            if status {
                var roomId2 = ""
                if chatting_option == 1{
                    roomId2   = friendID! + "_" + "\(user!.id ?? 0)"
                }
                else{
                    roomId2 = "\(msgIndexId)" + "_" + "\(user!.id ?? 0)"
                }
                // MARK:  // MARK: for message object for me - chatObject same object
                FirebaseAPI.sendMessage(chatObject, roomId2) { (status, message) in
                    if status {
                        
                        var listObject = [String: String]()
                        
                        if chatting_option == 1{
                            listObject["id"]   = "\(friendID ?? "0")"
                        }
                        else{
                            listObject["id"]   = msgIndexId
                        }
                        // MARK: for list view for my list object - - listobject
                        
                        listObject["message"]        = msgcontent
                        listObject["sender_id"]      = "\(user!.id ?? 0)"
                        listObject["sender_name"]    = "\(user!.user_name ?? "")"
                        listObject["sender_photo"]   = "\(user!.picture ?? "")"
                        listObject["time"]           = "\(timeNow)" as String
                        listObject["unread"]         = "0"
                        
                        FirebaseAPI.sendListUpdate(listObject, "u" + "\(user!.id ?? 0)", partnerid: "u" + listObject["id"]!){
                            (status,message) in
                            if status{
                                var listObject1 = [String: String]()
                                var partnerid = ""
                                if chatting_option == 1{
                                   partnerid   = "\(friendID ?? "0")"
                                    
                                }
                                else{
                                    partnerid   = msgIndexId
                                    
                                }
                                // MARK:  for list view for partner's list object - listobject1
                                listObject1["id"]            = "\(user!.id ?? 0)"
                                listObject1["message"]       = msgcontent
                                listObject1["sender_id"]     = "\(user!.id ?? 0)"
                                listObject1["sender_name"]   = "\(user!.user_name ?? "")"
                                listObject1["sender_photo"]  = "\(user!.picture ?? "")"
                                listObject1["time"]          = "\(timeNow)" as String
                                
                                if !online{
                                    self.unreadMessageNum += 1
                                    ApiManager.submitFCM(friend_id: partnerid, notitext: msgcontent) { (isSuccess, data) in
                                        if isSuccess{
                                            listObject1["unread"]        =  "\(self.unreadMessageNum)"
                                            FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                                (status,message) in
                                                if status{
                                                    self.working4textsend = false
                                                }
                                            }
                                        }else{
                                            listObject1["unread"]        =  "\(self.unreadMessageNum)"
                                            FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                                (status,message) in
                                                if status{
                                                    self.working4textsend = false
                                                }
                                            }
                                        }
                                    }
                                    
                                }else{
                                    listObject1["unread"]       =  "0"
                                    FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                        (status,message) in
                                        if status{
                                            self.working4textsend = false
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        //print(message)
                    }
                }
            } else {
                //print(message)
            }
        }
    }
    
    func uploadFile2Firebase(_ localFile: String, completion: @escaping (_ success: Bool, _ path: URL?) -> ())  {
        self.showLoadingView(vc: self)
        let storage = Storage.storage()
        let storageRef = storage.reference()
        let riversRef = storageRef.child("/" + "\(randomString(length: 2))" + ".jpg")
        
        _ = riversRef.putFile(from: URL(fileURLWithPath: localFile), metadata: nil) { metadata, error in
            self.hideLoadingView()
            guard metadata != nil else {
                completion(false, nil)
                return
            }
            riversRef.downloadURL { (url, error) in
                guard let downloadURL = url else {
                    completion(false,nil)
                    return
                }
                completion(true, downloadURL)
            }
        }
    }
    
    func imgageSend(_ online: Bool)  {
        if !working4imagesend{
            self.showLoadingView(vc: self)
            working4imagesend = true
            guard let downloadRL = self.downloadURL else {
                return
            }
             let timeNow = Int(NSDate().timeIntervalSince1970) * 1000
             var chatObject = [String: String]()
             // MARK: for message object for me - chatObject
             chatObject["message"]     = ""
             chatObject["image"]       = "\(downloadRL)"
             chatObject["photo"]       = "\(user!.picture ?? "")"
             chatObject["sender_id"]   = "\(user!.id ?? 0)"
             chatObject["time"]        = "\(timeNow)" as String
             chatObject["name"]        = "\(user!.user_name ?? "")"
             var roomId1 = ""
             if chatting_option == 1{
                 roomId1   = "\(user!.id ?? 0)" + "_" +  friendID!
             }
             else{
                roomId1 = "\(user!.id ?? 0)" + "_" + "\(msgIndexId)"
             }// MARK: for me message node
            
             FirebaseAPI.sendMessage(chatObject, roomId1) { (status, message) in
                 
                 if status {
                     var roomId2 = ""
                     if chatting_option == 1{
                         roomId2   = friendID! + "_" + "\(user!.id ?? 0)"
                     }
                     else{
                         roomId2 = "\(msgIndexId)" + "_" + "\(user!.id ?? 0)"
                     }
                     // MARK:  // MARK: for message object for me - chatObject same object
                     FirebaseAPI.sendMessage(chatObject, roomId2) { (status, message) in
                         if status {
                             
                             var listObject = [String: String]()
                             
                             if chatting_option == 1{
                                 listObject["id"]   = "\(friendID ?? "0")"
                             }
                             else{
                                 listObject["id"]   = msgIndexId
                             }
                             // MARK: for list view for my list object - - listobject
                             listObject["message"]       = "Shared a file"
                             listObject["sender_id"]     = "\(user!.id ?? 0)"
                             listObject["sender_name"]   = "\(user!.user_name ?? "")"
                             listObject["sender_photo"]  = "\(user!.picture ?? "")"
                             listObject["time"]          = "\(timeNow)" as String
                             listObject["unread"]        = "0"
                             
                             FirebaseAPI.sendListUpdate(listObject, "u" + "\(user!.id ?? 0)", partnerid: "u" + listObject["id"]!){
                                 (status,message) in
                                 if status{
                                     var listObject1 = [String: String]()
                                     var partnerid = ""
                                     if chatting_option == 1{
                                        partnerid   = "\(friendID ?? "0")"
                                         
                                     }
                                     else{
                                         partnerid   = msgIndexId
                                         
                                     }
                                     // MARK:  for list view for partner's list object - listobject1
                                     listObject1["id"]              = "\(user!.id ?? 0)"
                                     listObject1["message"]          = "Shared a file"
                                     listObject1["sender_id"]       = "\(user!.id ?? 0)"
                                     listObject1["sender_name"]     = "\(user!.user_name ?? "")"
                                     listObject1["sender_photo"]    = "\(user!.picture ?? "")"
                                     listObject1["time"]            = "\(timeNow)" as String
                                     if !online{
                                         self.unreadMessageNum += 1
                                         listObject1["unread"]      =  "\(self.unreadMessageNum)"
                                         ApiManager.submitFCM(friend_id: partnerid, notitext: "Shared a file") { (isSuccess, data) in
                                            if isSuccess{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbvChat?.scrollToBottomRow()
                                                    }
                                                }
                                            }else{
                                                FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                                    (status,message) in
                                                    if status{
                                                       self.hideLoadingView()
                                                       self.working4imagesend = false
                                                       self.uiv_postView.isHidden = true
                                                       self.imageFils.removeAll()
                                                       self.imv_Post.image = nil
                                                       self.tbvChat?.scrollToBottomRow()
                                                    }
                                                }
                                            }
                                        }
                                     }else{
                                         listObject1["unread"]      =  "0"
                                        FirebaseAPI.sendListUpdate(listObject1, "u" + partnerid, partnerid: "u" + "\(user!.id ?? 0)"){
                                            (status,message) in
                                            if status{
                                               self.hideLoadingView()
                                               self.working4imagesend = false
                                               self.uiv_postView.isHidden = true
                                               self.imageFils.removeAll()
                                               self.imv_Post.image = nil
                                               self.tbvChat?.scrollToBottomRow()
                                            }
                                        }
                                     }
                                 }
                             }
                         } else {
                             //print(message)
                         }
                     }
                 } else {
                     //print(message)
                 }
             }
        }
    }

    @IBAction func postImageBtnclicked(_ sender: Any) {
        self.getPartnerUnreadMessages { (isSuccess, num) in
            if isSuccess{
                self.unreadMessageNum = num.toInt() ?? 0
                print("unreadMessageNum==>",self.unreadMessageNum)
                self.getPartnerOnlineStatus { (isSuccess, online) in
                    if isSuccess{
                        print("successstate==>",online)
                        self.imgageSend(online)
                    }else{
                        print("failedstate===>",online)
                        self.showToast("Network issue")
                    }
                }
            }
        }
    }
    
    @IBAction func cancelImageBtnClicked(_ sender: Any) {
        self.uiv_postView.isHidden = true
        self.imageFils.removeAll()
        self.imv_Post.image = nil
    }
    
    @IBAction func meBtnClicked(_ sender: Any) {
        print("me")
        let button = sender as! UIButton
        let indexpathrow = button.tag
        if self.msgdataSource[indexpathrow].image != ""{
            if self.msgdataSource[indexpathrow].me{
                imageURL = self.msgdataSource[indexpathrow].image
                leftandright = self.msgdataSource[indexpathrow].me
                self.gotoVC("ImageShowView")
            }
        }
    }
    
    @IBAction func youBtnClicked(_ sender: Any) {
        print("you")
        let button = sender as! UIButton
        let indexpathrow = button.tag
        if self.msgdataSource[indexpathrow].image != ""{
            if !self.msgdataSource[indexpathrow].me{
                imageURL = self.msgdataSource[indexpathrow].image
                leftandright = self.msgdataSource[indexpathrow].me
                self.gotoVC("ImageShowView")
            }
        }
    }
    // MARK: In order to set the line spacing between cell in the table view we must use section insteaad of the row.
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.msgdataSource.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if self.msgdataSource[indexPath.section].image == ""{
            let cell = tbvChat?.dequeueReusableCell(withIdentifier: "ChatCell", for:indexPath) as! ChatCell
            cell.selectionStyle = .none
            cell.entity = msgdataSource[indexPath.section]
            
            if self.msgdataSource[indexPath.section].me{
                cell.meView.isHidden = false
                cell.youView.isHidden = true
            }else{
                cell.meView.isHidden = true
                cell.youView.isHidden = false
            }
            return cell
        }else{
            let cell = tbvChat?.dequeueReusableCell(withIdentifier: "chatImageCell", for:indexPath) as! chatImageCell
            cell.selectionStyle = .none
            cell.entity = msgdataSource[indexPath.section]
            cell.btn_me.tag = indexPath.section
            cell.btn_you.tag = indexPath.section
            if self.msgdataSource[indexPath.section].me{
                cell.uiv_me.isHidden = false
                cell.uiv_you.isHidden = true
            }else{
                cell.uiv_me.isHidden = true
                cell.uiv_you.isHidden = false
            }
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return cellSpacingHeight
    }
}





