//
//  PayoutScheduleVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/27/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import WebKit

class PayoutScheduleVC: UIViewController {
    @IBOutlet weak var ui_webView: WKWebView!
    @IBOutlet weak var lbl_topCaption: UILabel!
    var urlLink : URL!
    override func viewDidLoad() {
        super.viewDidLoad()
        if user?.type == PARAMS.SELLER{
            self.lbl_topCaption.text = "Payout Schedule"
            urlLink = URL(string: Constants.payoutScheduleLink)!
        }else{
            self.lbl_topCaption.text = "Refund Policy"
            urlLink = URL(string: Constants.refundPolicyLink)!
        }
        
        ui_webView.load(URLRequest(url: urlLink))

    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
