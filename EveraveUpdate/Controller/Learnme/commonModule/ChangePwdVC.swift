//
//  ChangePwdVC.swift
//  EveraveUpdate
//
//  Created by Mac on 6/29/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ChangePwdVC: BaseVC1 {

    @IBOutlet weak var edt_oldPwd: UITextField!
    @IBOutlet weak var edt_newPwd: UITextField!
    @IBOutlet weak var edt_confirmPwd: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        let oldpwd = edt_oldPwd.text
        let newpwd = edt_newPwd.text
        let confirmpwd = edt_confirmPwd.text
        
        if oldpwd != user!.password{
            showAlerMessage(message: "Invalid current password")
            return
        }
        if oldpwd == "" || newpwd == "" || confirmpwd == ""{
            showAlerMessage(message: "Enter your password")
            return
        }
        if newpwd != confirmpwd{
            showAlerMessage(message: "Confirm your password")
            return
        }else{
            self.showLoadingView(vc: self)
            ApiManager.changePwd( password: newpwd!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    self.showAlerMessage(message: "Updated your password!")
                }else{
                    self.showAlerMessage(message: "Network issue.")
                }
            }
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
