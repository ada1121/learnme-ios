//
//  ImageShowView.swift
//  EveraveUpdate
//
//  Created by Mac on 7/2/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit

class ImageShowView: BaseVC1 {

    @IBOutlet weak var imv_show: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let url = URL(string: imageURL)
        self.imv_show.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
