//
//  MessageViewVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/25/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import SwipeCellKit
import SwiftyJSON
import SwiftyUserDefaults
import Firebase
import FirebaseDatabase

class MessageViewVC: BaseVC1 {

    @IBOutlet weak var topCaptionView: UIView!
    @IBOutlet weak var searchTextfield: UITextField!
    
    @IBOutlet weak var ui_collectionView: UICollectionView!
    var msgDatasource = [MessageSwipeModel]()
    var msgDatasource1 = [MessageSwipeModel]()
    var newChatHandle: UInt?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        editInit()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //let roomId = "u1"
        let roomId = "u" + "\(user!.id ?? 0)"
        userlistListner(roomId)
    }
    
    func editInit()  {
        searchTextfield.addPadding(.left(16))
        searchTextfield.attributedPlaceholder = NSAttributedString(string: "Search message",attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }

    func userlistListner(_ userRoomid : String)  {
        self.msgDatasource.removeAll()
        //self.showLoadingView(vc: self)HUDHUD()
        newChatHandle = FirebaseAPI.getUserList(userRoomid : userRoomid ,handler: { (userlist) in
            //print("userlist == ",userlist)
            guard userlist.id != nil else{
                //self.hideLoadingView()
                return

            }
            
            self.msgDatasource.append(userlist)
            var ids = [String]()
            for one in self.msgDatasource{
                ids.append(one.id!)
            }
            //print(ids.removeDuplicates())
            var num = 0
            self.msgDatasource1.removeAll()
            for  one in ids.removeDuplicates(){
                num += 1
                self.msgDatasource1.append(self.getDataFromID(one))
                if num == ids.removeDuplicates().count{
                    //dump(self.msgDatasource1, name: "msgDatasource1====>")
                    //dump(self.msgDatasource, name: "msgDatasource====.")
                    self.ui_collectionView.reloadData()
                }
            }
          }
        )
    }
    
    func getDataFromID(_ id: String) -> MessageSwipeModel {
        var returnModel : MessageSwipeModel?
        for one in self.msgDatasource{
            if id == one.id{
                returnModel = one
            }
        }
        return returnModel ?? MessageSwipeModel()
    }
    
    @IBAction func goBack(_ sender: Any) {
        if user?.type == PARAMS.BUYER{
            self.gotoStoryBoardVC("MainVC", fullscreen: true)
        }else{
            self.gotoVC("SellerMainVC")
        }
    }
}

extension MessageViewVC : UICollectionViewDelegate, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return msgDatasource1.count
    }
    
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "MessageSwipeCell", for: indexPath) as! MessageSwipeCell
        
        cell.entity = msgDatasource1[indexPath.row]
        cell.delegate = self
    
    if self.msgDatasource1[indexPath.row].unreadMsgNum != "0" && self.msgDatasource1[indexPath.row].unreadMsgNum.toInt() ?? 0 > 0{
        cell.msgUnreadNum.text = self.msgDatasource1[indexPath.row].unreadMsgNum
        cell.UnreadView.isHidden = false
        cell.msgReadTick.isHidden = true
    }else{
        cell.msgUnreadNum.text = ""
        cell.UnreadView.isHidden = true
        cell.msgReadTick.isHidden = false
    }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        chatting_option = 0
        msgIndexId = msgDatasource1[indexPath.row].id!
        msgPartnerName = msgDatasource1[indexPath.row].msgUserName
        msgPartnerPicture = msgDatasource1[indexPath.row].sender_photo
        
        self.gotoVC("MessageSendVC")
    }
}

extension MessageViewVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let h: CGFloat = 80
        let w = collectionView.frame.size.width
        return CGSize(width: w, height: h)
    }
}

extension MessageViewVC: SwipeCollectionViewCellDelegate {
    func collectionView(_ collectionView: UICollectionView, editActionsForItemAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else {
           return nil
        }
        //let requestId = msgDatasource[indexPath.row].requestFriendId

        let deleteAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in
            //self.msgDatasource[indexPath.row].requestFriendName)
            
            let ref = Database.database().reference()
            let userchattingRoom = "u" + "\(user!.id ?? 0)"
            let partnerid = "u" + self.msgDatasource1[indexPath.row].id!
            ref.child("list").child(userchattingRoom).child(partnerid).observe(.childAdded, with: { (snapshot) in

                    snapshot.ref.removeValue(completionBlock: { (error, reference) in
                        if error != nil {
                            print("There has been an error:\(error)")
                        }
                    })
                })
            self.msgDatasource1.remove(at: indexPath.row)
            self.ui_collectionView.reloadData()

        }

        let unreadAction = SwipeAction(style: .default, title: nil) { (action, indexPath) in

            //self.msgDatasource[indexPath.row].readStatus = false
            self.msgDatasource1[indexPath.row].readStatus = true
            // TODO: must be changed
            
            self.msgDatasource1[indexPath.row].unreadMsgNum = "1"
            self.ui_collectionView.reloadData()

        }
               // customize swipe action
        deleteAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)
        unreadAction.transitionDelegate = ScaleTransition(duration: 0.15, initialScale: 0.7, threshold: 0.8)

               // customize the action appearance
        deleteAction.image = UIImage(named: "delete-1")
        unreadAction.image = UIImage(named: "unread-1")
        deleteAction.backgroundColor = UIColor.init(named: "ColorLightPrimary")
        unreadAction.backgroundColor = UIColor.init(named: "ColorLightPrimary")
        
        deleteAction.title = "delete"
        unreadAction.title = "unread"
        deleteAction.textColor = UIColor.black
        unreadAction.textColor = UIColor.black

        return [deleteAction,unreadAction]

    }
}
