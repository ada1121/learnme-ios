//
//  HistoryDetailVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/10/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit
import SwiftyJSON
import Kingfisher
import SwipeCellKit

class HistoryDetailVC: BaseVC1,UITextViewDelegate  {
    
    @IBOutlet weak var txv_problem: UITextView!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var lbl_bookingTime: UILabel!
    @IBOutlet weak var lbl_bookingCategory: UILabel!
    @IBOutlet weak var lbl_bookingCost: UILabel!
    @IBOutlet weak var lbl_bookingDate: UILabel!
    
    override func viewWillAppear(_ animated: Bool) {
      
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txv_problem.delegate = self
        loadLayout()
    }
    
    func loadLayout() {
        if let selectedbooking = historyBooking{
            if user?.type == PARAMS.BUYER{
                let url = URL(string: selectedbooking.seller_picture ?? "")
                self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
                self.lbl_userName.text = selectedbooking.seller_name
            }else{
                let url = URL(string: selectedbooking.buyer_picture ?? "")
                self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
                self.lbl_userName.text = selectedbooking.buyer_name
            }
            self.lbl_bookingCost.text = selectedbooking.service_cost
            self.lbl_bookingCategory.text = selectedbooking.service_name
            self.lbl_bookingTime.text = selectedbooking.service_time
            self.lbl_bookingDate.text = getMessageTimeFromTimeStamp(Int64(selectedbooking.service_date))
        }
        
        self.txv_problem.text = "Please write your problem ..."
        self.txv_problem.textColor = UIColor.lightGray
    }
    func textViewDidBeginEditing(_ textView: UITextView) {
       if txv_problem.textColor == UIColor.lightGray {
           txv_problem.text = nil
           txv_problem.textColor = UIColor.black
       }
   }
       
   func textViewDidEndEditing(_ textView: UITextView) {
       if txv_problem.text.isEmpty {
           txv_problem.text = "Please write your problem ..."
           txv_problem.textColor = UIColor.lightGray
       }
   }
   
    @IBAction func gotoMessagePage(_ sender: Any) {
        chatting_option = 1
        // TODO: just for test
//        let ids = ["0","2","3","4"]
//        friendID = ids[Int(arc4random_uniform(UInt32(ids.count)))]
        if let selectedbooking = historyBooking{
            if user?.type == PARAMS.SELLER{
                friendID = selectedbooking.buyer_id
                msgPartnerName = selectedbooking.buyer_name ?? "User"
                msgPartnerPicture = selectedbooking.buyer_picture ?? ""
            }else{
                friendID = selectedbooking.seller_id
                msgPartnerName = selectedbooking.seller_name ?? "User"
                msgPartnerPicture = selectedbooking.seller_picture ?? ""
            }
        }
        self.gotoVC("MessageSendVC")
    }
    
    @IBAction func deleteaction(_ sender: Any) {
        self.gotoVCModal("DeleteConfirmVC")
    }
    
    @IBAction func gotoBack(_ sender: Any) {
        if user?.type == PARAMS.BUYER{
            self.self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
        }else{
            self.self.gotoStoryBoardVC("HistorySellerVC", fullscreen: true)
        }
    }
}






