//
//  SettingsVC.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/18/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit

class SettingsVC: BaseVC1 , UITableViewDelegate , UITableViewDataSource {

    var settingDatasource = [SettingModel]()
    
    @IBOutlet weak var lbl_userName: UILabel!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var uiv_settings: UIView!
    @IBOutlet weak var uiv_dlgback: UIView!
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDatasource()
        loadLayout()
    }
    
    func loadLayout() {
        let url = URL(string: user!.picture ?? "")
        self.imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "logo"))
        self.lbl_userName.text = user?.user_name
        self.uiv_settings.isHidden = true
        self.uiv_dlgback.isHidden = true
    }
    
    func initDatasource() {
        
        var settingOption = [String]()
        if user?.type == PARAMS.SELLER{
            settingOption = ["My Account","Notifications","Terms and Conditions","Privacy Policy","Payout Schedule","Support","Change Password","Close Account","Logout"]
        }else{
            settingOption = ["My Account","Notifications","Terms and Conditions","Privacy Policy","Refund Policy","Support","Change Password","Close Account","Logout"]
        }
        
        let  images = ["icon_user","noty","terms","privacy","icon_payoutschedule","support","lock","logout","logout"]
        for i in 0 ... images.count - 1 {
            let one = SettingModel (settingOption[i],image: images[i], state: false)
            settingDatasource.append(one)
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return settingDatasource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
        cell.entity = settingDatasource[indexPath.row]
        let indextValue = indexPath.row
        
        if(indextValue == 1 ){
            cell.switch_btn.isHidden = false
        }
        else{
            cell.switch_btn.isHidden = true
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let aa = indexPath.row
        switch  aa {
        case 0 :
            if user?.type == PARAMS.BUYER{
                rootcontrollerName = rootcontrollerNames.SettingsVC.rawValue
                self.gotoVC("EditProfileVC")
            }else{
                rootcontrollerName1 = rootcontrollerNames1.SettingsVC.rawValue
                self.gotoStoryBoardVC("GeneralProfileVC", fullscreen: true)
            }
            break
        case 1 :
            let cell =  tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            let state = !cell.switch_btn.isOn
            self.settingDatasource[indexPath.row].state = state
            tableView.reloadData()
            
            break
        case 2 :
            self.gotoVC("TermsVC")
            break
        case 3 :
            self.gotoVC("PrivacyVC")
            break
        case 4 :
            self.gotoVC("PayoutScheduleVC")
            break
        case 5 :
            self.gotoVC("SupportVC")
            break
            
        case 6 :
            self.gotoVCModal("ChangePwdVC")
            break
            
        case 7 :
            self.uiv_settings.isHidden = false
            self.uiv_dlgback.isHidden = false
            
        case 8 :
            self.gotoVC("DashboardVC")
            UserDefault.setBool(key: PARAMS.LOGOUT, value: true)
            user!.clearUserInfo()
            UserDefault.setString(key: PARAMS.TYPE, value: nil)
            UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
            break
        
        default :
            print(aa)
        }
        tableView.reloadData()
    }
    @IBAction func gotoMain(_ sender: Any) {
        if user?.type == PARAMS.BUYER{
            self.gotoStoryBoardVC("MainVC", fullscreen: true)
        }else{
            self.gotoVC("SellerMainVC")
        }
        //self.dismiss(animated: false, completion: nil)
    }
    @IBAction func okBtnClicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        ApiManager.closeAccount { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.uiv_settings.isHidden = true
                user?.clearUserInfo()
                UserDefault.setString(key: PARAMS.TYPE, value: nil)
                UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
                self.gotoVC("DashboardVC")
            }
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.uiv_dlgback.isHidden = true
        self.uiv_settings.isHidden = true
    }
    
}
