//
//  TermsVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import WebKit

class TermsVC: BaseVC1 {
    
    @IBOutlet weak var ui_webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        let url = URL(string: Constants.termsLink)!
        
        ui_webView.load(URLRequest(url: url))

    }
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
    }
}
