//
//  SupportVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/12/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import MessageUI

class RefundVC : BaseVC1 ,UITextViewDelegate{
    
    
    @IBOutlet weak var txv_feedback: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.txv_feedback.delegate = self
        
        self.txv_feedback.text = "Please explain reason for cancellation"
        self.txv_feedback.textColor = UIColor.lightGray
        
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if txv_feedback.textColor == UIColor.lightGray {
            txv_feedback.text = nil
            txv_feedback.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if txv_feedback.text.isEmpty {
            txv_feedback.text = "Please explain reason for cancellation"
            txv_feedback.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func gotoRefund(_ sender: Any) {
        // cancel 5 for buyer, reject 4 for seller
        if self.txv_feedback.text == "" || self.txv_feedback.text == "Please explain reason for cancellation"{
           // self.showAlerMessage(message: "Please add your cancellation reason.")
            self.progShowInfo(true, msg: "Please add your cancellation reason.")
            return
        }else{
            if let refundbooking = refundBooking, let state = refund_state{
                self.showLoadingView(vc: self)
                ApiManager.cancelBooking(id: refundbooking.id!, reason: self.txv_feedback.text, state: state, service_name: refundbooking.service_name!, service_cost: refundbooking.service_cost!) { (isSuccess, data) in
                    self.hideLoadingView()
                    if isSuccess{
                        if user!.type == PARAMS.BUYER{
                            self.showAlerMessage(message: "Cancellation request successful.\nPlease allow 2-4 business days for refund")
                            self.gotoStoryBoardVC("BookingVC", fullscreen: true)
                        }else{
                            self.showAlerMessage(message: "Cancellation request successful.")
                            self.gotoStoryBoardVC("BookingSellerVC", fullscreen: true)
                        }
                    }else{
                        self.showAlerMessage(message: "Network issue!")
                    }
                }
            }
        }
    }
    
    @IBAction func goBack(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        //self.gotoStoryBoardVC("MainVC", fullscreen: true)
    }
}

