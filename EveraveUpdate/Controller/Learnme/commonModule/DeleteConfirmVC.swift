//
//  DeleteConfirmVC.swift
//  EveraveUpdate
//
//  Created by Mac on 5/11/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//


import UIKit

class DeleteConfirmVC : BaseVC1{
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated:true)
    }
    
    @IBAction func okbtnclicked(_ sender: Any) {
        self.showLoadingView(vc: self)
        if let selectedbooking = historyBooking{
            // TODO : Delete action
            ApiManager.removeBooking(id: selectedbooking.id!) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    if user?.type == PARAMS.SELLER{
                        self.gotoStoryBoardVC("HistorySellerVC", fullscreen: true)
                    }else{
                        self.gotoStoryBoardVC("HistoryVC", fullscreen: true)
                    }
                }
            }
        }
    }
}
