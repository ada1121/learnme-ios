//
//  BaseVC1.swift
//  Loyaltiyapp
//
//  Created by PSJ on 10/21/19.
//  Copyright © 2019 PSJ. All rights reserved.
//

import UIKit
import Toast_Swift
import SVProgressHUD
import SwiftyUserDefaults
import SwiftyJSON
import KRProgressHUD
import KRActivityIndicatorView
import MBProgressHUD

class BaseVC1: UIViewController {
    
    var gradientLayer: CAGradientLayer!
    var mainVC : MainVC!
    var loginVC : LoginVC!
    var alertController : UIAlertController? = nil
    var hud: MBProgressHUD?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func showProgressHUDHUD(view : UIView, mode: MBProgressHUDMode = .annularDeterminate) -> MBProgressHUD {
    
        let hud = MBProgressHUD .showAdded(to:view, animated: true)
        hud.mode = mode
        hud.label.text = "Loading";
        hud.animationType = .zoomIn
        hud.tintColor = UIColor.white
        hud.contentColor = .darkGray
        return hud
    }
    
    func hideLoadingView() {
       if let hud = hud {
           hud.hide(animated: true)
       }
    }
    
    func showLoadingView(vc: UIViewController, label: String = "") {
        
        hud = MBProgressHUD .showAdded(to: vc.view, animated: true)
        
        if label != "" {
            hud!.label.text = label;
        }
        hud!.mode = .indeterminate
        hud!.animationType = .zoomIn
        hud!.tintColor = UIColor.gray
        hud!.contentColor = Constants.PROGRESS_COLOR
    }
    
    func gotoStoryBoardVC(_ name: String, fullscreen: Bool) {
        let storyboad = UIStoryboard(name: name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        if fullscreen{
            targetVC.modalPresentationStyle = .fullScreen
        }
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoStoryBoardVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: name, bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .popover
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func gotoVCModal(_ name: String) {
        let storyboad = UIStoryboard(name: "Main", bundle: nil)
        let targetVC = storyboad.instantiateViewController(withIdentifier: name)
        targetVC.modalPresentationStyle = .popover
        //targetVC.modalTransitionStyle = .crossDissolve
        self.present(targetVC, animated: false, completion: nil)
    }
    
    func setEdtPlaceholder(_ edittextfield : UITextField , placeholderText : String, placeColor : UIColor, padding: UITextField.PaddingSide)  {
        edittextfield.attributedPlaceholder = NSAttributedString(string: placeholderText,
        attributes: [NSAttributedString.Key.foregroundColor: placeColor])
        edittextfield.addPadding(padding)
    }
    
    func createGradientView(_ targetView : UIView) {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.init(named: "startColor")!.cgColor, UIColor.init(named: "endColor")!.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
    }
    
    func createGradientLabel(_ targetView : UIView, letter : String,fontsize : Int ,position : Int) {
        gradientLayer = CAGradientLayer()
        gradientLayer.frame = targetView.bounds
        gradientLayer.colors = [UIColor.black.cgColor, UIColor.black.cgColor]
        targetView.layer.addSublayer(gradientLayer)
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Create a label and add it as a subview
        let label = UILabel(frame: targetView.bounds)
        label.text = letter
        label.font = UIFont.boldSystemFont(ofSize: CGFloat(fontsize))
        
        if position == 0 {
            label.textAlignment = .center
        }
        else if position == 1{
            label.textAlignment = .left
        }
        
        else if position == 2{
            label.textAlignment = .right
        }
        
        targetView.addSubview(label)
        
        // Tha magic! Set the label as the views mask
        targetView.mask = label
    }
    
    // MARK:KPProgressHUD group
    func setProgressHUDStyle(_ style: Int,backcolor: UIColor,textcolor : UIColor, imagecolor : UIColor ) {
         if style != 2{
         let styles: [KRProgressHUDStyle] = [.white, .black, .custom(background: #colorLiteral(red: 0.3411764801, green: 0.6235294342, blue: 0.1686274558, alpha: 1), text: #colorLiteral(red: 0.1294117719, green: 0.2156862766, blue: 0.06666667014, alpha: 1), icon: #colorLiteral(red: 0.9372549057, green: 0.3490196168, blue: 0.1921568662, alpha: 1))]
             KRProgressHUD.set(style: styles[style]) }
         
         else{
             let styles : KRProgressHUDStyle = .custom (background:backcolor,text : textcolor, icon: imagecolor )
              KRProgressHUD.set(style: styles)
         }
     }
     
    func showProgressSet_withMessage(_ msg : String, msgOn : Bool,styleVal: Int ,backColor: UIColor,textColor : UIColor,imgColor : UIColor , headerColor : UIColor, trailColor : UIColor)  {
     
     setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgColor)
         KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
         KRProgressHUD.show(withMessage: msgOn == false ? nil : msg)
     }
     
     func progressSet(styleVal: Int ,backColor: UIColor,textColor : UIColor , imgcolor: UIColor, headerColor : UIColor, trailColor : UIColor)  {
     
         setProgressHUDStyle(styleVal,backcolor: backColor,textcolor : textColor, imagecolor: imgcolor)
         KRProgressHUD.set(activityIndicatorViewColors: [headerColor, trailColor])
         
     }

     func progShowSuccess(_ msgOn:Bool, msg:String){
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorCancel")!, textColor: .black, imgcolor: .green, headerColor: .red, trailColor: .yellow)
         KRProgressHUD.showSuccess(withMessage: msgOn == false ? nil : msg)
     }

     func progShowError(_ msgOn:Bool, msg:String) {
        self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorCancel")!, textColor: .black, imgcolor: .red, headerColor: .red, trailColor: .yellow)
         KRProgressHUD.showError(withMessage: msgOn == false ? nil : msg)
     }

     func progShowWithImage(_ msgOn:Bool, msg:String ,image : String) {
         let image = UIImage(named:image)!
         KRProgressHUD.showImage(image, message: msgOn == false ? nil : msg)
     }
     
     func progShowInfo(_ msgOn:Bool, msg:String) {
         self.progressSet( styleVal: 2, backColor: UIColor.init(named: "ColorCancel")!, textColor: .black, imgcolor: .blue, headerColor: .red, trailColor: .yellow)
         KRProgressHUD.showInfo(withMessage: msgOn == false ? nil : msg)
     }
     
     func showProgress() {
         KRProgressHUD.show()
     }
    
     func hideProgress() {
         KRProgressHUD.dismiss()
     }
     
     func resetProgress() {
         KRProgressHUD.resetStyles()
     }
    
    // MARK: UIAlertView Controller
    func alertMake(_ msg : String) -> UIAlertController {
        alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
        alertController!.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        return alertController!
    }
    
    func alertDisplay(alertController: UIAlertController) {
        self.present(alertController, animated: true, completion: nil)
    }
    
//    func showMessage1(_ message : String) {
//        self.view.makeToast(message)
//    }
    // MARK: Unique function group
    
    
    /*func logOut(){
          let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
          loginVC = (storyBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC)
          loginNav = NavigationController(rootViewController: loginVC)
          loginNav.modalPresentationStyle = .fullScreen
          self.present(loginNav, animated: true, completion: nil)
          loginNav.isNavigationBarHidden = true
    }*/
    
    func gotoVC(_ nameVC: String){
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let toVC = storyBoard.instantiateViewController( withIdentifier: nameVC)
        toVC.modalPresentationStyle = .fullScreen
        self.present(toVC, animated: false, completion: nil)
    }
    
    func gotoNavPresent1(_ storyname : String, fullscreen: Bool) {
          let toVC = self.storyboard?.instantiateViewController(withIdentifier: storyname)
            if fullscreen{
                toVC?.modalPresentationStyle = .fullScreen
            }else{
                toVC?.modalPresentationStyle = .pageSheet
            }
          self.navigationController?.pushViewController(toVC!, animated: false)
            
    }
    
    func goBack() {
        self.navigationController?.popViewController(animated: false)
    }
    
    func goBackHome() {
        self.navigationController?.popToRootViewController(animated: false)
    }
    
    func goSetting() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "SettingsVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func gotoMessageSendVC() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageSendVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
    
    func gotoMessageViewVC() {
       let toVC = self.storyboard?.instantiateViewController(withIdentifier: "MessageViewVC")
       toVC?.modalPresentationStyle = .fullScreen
       self.navigationController?.pushViewController(toVC!, animated: false)
    }
       
   func navBarHidden() {
       self.navigationController?.isNavigationBarHidden = true
   }
    
   func navBarTransparent1() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
      self.navigationController?.navigationBar.shadowImage = UIImage()
      self.navigationController?.navigationBar.isTranslucent = true
      self.navigationController?.view.backgroundColor = .clear
    }
    
    func navBarwithColor() { self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
         self.navigationController?.navigationBar.shadowImage = UIImage()
         self.navigationController?.navigationBar.isTranslucent = true
         self.navigationController?.view.backgroundColor = .blue
   }
    
    //MARK:- Toast function
    func showToast(_ message : String) {
        self.view.makeToast(message)
    }
    
    func showToast(_ message : String, duration: TimeInterval = ToastManager.shared.duration, position: ToastPosition = .bottom) {
        self.view.makeToast(message, duration: duration, position: position)
    }
    
    func showToastCenter(_ message : String, duration: TimeInterval = ToastManager.shared.duration) {
        showToast(message, duration: duration, position: .center)
    }
}




