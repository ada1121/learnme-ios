//
//  HelperClass.swift
//  Everave
//
//  Created by kirti on 21/04/19.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseStorage



class HelperClass: NSObject {

    static let sharedInstance : HelperClass = {
        let instance = HelperClass()
        return instance
    }()
    
    class func showAlertMessage(message:String) {
        TinyToast.shared.show(message: message, valign: .top, duration: .normal)
    }
}
