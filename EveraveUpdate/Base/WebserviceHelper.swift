//
//  WebserviceHelper.swift
//  AutoHome
//
//  Created by kirti on 11/01/18.
//  Copyright © 2018 Deep Kiran. All rights reserved.
//

import UIKit
import Alamofire
import PKHUD

var DefaultEncoding = "URLEncoding.default"
var HttpBodyEncoding = "URLEncoding.httpBody"

typealias CompletionHandler = (_ success:Bool,_ reponsedata:NSDictionary) -> Void
typealias ResponseHandler = (_ success:Bool,_ data:NSData, _ error : String) -> Void
typealias ConnectivityHandler = (_ success:Bool) -> Void

class Connectivity {
  
    class func internetConnection(completionHandler: @escaping ConnectivityHandler){
        
        //var Status:Bool = false
       
        let url = NSURL(string: "http://google.com/")
        let request = NSMutableURLRequest(url: url! as URL)
        request.httpMethod = "HEAD"
        request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringLocalAndRemoteCacheData
        request.timeoutInterval = 1.0
        let session = URLSession.shared
       
        session.dataTask(with: request as URLRequest as URLRequest, completionHandler: {(data, response, error) in
          
            if let httpResponse = response as? HTTPURLResponse {
             
                if httpResponse.statusCode == 200 {
                    completionHandler(true)
                }
            }
            completionHandler(false)

        }).resume()
        
    }
    
     class func connetivityAvailable() ->Bool {
        return NetworkReachabilityManager()!.isReachable
    }

}

class WebserviceHelper: NSObject {

    class func postWebServiceCall(urlString:String, parameters:[String:AnyObject], encodingType: String, ShowProgress:Bool, completionHandler: @escaping CompletionHandler){
    
    if Connectivity.connetivityAvailable() == false {
        print("internet is not available.")
         HelperClass.showAlertMessage(message: "Please check your internet connection")
        completionHandler(false,NSMutableDictionary())
        return
    }
    
    if ShowProgress {
         HUD.show(.systemActivity)
      }
     //   var parametersnew = parameters
//        if let userid = UserDefaults.standard.value(forKey: KUserid) as? String {
//            parametersnew["user_id"] = userid as AnyObject;
//        }
   
       
    let encoding: ParameterEncoding!
    
    if encodingType == DefaultEncoding{
        encoding = JSONEncoding.default
    }else{
        encoding = URLEncoding.httpBody
    }
      
//        Alamofire.request(urlString).responseString { (respone) in
//            
//            print(respone)
//        }
        print("url:\(urlString)")
         print("url:\(parameters)")
    Alamofire.request(urlString, method: HTTPMethod.post, parameters: parameters, encoding:encoding).responseJSON(completionHandler: { (response) in
      
      if  response.result.isSuccess {
     
        if ShowProgress {
            HUD.hide()
        }
        
        if let result = response.result.value{
            
           let JSONdict = result as! NSDictionary
             print(JSONdict)
            
            if JSONdict .value(forKey: "result_code") as! NSNumber == 200{
               completionHandler(true,JSONdict)
            
            }else{
                if JSONdict.count > 0 {
                    completionHandler(false,JSONdict)
                }else{
                    completionHandler(false,NSMutableDictionary())
                }

            }

          }
      }
     
        if response.result.isFailure{
       
            if ShowProgress{
                HUD.hide()
            }
            print("Lost Connection %@",urlString)
            
            if let responseCode = response.response?.statusCode as Int?{
                
              print("Lost Connection with code %@",response.result.value as Any)
                print(response.result.error?.localizedDescription as Any)
              let responseDic = NSMutableDictionary()
               responseDic.setObject(responseCode, forKey: "statusCode" as NSCopying)
               completionHandler(false,NSMutableDictionary())
            }
            completionHandler(false,NSMutableDictionary())
       }
      
      
    })
    
  }
    
    
    class func showMessageOnTop(dicInfo : NSDictionary, view : UIView){
        
        if let message = dicInfo.object(forKey: "message") as? String {
            HelperClass .showAlertMessage(message: message)
          //  AppDelegate.showMessageWithView(message: message, view: view)
        }
    }
  
    class func WebapiGetMethod(strurl:String, ShowProgress: Bool = false, completionHandler: @escaping CompletionHandler){
    
        if Connectivity.connetivityAvailable() == false {
           print("internet is not available.")
           completionHandler(false,NSMutableDictionary())
             HelperClass.showAlertMessage(message: "Please check your internet connection")
          // AppDelegate .showMessage(message:"Please check your internet connection")
           return
        }
  
        if ShowProgress{
            HUD.show(.systemActivity)
        }
    
       Alamofire.request(strurl).responseJSON { response in
      
        if let status = response.response?.statusCode {
        switch(status){
        case 200:
         
            if let result = response.result.value{
                
                print(result)
                if let JSONdict = result as? NSMutableDictionary{
                    DispatchQueue.main.async {
                        completionHandler(true,JSONdict)
                    }
                }
                
                if ShowProgress {
                    HUD.hide()
                }
                
          }else{
            if ShowProgress {
                 HUD.hide()
            }
            completionHandler(false,NSMutableDictionary())
          }
          print("example success")
        
        default:
          print("error with get response status: \(status)")
          
            DispatchQueue.main.async {
                 if ShowProgress {
                  HUD.hide()
             }
          }
          
           completionHandler(false,NSMutableDictionary())
        }
      
        }
        
        DispatchQueue.main.async {
            if ShowProgress {
                HUD.hide()
            }
            
        }
      // completionHandler(false,NSMutableDictionary())
    }
  }
    
    
    class func WebapiImageUploadMethod(strurl:String, ShowProgress: Bool = false,parameters:[String:AnyObject],image:UIImage?,videoUrl:URL?, completionHandler: @escaping CompletionHandler){
        
        print(strurl)
        print(parameters)
        
        if ShowProgress{
            HUD.show(.systemActivity)
        }
   
    Alamofire.upload(multipartFormData: { multipartFormData in
        if (videoUrl != nil)  {
            do {
                let data = try Data(contentsOf: videoUrl!, options: .mappedIfSafe)
               let exten = videoUrl!.pathExtension
                 multipartFormData.append(data, withName: "video", fileName: "video.\(exten)", mimeType: "video/\(exten)")
                //  here you can see data bytes of selected video, this data object is upload to server by multipartFormData upload
            } catch  {
            }
        }else{
            if image != nil  {
                let imgData = image?.jpegData(compressionQuality: 0.1)
                multipartFormData.append(imgData!, withName:parameters["filetype"] as! String,fileName: "file.png", mimeType: "image/png")
            }
           
        }
        for (key, value) in parameters {
        
            if value is NSArray {
                multipartFormData.append(try! JSONSerialization.data(withJSONObject:value, options: []), withName: key)
            }
            else{
                multipartFormData.append((value as! String).data(using: String.Encoding.utf8)!, withName: key)
            }
        }
    },
    to:strurl)
    { (result) in
        switch result {
        case .success(let upload, _, _):
            upload.uploadProgress(closure: { (progress) in
                print("Upload Progress: \(progress.fractionCompleted)")
            })
            upload.responseJSON { response in
                if let dict = response.result.value as? NSDictionary {
                    print(dict)
                    if (dict["result_code"] as! Int) == 200 {
                      completionHandler(true,dict)
                    }else{
                        if let message = dict["message"] as? String {
                            HelperClass.showAlertMessage(message:message)
                        }
                    }
                }
            }
            
        case .failure(let encodingError):
            print(encodingError)
            HelperClass.showAlertMessage(message:encodingError.localizedDescription)
            
        }
    }
    
    
        if Connectivity.connetivityAvailable() == false {
            print("internet is not available.")
            completionHandler(false,NSMutableDictionary())
             HelperClass.showAlertMessage(message: "Please check your internet connection")
            return
        }
        
        
        
        Alamofire.request(strurl).responseJSON { response in
            
            if let status = response.response?.statusCode {
                switch(status){
                case 200:
                    
                    if let result = response.result.value{
                        
                        if ShowProgress {
                            HUD.hide()
                        }
                        
                        if let JSONdict = result as? NSMutableDictionary{
                            completionHandler(true,JSONdict)
                        }
                        
                    }else{
                        
                        if ShowProgress {
                            HUD.hide()
                        }
                        completionHandler(false,NSMutableDictionary())
                    }
                    print("example success")
                    
                default:
                    print("error with get response status: \(status)")
                    
                    DispatchQueue.main.async {
                        if ShowProgress {
                            HUD.hide()
                        }
                    }
                    
                    completionHandler(false,NSMutableDictionary())
                }
                
            }else{
                
                DispatchQueue.main.async {
                    if ShowProgress {
                        HUD.hide()
                    }
                }
                completionHandler(false,NSMutableDictionary())
            }

          }
      }
    
   }

