
import Foundation
//import Firebase
import UIKit

// MARK: for message section
var msgIndexId = ""
var friendID : String? =  nil
var msgPartnerName = ""
var msgPartnerPicture = ""
var chatting_option : Int! = 0 // 0 - from chatlist view : 1 - from friend list

// MARK: for select date
var selectedDate : Date? =  nil
var fromHour : String?
var toHour : String?
var fromMin : String?
var toMin : String?

// MARK: for video conferencing
var confRoomName = "default" // video conference room name

// MARK: mainview-> seller profile setting seller object
var sellerModel: SellerModel?

// MARK: for setting top tab in the seller portfolio
enum topTab: String {
    case review = "review"
    case portfolio = "portfolio"
}

// MARK: toptab global variable setting
var toptabType: String?

// MARK: current timezone
var localTimeZoneIdentifier: String { return TimeZone.current.identifier }

// MARK: seller model for seller side
var thisSeller: SellerModel?

// MARK: sellerside for setting weeks days selection
var weekday: String?
// MARK: for dropdown menu datasource setting
var dropDownOptionsDataSource = ["Barber Specialist", "Beautician Specialist", "Nails Specialist", "Eyebrow Specialist", "Makeup Specialist",
"Tutoring Services", "ASMR", "Mukbang", "Personal Assistant", "Legal Services", "Public Figure", "Yoga Instructor", "Medical Services",
"Dental Services", "Personal Trainer", "Veternarian", "Pet Groomer", "Pet Trainer", "Nutritionist", "Culinary Assistance", "Tour Guide","Accounting", "Auto Services", "Event Planning", "Dance Instructor", "Video Game Instructor", "Comedy Services",
"Interior Design", "Security Virtual Patrol", "Shoe Repair Assistance", "DIY Expert", "Music Lessons", "Fashion Designer"]

// MARK: for preview in the seller side availabletime and about me
var aboutMe = ""
var tools = ""
var languages = ""
var ratingVal = ""
var countVal = ""
var availableTimes = [""]

// MARK: for setting the viewcontroller remember

var controllerName = ""
enum controllerNames: String{ // for seller side
    case MainVC = "SellerMainVC"
    case SettingsVC = "SettingsVC"
}

// MARK: for buyside send to the available time model in the calendar
var sellerAvaillableTimemodel: AvailableTimeModel?
var sellerSeriveName: String?
var sellerServieTime: String?
var sellerServiePrice: String?

// MARK: for setting the viewcontroller remember for buyerside
var rootcontrollerName: String?
enum rootcontrollerNames: String{
    case MainVC = "MainVC"
    case SettingsVC = "SettingsVC"
}

var rootcontrollerName1: String?
enum rootcontrollerNames1: String{
    case SellerMainVC = "SellerMainVC"
    case SettingsVC = "SettingsVC"
}


// MARK:  for setting buyer and seller side to select the selected booking booking model to the detail controller
var selectedBooking: BookingModel?

// MARK:  for setting buyer and seller side to select the selected history booking model to the history detail controller
var historyBooking: BookingModel?


// MARK:  refund for the seller and buyer seller 4, buyer 5
var refundBooking: BookingModel?
var refund_state: String?

// MARK:  video chatting
var roomID: String?

// MARK:  OTP verfication
var OTP_countryCode: String?
var OTP_phonenumber: String?
var OTP_verifyID: String?

// MARK:  Signup parameters
var sign_photo: String?
var sign_firstName: String?
var sign_lastName: String?
var sign_userName: String?
var sign_email: String?
var sign_phone: String?
var sign_dob: String?
var sign_gender: String?
var sign_ssn: String?
var sign_password: String?
var sign_device_token: String?
var sign_type: String?

//MARK:  for signup birthdate
var birthdate: String?
var birthdayView : Bool = false
//MARK:  for send imageURL from message view to imageshow vc
var imageURL = ""
var leftandright = false
struct Constants {
    static let SAVE_ROOT_PATH = "save_root"
    static let termsLink = BASE_URL + "terms"
    static let payoutScheduleLink = BASE_URL + "payoutschedule"
    static let refundPolicyLink = BASE_URL + "refund"
    static let privacyLink = BASE_URL + "privacy"
    static let deviceToken = UIDevice.current.identifierForVendor!.uuidString
    static let PROGRESS_COLOR = UIColor.black
    //static let MAPBOX_ACCESS_TOKEN = "pk.eyJ1IjoiaG15b25nc29uZyIsImEiOiJjazc3aTJ6amwwODZnM2hsa2VwZnU5OGxvIn0.WzP_Y-tKr0U1jQWTJMCBeQ"
}
enum mode: String{
    case live = "live"
    case test = "test"
}
var paymentMode = ""


