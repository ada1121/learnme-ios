//
//  UIViewController+Extension.swift
//  EveraveUpdate
//
//  Created by Mac on 6/19/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase
import FirebaseStorage

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }

    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIViewController
{
    func tabbarStoryBoard()->UIStoryboard {
           return  UIStoryboard(name: "Tabbar", bundle: nil)
    }
    
    func addBackButton(){
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(image: UIImage(named: "back"), style: .plain, target: self, action:#selector(btnActionBackClicked))
    }
    
    @objc func btnActionBackClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func raveStoryBoard()->UIStoryboard {
        return  UIStoryboard(name: "Rave", bundle: nil)
    }
    func mainStoryBoard()->UIStoryboard {
        return  UIStoryboard(name: "Main", bundle: nil)
    }
    
    func chatStoryBoard()->UIStoryboard {
        return  UIStoryboard(name: "Chat", bundle: nil)
    }
    
    func showAlerMessage(message:String) {
        let alertController = UIAlertController(title: nil, message:message, preferredStyle: .alert)
        let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
        }
        alertController.addAction(action1)
        self.present(alertController, animated: true, completion: nil)
    }
    
    func showAlerMessagewithCompletion(message:String,handler: ((_ action: UIAlertAction)-> Void)?) {
        let alertController = UIAlertController(title: nil, message:message, preferredStyle: .alert)
        
        alertController.addAction(UIAlertAction(title: "", style: .default, handler: handler))
        self.present(alertController, animated: true, completion: nil)
    }
    
    var appDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    func getThumbnailImage(forUrl url: URL) -> UIImage? {
        let asset: AVAsset = AVAsset(url: url)
        let assetImgGenerate : AVAssetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImgGenerate.appliesPreferredTrackTransform = true
        let time = CMTimeMake(value: 1, timescale: 2)
        let img = try? assetImgGenerate.copyCGImage(at: time, actualTime: nil)
        if img != nil {
            let frameImg  = UIImage(cgImage: img!)
            return frameImg
        }
        return nil
    }
    
    func upload(_ image: UIImage,id:String,
                completion: @escaping (_ hasFinished: Bool, _ id :String, _ url: String) -> Void) {
        let data: Data = image.jpegData(compressionQuality: 0.01)!
        let messageName = NSUUID().uuidString
        let metadata2 = StorageMetadata()
        metadata2.contentType = "image/png"
        // ref should be like this
        let ref = Storage.storage().reference(withPath: "media/" + "\(messageName).png")
        ref.putData(data, metadata: metadata2) { (meta, error) in
            if error == nil {
                ref.downloadURL(completion: { (url, error) in
                    print(id)
                    completion(true,"1",url!.absoluteString)
                    // completion(true, url!.absoluteString)
                })
                // return url
                
            } else {
                completion(false,"1","")
            }
        }
    }
}
