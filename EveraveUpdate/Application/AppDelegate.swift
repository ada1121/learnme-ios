//
//  AppDelegate.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 12/10/19.
//  Copyright © 2019 Ubuntu. All rights reserved.
//

import UIKit
import CoreData
import IQKeyboardManagerSwift
import Firebase
import Stripe
import UserNotifications
import EBBannerView

let googleApiKey = "AIzaSyDlH0r6i2wJCGM29UwfXDQWRJ3XnSuPw8M"
//let stripe_public_key = "pk_test_TH7vGLG24SL0dttly3wgIRfz00m5W6YtvZ"
//let stripe_public_key = "pk_live_2496qoWZWxHRaJ3RenIFZryE00R4RVyc0B"
let agoraAppID = "a07aa44092ca4fe8afa40963aac5d7a1"

var deviceTokenString = ""

var user:UserModel?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        var stripe_key = ""
        // for switching live and test mode for stripe
        paymentMode = mode.live.rawValue
        
        if paymentMode == mode.test.rawValue{
            stripe_key = "pk_test_TH7vGLG24SL0dttly3wgIRfz00m5W6YtvZ"
        }else if paymentMode == mode.live.rawValue{
            stripe_key = "pk_live_2496qoWZWxHRaJ3RenIFZryE00R4RVyc0B"
        }
        // for stripe configuration
        STPPaymentConfiguration.shared().publishableKey = stripe_key
        // Override point for customization after application launch.
        FirebaseApp.configure()
        //BTAppSwitch.setReturnURLScheme("com.everave.update.payments")
        user = UserModel()
        user?.loadUserInfo()
        user?.saveUserInfo()
        
        IQKeyboardManager.shared.enable = true
        //GMSServices.provideAPIKey(googleApiKey)
        
        
        // for push notification setting
        let notificationTypes: UIUserNotificationType = [UIUserNotificationType.alert,UIUserNotificationType.badge, UIUserNotificationType.sound]
        let pushNotificationSettings = UIUserNotificationSettings(types: notificationTypes, categories: nil)

        application.registerUserNotificationSettings(pushNotificationSettings)
        //UIApplication.shared.applicationIconBadgeNumber = 0
        registerForPushNotifications()
        
        
        
        
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        return false
    }
    
    //MARK:-   set push notifations
        func registerForPushNotifications() {
            
            if #available(iOS 10.0, *) {
                UNUserNotificationCenter.current().delegate = self

                let authOptions: UNAuthorizationOptions = [.alert, .sound, .badge]//[.alert, .badge, .sound]
                UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler: {(granted, error) in
                    if granted {
                        print("Permission granted: \(granted)")
                        DispatchQueue.main.async() {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    }
                })
                
                Messaging.messaging().delegate = self
                Messaging.messaging().shouldEstablishDirectChannel = true
            } else {
                let settings: UIUserNotificationSettings = UIUserNotificationSettings(types: [.alert, .sound], categories: nil)
                UIApplication.shared.registerUserNotificationSettings(settings)
            }
        }
        
        func getRegisteredPushNotifications() {
            UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
                switch settings.authorizationStatus {
                    case .authorized, .provisional:
                        print("The user agrees to receive notifications.")
                        DispatchQueue.main.async {
                            UIApplication.shared.registerForRemoteNotifications()
                        }
                    case .denied:
                        print("Permission denied.")
                        // The user has not given permission. Maybe you can display a message remembering why permission is required.
                    case .notDetermined:
                        print("The permission has not been determined, you can ask the user.")
                        self.getRegisteredPushNotifications()
                    default:
                        return
                }
            })
        }

        func application(_ application: UIApplication, didRegister notificationSettings: UIUserNotificationSettings) {
            if notificationSettings.types != [] {
                application.registerForRemoteNotifications()
            }
        }
        
        func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
    //        if let refreshedToken = InstanceID.instanceID().token() {
    //            print("InstanceID token: \(refreshedToken)")
    //        }
            print("Successfully registered for notifications!")
            let tokenChars = (deviceToken as NSData).bytes.bindMemory(to: CChar.self, capacity: deviceToken.count)
            var tokenString = ""

            for i in 0..<deviceToken.count {
                tokenString += String(format: "%02.2hhx", arguments: [tokenChars[i]])
            }
    //        print("tokenString: \(tokenString)")
        }

        func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
            print("Failed to register for notifications: \(error.localizedDescription)")
        }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentCloudKitContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentCloudKitContainer(name: "EveraveUpdate")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

extension AppDelegate : UNUserNotificationCenterDelegate {
    
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        
        let aps             = userInfo["aps"] as? [AnyHashable : Any]
        //let badgeCount      = aps!["badge"] as? Int ?? 0
        let alertMessage    = aps!["alert"] as? [AnyHashable : Any]
        let bodyMessage     = alertMessage!["body"] as! String
        let titleMessage    = alertMessage!["title"] as! String
        
        NotificationCenter.default.post(name: Notification.Name("changedBadgeCount"), object: nil)
        
        let banner = EBBannerView.banner({ (make) in
            make?.style     = EBBannerViewStyle(rawValue: 12)
            make?.icon      = UIImage(named: "AppIcon")
            make?.title     = titleMessage
            make?.content   = bodyMessage
            make?.date      = "Now"
        })
         
        banner?.show()
        completionHandler([])
    }
}
//
extension AppDelegate : MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        Messaging.messaging().subscribe(toTopic: "/topics/all")
        deviceTokenString = fcmToken
        print("fcmToken: ", fcmToken)
        
//        let sender = PushNotificationSender()
//        let tk = "ffK7OmRn30cRt2vja4kT9P:APA91bEsrvHzlqF3k6iLNNVJJuU4DuXqNxfrICShG5sOK1HkAGaKKN6Sr6vM85VII2Av31D-QdLGwGCuCRREQAyqTwZoXGn8j1oHFLc3kzg8vd3oDHNzKLdiVrBrQUuddkNvuKzGWI1Y"
//        sender.sendPushNotification(to: tk, title: APP_NAME, body: CONSTANT.NOTI_BODY, badgeCount: 5)
    }
}



